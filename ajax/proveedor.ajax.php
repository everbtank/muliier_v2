<?php

require_once "../controladores/proveedor.controlador.php";
require_once "../modelos/proveedor.modelo.php";

class AjaxProveedor{

	/*=============================================
	VALIDAR EMAIL EXISTENTE
	=============================================*/	

	public $validarEmail;

	public function ajaxValidarEmail(){

		$datos = $this->validarEmail;

		$respuesta = ControladorProveedor::ctrMostrarProveedor("email", $datos);

		echo json_encode($respuesta);

	}

	/*=============================================
	REGISTRO CON FACEBOOK
	=============================================*/

	public $email;
	public $nombre;
	public $foto;


	public function ajaxRegistroFacebook(){

		$datos = array("nombre"=>$this->nombre,
					   "email"=>$this->email,
					   "foto"=>$this->foto,
					   "password"=>"null",
					   "modo"=>"facebook",
					   "verificacion"=>0,
					   "emailEncriptado"=>"null"
					);

		$respuesta = ControladorProveedor::ctrRegistroRedesSociales($datos);

		echo $respuesta;

	}	


	public $idCategoria;

	public function ajaxTraerSubCategoria(){

		$item = "id_categoria";
		$valor = $this->idCategoria;

		$respuesta = ControladorProveedor::ctrMostrarSubCategorias($item, $valor);

		echo json_encode($respuesta);

	}

	/*=============================================
	AGREGAR A LISTA DE DESEOS
	=============================================*/	

	public $idProveedor;
	public $idProducto;

	public function ajaxAgregarDeseo(){

		$datos = array("idProveedor"=>$this->idProveedor,
					   "idProducto"=>$this->idProducto);

		$respuesta = ControladorProveedor::ctrAgregarDeseo($datos);

		echo $respuesta;

	}

	/*=============================================
	QUITAR PRODUCTO DE LISTA DE DESEOS
	=============================================*/

	public $idDeseo;	

	public function ajaxQuitarDeseo(){

		$datos = $this->idDeseo;

		$respuesta = ControladorProveedor::ctrQuitarDeseo($datos);

		echo $respuesta;

	}




}

/*=============================================
VALIDAR EMAIL EXISTENTE
=============================================*/	

if(isset($_POST["validarEmail"])){

	$valEmail = new AjaxProveedor();
	$valEmail -> validarEmail = $_POST["validarEmail"];
	$valEmail -> ajaxValidarEmail();

}

/*=============================================
REGISTRO CON FACEBOOK
=============================================*/


if(isset($_POST["email"])){

	$regFacebook = new AjaxProveedor();
	$regFacebook -> email = $_POST["email"];
	$regFacebook -> nombre = $_POST["nombre"];
	$regFacebook -> foto = $_POST["foto"];
	$regFacebook -> ajaxRegistroFacebook();

}

/*=============================================
AGREGAR A LISTA DE DESEOS
=============================================*/	

if(isset($_POST["idCategoria"])){

	$traerSubCategoria = new AjaxProveedor();
	$traerSubCategoria -> idCategoria = $_POST["idCategoria"];
	$traerSubCategoria -> ajaxTraerSubCategoria();

}

if(isset($_POST["idProveedor"])){

	$deseo = new AjaxProveedor();
	$deseo -> idProveedor = $_POST["idProveedor"];
	$deseo -> idProducto = $_POST["idProducto"];
	$deseo ->ajaxAgregarDeseo();
}

/*=============================================
QUITAR PRODUCTO DE LISTA DE DESEOS
=============================================*/

if(isset($_POST["idDeseo"])){

	$quitarDeseo = new AjaxProveedor();
	$quitarDeseo -> idDeseo = $_POST["idDeseo"];
	$quitarDeseo ->ajaxQuitarDeseo();
}
