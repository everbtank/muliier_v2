<?php

require_once "../extensiones/paypal.controlador.php";

require_once "../controladores/carrito.controlador.php";
require_once "../modelos/carrito.modelo.php";

require_once "../controladores/productos.controlador.php";
require_once "../modelos/productos.modelo.php";

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

require_once "../controladores/notificaciones.controlador.php";
require_once "../modelos/notificaciones.modelo.php";


require_once "../modelos/usuarios.modelo.php";

require_once "../modelos/rutas.php";

require_once "../extensiones/PHPMailer/PHPMailerAutoload.php";
require_once "../extensiones/vendor/autoload.php";


$url = Ruta::ctrRuta();

/*if(!isset($_SESSION["validarSesion"])){

   echo '<script>window.location = "'.$url.'";</script>';

   exit();

}*/


class AjaxCarrito{




public $divisa;
   public $total;
   public $totalEncriptado;
   public $impuesto;
   public $envio;
   public $subtotal;
   public $tituloArray;
   public $cantidadArray;
   public $valorItemArray;
   public $idProductoArray;

   public function ajaxEnviarPaypal(){

      if(md5($this->total) == $this->totalEncriptado){

            $datos = array(
                  "divisa"=>$this->divisa,
                  "total"=>$this->total,
                  "impuesto"=>$this->impuesto,
                  "envio"=>$this->envio,
                  "subtotal"=>$this->subtotal,
                  "tituloArray"=>$this->tituloArray,
                  "cantidadArray"=>$this->cantidadArray,
                  "valorItemArray"=>$this->valorItemArray,
                  "idProductoArray"=>$this->idProductoArray,
               );

            $respuesta = Paypal::mdlPagoPaypal($datos);

            echo $respuesta;

      }
   }

   /*=============================================
   MÉTODO PAYU
   =============================================*/

   public function ajaxTraerComercioPayu(){

      $respuesta = ControladorCarrito::ctrMostrarTarifas(); 

      echo json_encode($respuesta);
   }


	public $idUsuario;
	public $idProducto;

	public function ajaxVerificarProducto(){

		$datos = array("idUsuario"=>$this->idUsuario,
					   "idProducto"=>$this->idProducto);

		$respuesta = ControladorCarrito::ctrVerificarProducto($datos);

		echo json_encode($respuesta);

	}

}



if(isset($_POST["metodo"]) == "contraentrega"){

   /*$productos = explode("-", $_POST['productos']);
   $cantidad = explode("-", $_POST['cantidad']);
   $pago = explode("-", $_POST['pago']);*/

    $idproductos = explode("," , $_POST["idProductoArray"]);
	$cantidad = explode("," , $_POST["cantidadArray"]);
	//$precioProductos = explode("," , $_POST["valorItemArray"]);


	for($i = 0; $i < count($idproductos); $i++){


   		$datos = array("idusuario"=>$_POST["idusuario"],
   						"idproducto"=>$idproductos[$i],
   						"metodo"=>$_POST["metodo"],
   						"email"=>$_POST["email"],
   						"direccion"=>$_POST["direccion"],
   						"cod_asesor"=>$_POST["cod_asesor"],
   						"celular"=>$_POST["celular"],
   						"municipio"=>$_POST["municipio"],
	                     "cantidad"=>$cantidad[$i],
	                     "detalle"=>$_POST["detalle"],
	                     "pago"=>$_POST["pago"],
	                     "barrio"=>$_POST["barrio"]);

          //var_dump($datos);
   		$respuesta = ControladorCarrito::ctrNuevasCompras($datos);
   		 $ordenar = "id_producto";
         $item = "id_producto";
         $valor = $idproductos[$i];

         $productosCompra = ControladorProductos::ctrListarProductos($ordenar, $item, $valor);

         foreach ($productosCompra as $key => $value) {

            $item1 = "ventas_producto";
            $valor1 = $value["ventas_producto"] + $cantidad[$i];
            $item2 = "id_producto";
            $valor2 =$value["id_producto"];

            $actualizarCompra = ControladorProductos::ctrActualizarProducto($item1, $valor1, $item2, $valor2);
            
         }

       _
   		if($respuesta == "ok"){
   			echo "carrito-de-compras";

   		}

   	}

}



/*=============================================
MÉTODO PAYU
=============================================*/ 

if(isset($_POST["metodoPago"]) && $_POST["metodoPago"] == "payu"){

   $idProductos = explode("," , $_POST["idProductoArray"]);
   $cantidadProductos = explode("," , $_POST["cantidadArray"]);
   $precioProductos = explode("," , $_POST["valorItemArray"]);

   $item = "id";

   for($i = 0; $i < count($idProductos); $i ++){

      $valor = $idProductos[$i];

      $verificarProductos = ControladorProductos::ctrMostrarInfoProducto($item, $valor);

      $divisa = file_get_contents("http://free.currconv.com/api/v7/convert?q=COP_".$_POST["divisaPayu"]."&compact=ultra&apiKey=a01ebaf9a1c69eb4ff79");

      $jsonDivisa = json_decode($divisa, true);

      $conversion = number_format($jsonDivisa["COP_".$_POST["divisaPayu"]],2);

      if($verificarProductos["precioOferta_producto"] == 0){

         $precio = $verificarProductos["precio_venta_producto"];
      
      }else{

         $precio = $verificarProductos["precioOferta_producto"];

      }

      $verificarSubTotal = $cantidadProductos[$i]*$precio;

      // echo number_format($verificarSubTotal,2)."<br>";
      // echo number_format($precioProductos[$i],2)."<br>";

      // return;

      if(number_format($verificarSubTotal,2) != number_format($precioProductos[$i],2)){

         echo "carrito-de-compras";

         return;

      }

   }

   $payu = new AjaxCarrito();
   $payu -> ajaxTraerComercioPayu();

}

/*=============================================
VERIFICAR QUE NO TENGA EL PRODUCTO ADQUIRIDO
=============================================*/ 

if(isset($_POST["idUsuario"])){

	$deseo = new AjaxCarrito();
	$deseo -> idUsuario = $_POST["idUsuario"];
	$deseo -> idProducto = $_POST["idProducto"];
	$deseo ->ajaxVerificarProducto();
}

   