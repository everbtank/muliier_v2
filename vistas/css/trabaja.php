
	<section id="what-we-do">
		<div class="container">
			<center><h1 class="block-h1 mb-1 h1">TRABAJA CON NOSOTROS</h1></center>
			<br>
			<p class="text-center text-muted h4">Te gustaría ser ASESOR de moda contactenos ya..! Estamos en todo Barranquilla..!</p>
			<center><h4 class="section-title mb-2 h2">Lo que te Ofrecemos</h4></center>
			<div class="row mt-5">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block-1">
							<h3 class="card-title">Llegar a mas Clientes</h3>
							<div class="col-md-2">
								<i class="fa fa-users fa-3x" aria-hidden="true"></i>
							</div>
							<div class="col-md-10">
							<p class="card-text">Llegar a nuevos clientes con una gama amplia de nuevos productos y así ampliar los servicios que ya ofreces.</p>
						    </div>	
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block-2">
							<h3 class="card-title">Conocimiento</h3>
                             <div class="col-md-2">
								<i class="fa fa-globe fa-3x" aria-hidden="true"></i>
							</div>
							 <div class="col-md-10">
							   <p class="card-text"> Afianzas tu  papel como asesor y comercializador en la cadena de suministros de tu cliente, sin tener que preocupar por entregar y cobrar.</p>
						    </div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block-3">
							<h3 class="card-title">Desarrollo personal </h3>
							<div class="col-md-2">
								<i class="fa fa-child fa-3x" aria-hidden="true"></i>
							</div>
							<div class="col-md-10">
							<p class="card-text"> Desarrollo personal y empresarial para que logres forma un negocio que te permite cumplir con tus sueños.</p>
						    </div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block-4">
							<h3 class="card-title">Conoce mas de Moda</h3>
							<div class="col-md-2">
								<i class="fa fa-diamond fa-3x" aria-hidden="true"></i>
							</div>
							<div class="col-md-10">
							<p class="card-text"> Desarrollo en conocimientos en asesoría de moda.</p>
						    </div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block-5">
							<h3 class="card-title">Poca Inversion</h3>
							<div class="col-md-2">
							  <i class="fa fa-university fa-3x" aria-hidden="true"></i>
							</div>
							<div class="col-md-10">
								<p class="card-text"> No tendrás que invertir grandes sumas para comprar varios productos llenarte de inventarios innecesarios.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block-6">
							<h3 class="card-title">Cuenta en Linea</h3>
							<div class="col-md-2">
								<i class="fa fa-desktop fa-3x" aria-hidden="true"></i>
							</div>
							<div class="col-md-10">
							  <p class="card-text"> Tienes acceso directo a tu propia cuenta personal en línea, que te permite hacer seguimiento a las compras de tus clientes y tus comisiones. </p>
							 </div>
						</div>
					</div>
				</div>
			</div>
		</div>	
       <br>
		<div class="container">
			<center><h2 class="section-title mb-2 h2">Si eres Fabricante o Proveedor</h2></center>
			<center><h3 class="section-title mb-3 h3">Te ayudamos a</h3></center>
			<div class="row mt-5">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block-1">
							<h3 class="card-title">Aumentar Ventas</h3>
							<div class="col-md-2">
								<i class="fa fa-thumbs-up fa-3x" aria-hidden="true"></i> 
							</div>
							<div class="col-md-10">
								<p class="card-text"> Aumentar el poder de ventas mediante un nuevo canal de distribución.</p>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block-2">
							<h3 class="card-title">Sin Intermediario</h3>
							<div class="col-md-2">
								<i class="fa fa-shopping-cart fa-3x" aria-hidden="true"></i>
							</div>
							<div class="col-md-10">
							<p class="card-text"> Podrás eliminar intermediarios y poder ofrecer mejores producto</p>
						 	</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block-3">
							<h3 class="card-title">Venta Rapida y Directa</h3>
							<div class="col-md-2">
								<i class="fa fa-line-chart fa-3x" aria-hidden="true"></i>
							</div>
							<div class="col-md-10">
								<p class="card-text">  Venta de tus productos sin tantos tramites y un solo responsable</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block-4">
							<h3 class="card-title">Estrategia de marketing</h3>
							<div class="col-md-2">
								<i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i>
							</div>
							<div class="col-md-10">
								<p class="card-text"> Ayudamos a desarrollar una estrategia de marketing por medio de la empresa y mejora en el proceso en de producción.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block-5">
							<h3 class="card-title">Gran escala</h3>
							<div class="col-md-2">
								<i class="fa fa-user-plus fa-3x" aria-hidden="true"></i>
							</div>
							<div class="col-md-10">
								<p class="card-text"> Lleva tus productos a gran escala </p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
					<div class="card">
						<div class="card-block block">
							<h3 class="card-title">Cuenta personal en linea</h3>
							<div class="col-md-2">
								<i class="fa fa-desktop fa-3x" aria-hidden="true"></i> 
							</div>
							<div class="col-md-10">
								<p class="card-text">  Tienen acceso directo a su propia cuenta personal en línea, donde podrán administrar sus productos a disposición para venta, en tallas y colores y precios.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<br>
		<br>
	</section>
	<!-- /Services section -->