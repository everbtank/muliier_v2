<!--=====================================
FOOTER
======================================-->

<footer class="container-fluid">

	<div class="container">

		<div class="row">

		 	<!--=====================================
			CATEGORÍAS Y SUBCATEGORÍAS FOOTER
			======================================-->

			<div class="col-lg-5 col-md-6 col-xs-12 footerCategorias">

			<?php

				$url = Ruta::ctrRuta();

				$item = null;
				$valor = null;

				$categorias = ControladorProductos::ctrMostrarCategorias($item, $valor);

				foreach ($categorias as $key => $value) {

					if($value["estado_categoria"] != 0){

						echo '<div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">

							<h4><a href="'.$url.$value["ruta_categoria"].'" class="pixelCategorias" titulo="'.$value["categoria_categoria"].'">'.$value["categoria_categoria"].'</a></h4>

							<hr>

							<ul>';

							$item = "id_categoria_subcategoria";

							$valor = $value["id_categoria"];

							$subcategorias = ControladorProductos::ctrMostrarSubCategorias($item, $valor);
							
							foreach ($subcategorias as $key => $value) {

								if($value["estado_subcategoria"] != 0){
							
									echo '<li><a href="'.$url.$value["ruta_subcategoria"].'" class="pixelSubCategorias" titulo="'.$value["subcategoria_subcategoria"].'">'.$value["subcategoria_subcategoria"].'</a></li>';

								}

							}

							echo '</ul>

						</div>';

					}

				}

			?>

			</div>

			<!--=====================================
			DATOS CONTACTO
			======================================-->

			<div class="col-md-3 col-sm-6 col-xs-12 text-left infoContacto">
				
				<h5>Dudas e inquietudes, contáctenos en:</h5>

				<br>
				
				<h5>
					
					<i class="fa fa-phone-square" aria-hidden="true"></i> (+57) 3165782690

					<br><br>

					<i class="fa fa-envelope" aria-hidden="true"></i> yosoymuliier@gmail.com

					<br><br>

					<i class="fa fa-map-marker" aria-hidden="true"></i>  Carrera 54 #64-223

					<br><br>
					Barranquilla | Colombia

				</h5>
	

			</div>

			<!--=====================================
			FORMULARIO CONTÁCTENOS
			======================================-->

			<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12 formContacto">
				
				<h4> CONSULTAS</h4>

				<form role="form" method="post" onsubmit="return validarContactenos()">

			  		<input type="text" id="nombreContactenos" name="nombreContactenos" class="form-control" placeholder="Escriba su nombre" required> 

			   		<br>
	    	      
   					<input type="email" id="emailContactenos" name="emailContactenos" class="	form-control" placeholder="Escriba su correo electrónico" required>  
                    <br>
   					<input type="text" id="numeroContactenos" name="numeroContactenos" class="	form-control" placeholder="Escriba su N° Celular" required>

   					<br>
	    		     	          
	       			<textarea id="mensajeContactenos" name="mensajeContactenos" class="form-control" placeholder="Escriba su mensaje" rows="5" required></textarea>

	       			<br>
	    	
	       			<input type="submit" value="Enviar" class="btn btn-default backColor pull-right" id="enviar">         

				</form>

				<?php 

					$contactenos = new ControladorUsuarios();
					$contactenos -> ctrFormularioContactenos();

				?>

			</div>
			
		</div>

	</div>

</footer>


<!--=====================================
FINAL
======================================-->

<div class="container-fluid final">
	
	<div class="container">
	
		<div class="row">
			
			<div class="col-md-8 col-sm-6 col-xs-12 text-left text-muted">
				
				<h5>&copy; 2020 Todos los derechos reservados. Sitio elaborado por Muliier con ♥    (&& <a href="https://sumaktec.com">Sumaktec</a> ) </h5>

			</div>
     
			<div class="col-sm-5 col-xs-12 col-md-4 text-right social">
				
			<ul>

			<?php
				
			$social = ControladorPlantilla::ctrEstiloPlantilla();

				$jsonRedesSociales = json_decode($social["redesSociales"],true);		

				foreach ($jsonRedesSociales as $key => $value) {

					echo '<li>
							<a href="'.$value["url"].'" target="_blank">
								<i class="fa '.$value["red"].' redSocial '.$value["estilo"].'" aria-hidden="true"></i>
							</a>
						</li>';
				}

			?>

			</ul>
			 <br>

			</div>

		</div>

	</div>

</div>