<!--=====================================
VERIFICAR
======================================-->

<?php

	$usuarioVerificado = false;
	
	$item = "EmailEncriptado";
	$valor =  $rutas[1];

	$respuesta = ControladorUsuarios::ctrMostrarUsuario($item, $valor);

	if($valor == $respuesta["emailEncriptado"]){

		$id = $respuesta["id"];
		$item2 = "verificacion";
		$valor2 = 0;

		$respuesta2 = ControladorUsuarios::ctrActualizarUsuario($id, $item2, $valor2);

		if($respuesta2 == "ok"){

			$usuarioVerificado = true;

		}

	}
		
	$proveedorVerificado = false;
	
	$item1 = "EmailEncriptado";
	$valor1 =  $rutas[1];

	$respuesta2 = ControladorProveedor::ctrMostrarProveedor($item1, $valor1);

	if($valor1 == $respuesta2["emailEncriptado"]){

		$id1 = $respuesta2["id"];
		$item2 = "verificacion";
		$valor2 = 0;

		$respuesta2 = ControladorProveedor::ctrActualizarProveedor($id1, $item2, $valor2);

		if($respuesta2 == "ok"){

			$proveedorVerificado = true;

		}

	}

?>

<div class="container">
	
	<div class="row">
	 
		<div class="col-xs-12 text-center verificar">
			
			<?php

				if($usuarioVerificado){

					echo '<h3>Gracias</h3>
						<h2><small>Hemos verificado tu correo electrónico. Comienza la aventura con Muliier.</small></h2>

						<br>

						<a href="#modalIngreso" data-toggle="modal"><button class="btn btn-default backColor btn-lg">INGRESAR</button></a>';
				

				} elseif ($proveedorVerificado) {

						echo '<h3>Gracias</h3>
						<h2><small>¡Hemos verificado su correo electrónico, ya puede ingresar al sistema como proveedor!</small></h2>

						<br>

						<a href="#modalProveedor" data-toggle="modal"><button class="btn btn-default backColor btn-lg">INGRESAR</button></a>';
					
				} else{

					#echo '<h3>Error</h3>';
					
					/*echo '<script>
		
            				window.location = "perfil";
            
            		</script>';*/
            		
            	
					echo '<br><h4>Le Damos la  Bienvenida a Muliier.com </h4>';
					
					
					echo '<br><h3> ! Comencemos ! </h3>';
					
					

					echo '<br>
					<hr>

					<a href="../perfil" data-toggle="modal"><button class="btn btn-default backColor btn-lg">IR A PERFIL</button></a>';
					
					
					/*	echo '<h2><small>¡ Sino pudo Ingresar al sistema, Hubo un problema al  verificar el correo electrónico,  vuelva a registrarse!. En caso contrario, puede obviar este mensaje. Estamos actualizando...! la plataforma, por eso estas notificiaciones. Gracias por su Compresion...! </small></h2>

					<br>

					<a href="#modalRegistro" data-toggle="modal"><button class="btn btn-default backColor btn-lg">REGISTRO</button></a>';*/


				}

			?>

		</div>

	</div>

</div>

