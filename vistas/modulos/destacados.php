<!--=====================================
BANNER
======================================-->

<?php

$servidor = Ruta::ctrRutaServidor();

$ruta = "sin-categoria";

$banner = ControladorProductos::ctrMostrarBanner($ruta);

if($banner != null){

	if($banner["estado"] != 0){

		echo '<figure class="banner">

				<img src="'.$servidor.$banner["img"].'" class="img-responsive" width="100%">	

			  </figure>';

	}

}

/*=============================================
PRODUCTOS DESTACADOS
=============================================*/

$titulosModulos = array("ARTÍCULOS EN OFERTA", "LO MÁS NUEVO", "LO MÁS VENDIDO", "LO MÁS VISTO");
$rutaModulos = array("articulos-oferta","lo-mas-nuevo", "lo-mas-vendido","lo-mas-visto");

$base = 0;
$tope = 4;

if($titulosModulos[0] == "ARTÍCULOS EN OFERTA"){

$ordenar = "precioOferta_producto";
$item = "oferta_producto";
$valor = 1;
$modo = "DESC";

$oferta = ControladorProductos::ctrMostrarProductos($ordenar, $item, $valor, $base, $tope, $modo);

}

if($titulosModulos[1] == "LO MÁS NUEVO"){

$ordenar = "id_producto";
$modo = "DESC";
$nuevo = ControladorProductos::ctrMostrarProductosNuevo($ordenar,  $base, $tope, $modo);

}

if($titulosModulos[2] == "LO MÁS VENDIDO"){

$ordenar = "ventas_producto";
$item = "estado_producto";
$valor = 1;
$modo = "DESC";

$ventas = ControladorProductos::ctrMostrarProductos($ordenar, $item, $valor, $base, $tope, $modo);

}

if($titulosModulos[3] == "LO MÁS VISTO"){

$ordenar = "vistas_producto";
$item = "estado_producto";
$valor = 1;
$modo = "DESC";

$vistas = ControladorProductos::ctrMostrarProductos($ordenar, $item, $valor, $base, $tope, $modo);

}

$modulos = array($oferta, $nuevo, $ventas, $vistas);

for($i = 0; $i < count($titulosModulos); $i ++){

	echo '<div class="container-fluid well well-sm barraProductos">

			<div class="container">
				
				<div class="row">
					
					<div class="col-xs-12 organizarProductos">

						<div class="btn-group pull-right">

							 <button type="button" class="btn btn-default btnGrid" id="btnGrid'.$i.'">
							 	
								<i class="fa fa-th" aria-hidden="true"></i>  

								<span class="col-xs-0 pull-right"> GRID</span>

							 </button>

							 <button type="button" class="btn btn-default btnList" id="btnList'.$i.'">
							 	
								<i class="fa fa-list" aria-hidden="true"></i> 

								<span class="col-xs-0 pull-right"> LIST</span>

							 </button>
							
						</div>		

					</div>

				</div>

			</div>

		</div>


		<div class="container-fluid productos">
	
			<div class="container">
		
				<div class="row">

					<div class="col-xs-12 tituloDestacado">

						<div class="col-sm-6 col-xs-12">
					
							<h1><small>'.$titulosModulos[$i].' </small></h1>

						</div>

						<div class="col-sm-6 col-xs-12">
					
							<a href="'.$rutaModulos[$i].' ">
								
								<button class="btn btn-default backColor pull-right">
									
									VER MÁS <span class="fa fa-chevron-right"></span>

								</button>

							</a>

						</div>

					</div>

					<div class="clearfix"></div>

					<hr>

				</div>

				<ul class="grid'.$i.'">';

				foreach ($modulos[$i] as $key => $value) {

					if($value["estado_producto"] != 0){
					
					echo '<li class="col-md-3 col-sm-6 col-xs-12">

							<figure>
								
								<a href="'.$value["ruta_producto"].'" class="pixelProducto" >
									
									<center>
									<img src="'.$servidor.$value["portada_producto"].'" class="img-responsive" width="100%">
									</center>

								</a>

							</figure>

							<h4>
					
								<small>
									
									<a href="'.$value["ruta_producto"].'" class="pixelProducto">
										
										'.$value["titulo_producto"].'<br>

										<span style="color:rgba(0,0,0,0)">-</span>';

										$fecha = date('Y-m-d');
										$fechaActual = strtotime('-30 day', strtotime($fecha));
										$fechaNueva = date('Y-m-d', $fechaActual);

										if($fechaNueva < $value["fecha_producto"]){

											echo '<span class="label label-warning fontSize">Nuevo</span> ';

										}

										if($value["oferta_producto"] != 0 && $value["precio_venta_producto"] != 0){

											echo '<span class="label label-warning fontSize">'.$value["descuentoOferta_producto"].'% off</span>';

										}

									echo '</a>	

								</small>			

							</h4>

							<div class="col-xs-6 precio">';

							if($value["precio_venta_producto"] == 0){

								echo '<h2><small>COP '.$value["precio_producto"].' x Mayor</small></h2>';

							}else{

								if($value["oferta_producto"] != 0){

									echo '<h2>

											<small>
						
												<strong class="oferta">COP $'.$value["precio_venta_producto"].'</strong>

											</small>

											<small>$'.$value["precioOferta_producto"].'</small>
										
										</h2>';

								}else{

									echo '<h2><small>COP $'.$value["precio_venta_producto"].'</small></h2>';

								}
								
							}
											
							echo '</div>

							<div class="col-xs-6 enlaces">
								
								<div class="btn-group pull-right">
									
									<button type="button" class="btn btn-default btn-xs deseos" idProducto="'.$value["id_producto"].'" data-toggle="tooltip" title="Agregar a mi lista de deseos">
										
										<i class="fa fa-heart" aria-hidden="true"></i>

									</button>';

									if($value["tipo_producto"] == "virtual" && $value["precio_venta_producto"] != 0){

										if($value["oferta_producto"] != 0){

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id_producto"].'" imagen="'.$servidor.$value["portada_producto"].'" titulo="'.$value["titulo_producto"].'" precio="'.$value["precioOferta_producto"].'" tipo="'.$value["tipo_producto"].'" peso="'.$value["peso_producto"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}else{

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id_producto"].'" imagen="'.$servidor.$value["portada_producto"].'" titulo="'.$value["titulo_producto"].'" precio="'.$value["precio_venta_producto"].'" tipo="'.$value["tipo_producto"].'" peso="'.$value["peso_producto"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}

									}

									echo '<a href="'.$value["ruta_producto"].'" class="pixelProducto">
									
										<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" title="Ver producto">
											
											<i class="fa fa-eye" aria-hidden="true"></i>

										</button>	
									
									</a>

								</div>

							</div>

						</li>';

					}
				}

				echo '</ul>

				<ul class="list'.$i.'" style="display:none">';

				foreach ($modulos[$i] as $key => $value) {

					if($value["estado_producto"] != 0){

					echo '<li class="col-xs-12">
					  
				  		<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
							   
							<figure>
						
								<a href="'.$value["ruta_producto"].'" class="pixelProducto">
									
									<img src="'.$servidor.$value["portada_producto"].'" class="img-responsive">

								</a>

							</figure>

					  	</div>
							  
						<div class="col-lg-10 col-md-7 col-sm-8 col-xs-12">
							
							<h1>

								<small>
								
									<a href="'.$value["ruta_producto"].'" class="pixelProducto">
										
										'.$value["titulo_producto"].'<br>';

										$fecha = date('Y-m-d');
										$fechaActual = strtotime('-30 day', strtotime($fecha));
										$fechaNueva = date('Y-m-d', $fechaActual);

										if($fechaNueva < $value["fecha_producto"]){

											echo '<span class="label label-warning">Nuevo</span> ';

										}

										if($value["oferta"] != 0 && $value["precio_producto"] != 0){

											echo '<span class="label label-warning">'.$value["descuentoOferta_producto"].'% off</span>';

										}		

									echo '</a>

								</small>

							</h1>

							<p class="text-muted">'.$value["titular_producto"].'</p>';

							if($value["precio_venta_producto"] == 0){

								echo '<h2><small>COP $'.$value["precio"].'x Mayor</small></h2>';

							}else{

								if($value["oferta_producto"] != 0){

									echo '<h2>

											<small>
						
												<strong class="oferta">COP $'.$value["precio_venta_producto"].'</strong>

											</small>

											<small>$'.$value["precioOferta_producto"].'</small>
										
										</h2>';

								}else{

									echo '<h2><small>COP $'.$value["precio_venta_producto"].'</small></h2>';

								}
								
							}

							echo '<div class="btn-group pull-left enlaces">
						  	
						  		<button type="button" class="btn btn-default btn-xs deseos"  idProducto="'.$value["id_producto"].'" data-toggle="tooltip" title="Agregar a mi lista de deseos">

						  			<i class="fa fa-heart" aria-hidden="true"></i>

						  		</button>';

						  		if($value["tipo_producto"] == "virtual" && $value["precio_producto"] != 0){

										if($value["oferta_producto"] != 0){

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id_producto"].'" imagen="'.$servidor.$value["portada_producto"].'" titulo="'.$value["titulo_producto"].'" precio="'.$value["precioOferta"].'" tipo="'.$value["tipo_producto"].'" peso="'.$value["peso_producto"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}else{

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id_producto"].'" imagen="'.$servidor.$value["portada_producto"].'" titulo="'.$value["titulo_producto"].'" precio="'.$value["precio_venta_producto"].'" tipo="'.$value["tipo_producto"].'" peso="'.$value["peso_producto"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}

									}

						  		echo '<a href="'.$value["ruta_producto"].'" class="pixelProducto">

							  		<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" title="Ver producto">

							  		<i class="fa fa-eye" aria-hidden="true"></i>

							  		</button>

						  		</a>
							
							</div>

						</div>

						<div class="col-xs-12"><hr></div>

					</li>';

					}

				}

				echo '</ul>

			</div>

		</div>';

}

?>

