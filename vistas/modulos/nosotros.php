

<!------ Include the above in your HEAD tag ---------->

<center><h1 class="block-h1" > ¿QUIENES SOMOS? </h1></center>
<div class="container">
     <div class="col-md-1">
        </div>
       <div class="col-md-10">

           <center>
            <div class="block-51-text">
              	<p class="text-center text-muted h4">Muliier es una experiencia de estilo y determinación, enfocada en el crecimiento personal y la búsqueda de soluciones en temas de moda, ofreciendo calidad y originalidad en todos los productos y servicios. </p>
             </div>
             </center>
      </div>
       <div class="col-md-1">
        </div>
</div>
<br>
 <div class="container">
          <img src="vistas/img/nosotros/muliierlogo.jpg" alt="" class="img-responsive" width="1150" height="400" >
</div>
<br>
<div class="container">
    <div class="page-header">
        <center><h1 class="block-titulo" id="timeline">Historia de la Empresa</h1></center>
        
        
    </div>
    <ul class="timeline">
        <li>
          <div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Inicios</h4>
              <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Desde el 2017 </small></p>
            </div>
            <div class="timeline-body">
              <p>Muliier, empresa moderna, visionaria y en constante crecimiento; tiene su nacimiento en el año 2017, en la ciudad de Barranquilla. Creada por la Ingeniera Marcela Ospino, quien ha sido su gestora desde los inicios.  Este, es un proyecto que comenzó a germinar, como una semilla hasta robustecerse y convertirse en la sólida empresa que es hoy. </p>
              <p>
                Bajo su liderazgo y con el apoyo de Berenice Vega, propietaria de Calzados Francii, se dio inicio a la  comercialización de calzado personalizado y a las medidas de los requerimientos de los clientes, lo cual fue la tierra que sirvió de base para echar raíces que sostienen a Mullier.
.</p>
            </div>
          </div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-badge warning"><i class="glyphicon glyphicon-credit-card"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Formación</h4>
            </div>
            <div class="timeline-body">
              <p>Muliier en el 2017 participó de programas como: Apps.co y Reto creativos. Este último es un programa de MacondoLab, entidad que ha aportado valiosamente en el proceso de desarrollo y consolidación del negocio.</p>
              <p>En el 2018 se abrió la oportunidad apoyar a más mujeres que buscan nuevas oportunidades de emprender y cumplir sus sueños, asesorando a sus clientas y comercializando productos sin tener que preocuparse por la logística y recaudo de dinero propios de esta labor.</p>
            
            </div>
          </div>
        </li>
        <li>
          <div class="timeline-badge danger"><i class="glyphicon glyphicon-credit-card"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Alianzas</h4>
            </div>
            <div class="timeline-body">
              <p>En el 2019 la oferta de productos se aumenta gracias a las alianzas con emprendedores y dueños de negocios que buscaban un apoyo en venta de sus productos, puesto que es difícil mantener una excelente visualización en el mercado por la alta oferta de sus competidores.</p>
            </div>
          </div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-badge success"><i class="glyphicon glyphicon-thumbs-up"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Muliier hoy</h4>
            </div>
            <div class="timeline-body">
              <p>En la actualidad, Muliier es una empresa que apoya el desarrollo personal de sus Asesoras de moda, partiendo del empoderamiento personal. Además se ofrece la facilidad de adquirir productos de alta calidad, 100% Colombianos, hechos por empresarios que están convencidos de que la moda es la representación de nuestro ser y reflejo de nuestra personalidad y esencia.</p>
            </div>
          </div>
        </li>
    </ul>
</div>

  <div class='col-lg-8 col-lg-offset-2'>
    <hr>
    </div>
  <div class="site-section">
    <div class="container">
      <div class="row ">

        <div class="col-md-6 order-md-2 mb-3 mb-md-0">
          <img src="vistas/img/nosotros/mision3.jpg" alt=""  style=" width:500; height:380; margin-padding:20px;" class="img-responsive"  >
        </div>
      
        <div class="col-lg-6 col-md-6 pr-md-6 mb-6 order-md-1">
          <div class="card" >
          <div class="block-41">
            
            <center><h2 class="block-41-heading mb-3">Misión</h2></center>
            <div class="block-41-text">
              <p class="text-muted lead text-justify">Transmitimos estilo y determinación, por medio de  productos y servicios de alta calidad relacionados con moda, tendencias, motivación personal e innovación, contando con una variedad de alternativas que expresan originalidad y generan respaldo a emprendedores y artesanos en todo el territorio nacional.</p>
            </div>
          </div>
        </div>
        </div>
        
      </div> <!-- .row -->

      <div class="row ">
       
        <div class="col-md-6 pl-md-6 mb-6">
          <div class="card" >
          <div class="block-41">
            <center><h2 class="block-41-heading mb-2">Visión</h2></center>
            <div class="block-41-text">
              <p class="text-muted lead text-justify">Para el año 2023 impactar positivamente la vida de las personas a través de experiencias y soluciones en moda, que nos permitan consolidarnos en el mercado nacional. </p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6 order-md-2 mb-5 mb-md-0">
          <img src="vistas/img/nosotros/vision.jpeg" alt="" class="img-responsive" alt="Responsive image" style="  max-height: 60%; width:500; height:380; margin-padding:20px;"  >
        </div>

      </div> <!-- .row -->

    </div>
    </div>
     <div class='col-lg-8 col-lg-offset-2'>
        <hr>
       </div>
    <div class="row">
      <div class="col-md-12 mb-5 text-center mt-5">
          <h2 class="block-titulo">Equipo de Trabajo</h2>
        </div>
        <div class="container">
        <div class="col-md-6 col-lg-4">
        </div>
        <div class="col-md-6 col-lg-4">
          <div class="block-38 text-center">
            <div class="block-38-img">
              <div class="block-38-header">
                <img src="vistas/img/nosotros/equipo1.jpg" alt="Image placeholder" class="img-circle" width="100" height="100">
                <h3 class="block-38-heading">Marcela Ospino</h3>
                <p class="block-38-subheading">CEO y Fundadora </p>
              </div>
              <div class="block-38-body">
                <p>Mujer empresaria de nacimiento, apasionada, comprometida y luchadora. Ingeniera industrial, Universidad Simón Bolívar de profesión con énfasis en Logística y operaciones. Experiencia en Asesoría junior, mentiría y joven investigador de la MacondoLab de la Universidad Simón Bolívar  </p>
               
              </div>
              <ul class="follow-us clearfix">
                <li><a href="https://www.facebook.com/ospinomarcela05"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/ingmarcelaospino/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="https://www.linkedin.com/in/marcela-leonor-ospino-escobar-51ba82a1/"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        <div class="btn-group">
           
         </div>
       </div>
        </div>
        <div class="col-md-6 col-lg-4">
          <div class="block-38 text-center">
            <div class="block-38-img">
              <div class="block-38-header">
                <img src="vistas/img/nosotros/equipo2.jpg" alt="Image placeholder" class="img-circle" width="100" height="100">
                <h3 class="block-38-heading">Ever Aguirre</h3>
                <p class="block-38-subheading">Dir. Tecnologías y Desarrollador</p>
              </div>
              <div class="block-38-body">
                <p>Ingeniero de sistemas unheval-PERU, Desarrollador de sistema de prototipos de robótica, IA, Realidad aumentado, especialista en mantenimiento y soporte de sistemas informáticos. Practicas Macondolab - Colombia </p>
              </div>
              <ul class="follow-us clearfix">
                <li><a href="https://www.facebook.com/everbtank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/everbtank/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="https://www.linkedin.com/in/ever-aguirre-raymundo-699499193/"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4">
          <div class="block-38 text-center">
            <div class="block-38-img">
              <div class="block-38-header">
                <img src="vistas/img/nosotros/equipo3.jpg" alt="Image placeholder" class="img-circle" width="100" height="100">
                <h3 class="block-38-heading">Dalgys</h3>
                <p class="block-38-subheading">Dra Moda y Creación</p>
              </div>
              <div class="block-38-body">
                <p>Mujer dinámica, responsable y comunicadora. Administradora de Negocios Internacionales y Tecnóloga en Diseño para la Industria de la Moda con experiencia en el sector de ventas y diseño de prendas de vestir.  </p>
              </div>
                <ul class="follow-us clearfix">
                <li><a href="https://www.facebook.com/SOYDAL"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/soydal_/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4">
          <div class="block-38 text-center">
            <div class="block-38-img">
              <div class="block-38-header">
                <img src="vistas/img/nosotros/equipo4.jpg" alt="Image placeholder" class="img-circle" width="100" height="100">
                <h3 class="block-38-heading">Jossi Durango</h3>
                <p class="block-38-subheading">Asesor Creativo</p>
              </div>
              <div class="block-38-body">
                <p>Mercadólogo, gestor cultural y líder de proyectos turísticos, emprendimiento y marketing. </p>
              </div>
               <ul class="follow-us clearfix">
                <li><a href="https://www.facebook.com/Jeglio"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/jossidurangoa/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="https://www.linkedin.com/in/jossidurango/"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>

 
      </div>
  
    </div>
  </div>
   

    <hr>
 
<br>
<br>
  <div class="container">
    <center><h2 class="block-titulo">Alianzas y Apoyo</h2></center>
    <br>
     <div class="row">
     
      <div class="col-md-4">
       <img src="vistas/img/nosotros/alianza1.jpg" class="img-responsive" alt="Responsive image" width="350" height="200" />
      </div>
      <div class="col-md-4">
       <img src="vistas/img/nosotros/alianza2.jpg" class="img-responsive" alt="Responsive image" width="350" height="200" />
      </div>
       <div class="col-md-4">
       <img src="vistas/img/nosotros/alianzaenvios.png" class="img-responsive" alt="Responsive image" width="350" height="200" />
      </div>
    </div>
    <br>
    <br>
    </div>
  </div>

  

  

