<br>
<div class="container">
        <center><div ><h1 class="block-h1 mb-1 h1">COMO COMPRAR</h1></div></center>
</div>
<div class="container">
    <hr>
  <div class="row">

    <center><div class=""><h4 class="section-title mb-2 h3">Tabla de Medidas</h4></div></center>
    <br>
    <div class="zoom1 col-md-4 col-sm-6 col-xs-12"><img class="img-responsive" src="vistas/img/comocomprar/comprar1.jpg" width="300" height="430"/>
      <center><h4 class="section-title mb-3 h4">Faldas</h4></center>
    </div>
    <div class="zoom2 col-md-4 col-sm-6 col-xs-12"><img class="img-responsive" src="vistas/img/comocomprar/comprar2.jpg" width="300" height="430"/><center><h4 class="section-title mb-3 h4">Short</h4></center></div>
    <div class="zoom3 col-md-4 col-sm-6 col-xs-12"><img class="img-responsive" src="vistas/img/comocomprar/comprar3.jpg" width="300" height="430" /><center><h4 class="section-title mb-3 h4">Jean</h4></center></div>
  </div>
  <hr>
  <div class="row">
       <center><div class=""><h4 class="section-title mb-2 h3">Medidas Especificas</h4></div></center>
     <br>
   <div  class="zoom4 col-md-4 col-sm-6 col-xs-12"><img class="img-responsive" id="img1" src="vistas/img/comocomprar/tallas4.png" width="320" height="320"/>
   </div>
    <div class="zoom5 col-md-4 col-sm-6 col-xs-12"><img class="img-responsive" src="vistas/img/comocomprar/tallas5.png" width="320" height="320" /></div>
    <div class="zoom6 col-md-4 col-sm-6 col-xs-12"><img class="img-responsive" src="vistas/img/comocomprar/tallas6.png" width="320" height="320" /></div>
    </div>
    
    <br>
    <hr>
    <div class="row">
        <br>
   <center>
   <div  class="zoom4 col-md-12 col-sm-6 col-xs-12"><img class="img-responsive" id="img1" src="vistas/img/comocomprar/medida05.jpeg" width="320" height="320"/>
   </div>
   
    </center>
 
</div>
 </div>
</div>



<div id="modal1" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

<br>
<br>
<br>
<div class="container">
  <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="media">
            <div class="row">
              <div class="col-md-2 col-sm-2">
                <i class="fa fa-cart-arrow-down fa-4x" aria-hidden="true"></i>
              </div>
              <div class="col-md-10 col-sm-10">
                <div class="media-body">
                  <h3 class="media-heading">Politicas de Compra</h3>
                  <br>
                  <p><i class="fa fa-check" aria-hidden="true"></i> El cliente paga el valor correspondiente al envió, el cual se realiza por contrata a una empresa tercero para que realice el domicilio correspondiente</p>
                  <p><i class="fa fa-check" aria-hidden="true"></i> Los productos se deben pagar al momento de la entrega o antes del despacho.</p>
                  <p><i class="fa fa-check" aria-hidden="true"></i> El despacho a nivel nacional se realizara por una empresa tercera quien el cliente asumirá el costo del mismo, y lo pagara cuando este llegue a su destino.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="col-md-6 col-sm-6">
          <div class="media">
            <div class="row">
              <div class="col-md-2 col-sm-2">
                <i class="fa fa-money fa-4x" aria-hidden="true"></i>
              </div>
              <div class="col-md-10 col-sm-10">
                <div class="media-body">
                  <h3 class="media-heading">Metodo de Pago</h3>
                    <br>
                    <p><i class="fa fa-check" aria-hidden="true"></i> Pagos contra entrega. Aplica en barranquilla y su área metropolitana</p> 
                    <p><i class="fa fa-check" aria-hidden="true"></i> Consignación Bancolombia. Cuenta de Ahorro 48183461268</p>
                    <p><i class="fa fa-check" aria-hidden="true"></i> Si se realiza por otro medio como Efecty, Super giros el cliente asume los costos adiciones.</p>
                </div>
            </div>
            </div>
          </div>
        </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-6 col-sm-6">
          <div class="media">
            <div class="row">
              <div class="col-md-2 col-sm-2">
                  <i class="fa  fa-users fa-4x" aria-hidden="true"></i>
                </div>
              <div class="col-md-10 col-sm-10">
                <div class="media-body">
                <h3 class="media-heading">Proceso de Garantia</h3>
                <br>
                <p><i class="fa fa-check" aria-hidden="true"></i> El proceso de garantía se aplica defectos de fábricas por daños o desgastes asociados a los mismos.
                <p><i class="fa fa-check" aria-hidden="true"></i> Para cambios por tallas, el tiempo límite es de 3 días una vez realizada la compra </p>
                <p><i class="fa fa-check" aria-hidden="true"></i> En compras de personalización (sobre medida) no se realizan cambios por tallas, solamente por desperfectos.  </p>
                <p><i class="fa fa-check" aria-hidden="true"></i> Los gastos de domicilio por cambios los asume el cliente</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        
  </div>
</div>

             
<br>
<hr>
<br>
<div class="container">
  <br>
    <h1 class="page-heading bottom-indent">¿Cómo Realizar Compra?</h1>
    <ul class="timeline">
        <li>
            <div class="timeline-badge">1</div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <div class="col-md-3">
                  <i class="fa fa-laptop fa-4x" aria-hidden="true"></i>
                </div>
                <div class="col-md-9">
                    <h4 class="timeline-title">Ingrese a la plataforma WEB</h4>
                </div>
                <div class="timeline-body">
                    <p>Visita nuestra pagina web:www.muliier.com vea productos y cree su cuenta en nuestra plataforma.</p>
                </div>
              </div>
            </div>
        </li>
        <li class="timeline-inverted">
            <div class="timeline-badge warning">2</div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                <div class="col-md-3">
                  <i class="fa fa-plus-square fa-5x" aria-hidden="true"></i>
                </div>
                <div class="col-md-9">
                    <h4 class="timeline-title">Agregue Productos</h4>
                </div>
                <div class="timeline-body">
                    <p>Agrega tus productos al carrito de compra.</p>
                </div>
              </div>
            </div>
        </li>
        <li>
            <div class="timeline-badge">3</div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                <div class="col-md-3">
                  <i class="fa fa fa-credit-card-alt fa-5x" aria-hidden="true"></i>
                </div>
                <div class="col-md-9">
                    <h4 class="timeline-title">Pago</h4>
                </div>
                <div class="timeline-body">
                    <p>Escoge tu medio de pago.</p>
                </div>
              </div>
            </div>
        </li>
        <li class="timeline-inverted">
            <div class="timeline-badge warning">4</div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                <div class="col-md-3">
                  <i class="fa fa-user fa-5x" aria-hidden="true"></i>
                </div>
                <div class="col-md-9">
                    <h4 class="timeline-title">Verifica tus Datos</h4>
                </div>
                <div class="timeline-body">
                    <p>Verifica tus datos peronales y confirmar dirección y contacto para el despacho. (no olvisdes agregar quien fue tu asesor de moda y asi poder acumular puntos) </p>
                </div>
              </div>
            </div>
        </li>
        <li>
            <div class="timeline-badge">5</div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                <div class="col-md-3">
                  <i class="fa fa-user-times fa-5x" aria-hidden="true"></i>
                </div>
                <div class="col-md-9">
                    <h4 class="timeline-title">Observaciones</h4>
                </div>
                <div class="timeline-body">
                    <p>Nos puedes dejar tus observaciones en caso del que tu pedido sea personalizado.</p>
                </div>
              </div>
            </div>
        </li>
        <li class="timeline-inverted">
            <div class="timeline-badge info">6</div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                <div class="col-md-3">
                  <i class="fa fa fa-bus fa-5x" aria-hidden="true"></i>
                </div>
                <div class="col-md-9">
                    <h4 class="timeline-title">Recibe</h4>
                </div>
                <div class="timeline-body">
                    <p>Recibe tu producto en casa.</p>
                </div>
              </div>
            </div>
        </li>
        <li class="timeline-badge">
            <div class="timeline-badge ">7</div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                <div class="col-md-3">
                  <i class="fa fa-thumbs-o-up fa-5x" aria-hidden="true"></i>
                </div>
                <div class="col-md-9">
                    <h4 class="timeline-title"> Comparte tu experiencia.</h4>
                </div>
                <div class="timeline-body">
                    <p>Comparte tu experiencia en dejandonos comentarios en nuestras redes Sociales.</p>
                </div>
              </div>
            </div>
        </li>
    </ul>
</div>