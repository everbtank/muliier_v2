<?php

$servidor = Ruta::ctrRutaServidor();
$url = Ruta::ctrRuta();

?>


<!--=====================================
BREADCRUMB OFERTAS
======================================-->
<div class="container-fluid well well-sm">
	
	<div class="container">
		
		<div class="row">
			
			<ul class="breadcrumb fondoBreadcrumb text-uppercase">
				
				<li><a href="<?php echo $url;  ?>">INICIO</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>

			</ul>

		</div>

	</div>

</div>

<!--=====================================
JUMBOTRON AVISO OFERTA
======================================-->

<?php

	if(isset($rutas[1])){

		if($rutas[1] == "aviso"){

			echo '<div class="container-fluid">

					<div class="container">
	
					 	<div class="jumbotron">

					 		<button type="button" class="close cerrarOfertas" style="margin-top:-50px;"><h1>&times;</h1></button>

					 		<h1 class="text-center">¡Hola!</h1> 

					 		 <p class="text-center">
					 		 	
								Tu artículo ha sido asignado a tus compras, pero antes queremos presentarte las siguientes ofertas, si no deseas ver las ofertas y continuar en el artículo que acabas de adquirir haz click en el siguiente botón:
								<br><br>
								<a href="'.$url.'perfil">
								<button class="btn btn-default backColor btn-lg">
								VER ARTÍCULOS COMPRADOS
								</button>
								</a>
								<br><br>
								<a href="#moduloOfertas">
								<button class="btn btn-default btn-lg">
								VER OFERTAS
								</button>
								</a>

					 		 </p>
					 
					 	</div>

					</div>

				</div>';

		}

	}

?>

<div class="container-fluid">

	<div class="container">

		<div class="row" id="moduloOfertas">
			
			<?php

			$item = null;
			$valor = null;

			date_default_timezone_set('America/Bogota');

			$fecha = date('Y-m-d');
			$hora = date('H:i:s');
			
			$fechaActual = $fecha.' '.$hora;
			
			/*=============================================
			TRAEMOS LAS OFERTAS DE CATEGORÍAS
			=============================================*/

			$respuesta = ControladorProductos::ctrMostrarCategorias($item, $valor);

			foreach ($respuesta as $key => $value) {

				if($value["estado_categoria"] != 0){

					if($value["oferta_categoria"] == 1){

						if($value["finOferta_categoria"] > $fecha){

							$datetime1 = new DateTime($value["finOferta_categoria"]);
							$datetime2 = new DateTime($fechaActual);

							$interval = date_diff($datetime1, $datetime2);

							$finOferta = $interval->format('%a');

							echo '<div class="col-md-4 col-sm-6 col-xs-12">

								<div class="ofertas">
									
									<h3 class="text-center text-uppercase">

										¡OFERTA ESPECIAL EN <br>'.$value["categoria_categoria"].'!
									
									</h3>

									<figure>

										<img class="img-responsive" src="'.$servidor.$value["imgOferta_categoria"].'" width="100%">

										<div class="sombraSuperior"></div>';

										if($value["descuentoOferta_categoria"] != 0){


											echo '<h1 class="text-center text-uppercase">%'.$value["descuentoOferta_categoria"].' OFF</h1>';
										
										}else{

											echo '<h1 class="text-center text-uppercase">$ '.$value["precioOferta_categoria"].'</h1>';

										}
				

									echo '</figure>';

									if($finOferta == 0){

										echo '<h4 class="text-center">La oferta termina hoy</h4>';

									}else if($finOferta == 1){

										echo '<h4 class="text-center">La oferta termina en '.$finOferta.' día</h4>';

									}else{

										echo '<h4 class="text-center">La oferta termina en '.$finOferta.' días</h4>';
									}

								echo '<center>
									
									<div class="countdown" finOferta="'.$value["finOferta"].'"></div>

									<a href="'.$url.$value["ruta"].'" class="pixelOferta" titulo="'.$value["categoria"].'">

									<button class="btn backColor btn-lg text-uppercase">Ir a la Oferta</button>

									</a>

								</center>
									
								</div>

							</div>';

						}

					}

				}
				
			}

			/*=============================================
			TRAEMOS LAS OFERTAS DE SUBCATEGORÍAS
			=============================================*/

			$respuestaSubcategorias = ControladorProductos::ctrMostrarSubCategorias($item, $valor);

			foreach ($respuestaSubcategorias as $key => $value) {

				if($value["estado_subcategoria"] != 0){

					if($value["oferta_subcategoria"] == 1 && $value["ofertadoPorCategoria_subcategoria"] == 0){

						if($value["finOferta"] > $fecha){

							$datetime1 = new DateTime($value["finOferta_subcategoria"]);
							$datetime2 = new DateTime($fechaActual);

							$interval = date_diff($datetime1, $datetime2);

							$finOferta = $interval->format('%a');

							echo '<div class="col-md-4 col-sm-6 col-xs-12">

								<div class="ofertas">
									
									<h3 class="text-center text-uppercase">

										¡OFERTA ESPECIAL EN <br>'.$value["subcategoria_subcategoria"].'!
									
									</h3>

									<figure>

										<img class="img-responsive" src="'.$servidor.$value["imgOferta_subcategoria"].'" width="100%">

										<div class="sombraSuperior"></div>';

										if($value["descuentoOferta_subcategoria"] != 0){


											echo '<h1 class="text-center text-uppercase">%'.$value["descuentoOferta_subcategoria"].' OFF</h1>';
										
										}else{

											echo '<h1 class="text-center text-uppercase">$ '.$value["precioOferta_subcategoria"].'</h1>';

										}
				

									echo '</figure>';

									if($finOferta == 0){

										echo '<h4 class="text-center">La oferta termina hoy</h4>';

									}else if($finOferta == 1){

										echo '<h4 class="text-center">La oferta termina en '.$finOferta.' día</h4>';

									}else{

										echo '<h4 class="text-center">La oferta termina en '.$finOferta.' días</h4>';
									}

								echo '<center>
									
									<div class="countdown" finOferta="'.$value["finOferta"].'"></div>

									<a href="'.$url.$value["ruta_subcategoria"].'" class="pixelOferta">

									<button class="btn backColor btn-lg text-uppercase">Ir a la Oferta</button>

									</a>

								</center>
									
								</div>

							</div>';

						}

					}

				}
				
			}

			/*=============================================
			TRAEMOS LAS OFERTAS DE PRODUCTOS
			=============================================*/

			$ordenar = "id_producto";

			$respuestaProductos = ControladorProductos::ctrListarProductos($ordenar, $item, $valor);

			foreach ($respuestaProductos as $key => $value) {

				if($value["estado_producto"] != 0){

					if($value["oferta_producto"] == 1 && $value["ofertadoPorCategoria"] == 0 && $value["ofertadoPorSubCategoria_producto"] == 0){

						if($value["finOferta_producto"] > $fecha){

							$datetime1 = new DateTime($value["finOferta_producto"]);
							$datetime2 = new DateTime($fechaActual);

							$interval = date_diff($datetime1, $datetime2);

							$finOferta = $interval->format('%a');

							echo '<div class="col-md-4 col-sm-6 col-xs-12">

								<div class="ofertas">
									
									<h3 class="text-center text-uppercase">

										¡OFERTA ESPECIAL EN <br>'.$value["titulo_producto"].'!
									
									</h3>

									<figure>

										<img class="img-responsive" src="'.$servidor.$value["imgOferta_producto"].'" width="100%">

										<div class="sombraSuperior"></div>';

										if($value["descuentoOferta_producto"] != 0){


											echo '<h1 class="text-center text-uppercase">%'.$value["descuentoOferta_producto"].' OFF</h1>';
										
										}else{

											echo '<h1 class="text-center text-uppercase">$ '.$value["precioOferta_producto"].'</h1>';

										}
				

									echo '</figure>';

									if($finOferta == 0){

										echo '<h4 class="text-center">La oferta termina hoy</h4>';

									}else if($finOferta == 1){

										echo '<h4 class="text-center">La oferta termina en '.$finOferta.' día</h4>';

									}else{

										echo '<h4 class="text-center">La oferta termina en '.$finOferta.' días</h4>';
									}

								echo '<center>
									
									<div class="countdown" finOferta="'.$value["finOferta_producto"].'"></div>

									<a href="'.$url.$value["ruta_producto"].'" class="pixelOferta">

									<button class="btn backColor btn-lg text-uppercase">Ir a la Oferta</button>

									</a>

								</center>
									
								</div>

							</div>';

						}

					}

				}
				
			}


			?>


		</div>

	</div>

</div>

