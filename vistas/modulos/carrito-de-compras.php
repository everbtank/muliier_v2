<?php
    
    $url = Ruta::ctrRuta();

 ?>

<!--=====================================
BREADCRUMB CARRITO DE COMPRAS
======================================-->

<div class="container-fluid well well-sm">
	
	<div class="container">
		
		<div class="row">
			
			<ul class="breadcrumb fondoBreadcrumb text-uppercase">
				
				<li><a href="<?php echo $url;  ?>">CARRITO DE COMPRAS</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>

			</ul>

		</div>

	</div>

</div>

<!--=====================================
TABLA CARRITO DE COMPRAS
======================================-->

<div class="container-fluid">

	<div class="container">

		<div class="panel panel-default">
			
			<!--=====================================
			CABECERA CARRITO DE COMPRAS
			======================================-->

			<div class="panel-heading cabeceraCarrito">
				
				<div class="col-md-6 col-sm-7 col-xs-12 text-center">
					
					<h3>
						<small>PRODUCTO</small>
					</h3>

				</div>

				<div class="col-md-2 col-sm-1 col-xs-0 text-center">
					
					<h3>
						<small>PRECIO</small>
					</h3>

				</div>

				<div class="col-sm-2 col-xs-0 text-center">
					
					<h3>
						<small>CANTIDAD</small>
					</h3>

				</div>

				<div class="col-sm-2 col-xs-0 text-center">
					
					<h3>
						<small>SUBTOTAL</small>
					</h3>

				</div>

			</div>

			<!--=====================================
			CUERPO CARRITO DE COMPRAS
			======================================-->

			<div class="panel-body cuerpoCarrito">

				

			</div>

			<!--=====================================
			SUMA DEL TOTAL DE PRODUCTOS
			======================================-->

			<div class="panel-body sumaCarrito">

				<div class="col-md-5 col-sm-6 col-xs-12 pull-right well">
					
					<div class="col-xs-6">
						
						<h4>TOTAL:</h4>

					</div>

					<div class="col-xs-6">

						<h4 class="sumaSubTotal">
							
							

						</h4>

					</div> 

				</div>

			</div>

			<!--=====================================
			BOTÓN CHECKOUT
			======================================-->

			<div class="panel-heading cabeceraCheckout">

			<?php

				if(isset($_SESSION["validarSesion"])){

					if($_SESSION["validarSesion"] == "ok"){

						if($_SESSION["direccion"]!="" && $_SESSION["celular"]!="" && $_SESSION["barrio"]!=""){

							echo '<a id="btnCheckout" href="#modalCheckout" data-toggle="modal" idUsuario="'.$_SESSION["id"].'"><button class="btn btn-default backColor btn-lg pull-right">REALIZAR PAGO</button></a>';
						}else{

							echo '<script> 

								swal({
									  title: "¡Actualizar sus Datos!",
									  text: "¡Datos de direccion, celular o barrio Vacios ...! Porfavor tiene que actualizar sus datos..! ",
									  type:"warning",
									  confirmButtonText: "Cerrar",
									  closeOnConfirm: false
									},

									function(isConfirm){

										if(isConfirm){
											history.back();
											window.location = "perfil";
										}
								});

							</script>';
						
						}

					}


				}else{

					echo '<a href="#modalIngreso" data-toggle="modal"><button class="btn btn-default backColor btn-lg pull-right">REALIZAR PAGO</button></a>';
				}

			?>	

			</div>

		</div>

	</div>

</div>

<!--=====================================
VENTANA MODAL PARA CHECKOUT
======================================-->

<div id="modalCheckout" class="modal fade modalFormulario" role="dialog">


	
		 <div class="modal-content modal-dialog">
		 	
			<div class="modal-body modalTitulo">
				
				<h3 class="backColor">REALIZAR PAGO</h3>

				<button type="button" class="close" data-dismiss="modal">&times;</button>

				<div class="contenidoCheckout">

					<?php

					$respuesta = ControladorCarrito::ctrMostrarTarifas();

					echo '<input type="hidden" id="tasaImpuesto" value="'.$respuesta["impuesto"].'">
						  <input type="hidden" id="envioNacional" value="'.$respuesta["envioNacional"].'">
					      <input type="hidden" id="envioInternacional" value="'.$respuesta["envioInternacional"].'">
					      <input type="hidden" id="tasaMinimaNal" value="'.$respuesta["tasaMinimaNal"].'">
					      <input type="hidden" id="tasaMinimaInt" value="'.$respuesta["tasaMinimaInt"].'">
					      <input type="hidden" id="tasaPais" value="'.$respuesta["pais"].'">
					      <input type="hidden" id="idusuario" value="'.$_SESSION["id"].'">
					      <input type="hidden" id="idproducto" value="'.$_SESSION["id"].'">




					';

					?>

					<div class="formaPago row">

						<input type="hidden" id="idUsuario" value="<?php echo $_SESSION["id"]; ?>">
						
						<h4 class="text-center well text-muted text-uppercase">Elige la forma de pago</h4>

						<figure class="col-md-6 col-xs-6">
							
							<center>
								
								<input id="checkPago" type="radio" name="pago"  value="contraentrega" checked>

							</center>	
							
						
							<div>
							<center><h4>PAGO CONTRAENTREGA</h4></center>

	                         <center><i class="fa fa-hand-pointer-o fa-5x" aria-hidden="true"></i></center>
	                 
	                       </div>
							

						</figure>
					

						<figure class="col-md-6 col-xs-6">
							
							<center>
								
								<input id="checkPayu" type="radio" name="pago" 
								 value="payu">

							</center>

							<img src="<?php echo $url; ?>vistas/img/plantilla/payu.jpg" class="img-thumbnail">

						</figure>

						<!--<figure class="col-md-4 col-xs-4">
						
						<center>
							
							<input id="checkPaypal" type="radio" name="pago" value="paypal" checked>

						</center>	
						
						<img src="<?php echo $url; ?>vistas/img/plantilla/paypal.jpg" class="img-thumbnail">		

					</figure>-->

					</div>
					<br>
					
					

					<h4 class="text-center well text-muted text-uppercase">Información de envío</h4>

					
					<br>

					<div class="formEnvio row" id="formcontraentrega">
						
						<center><label class="control-label text-muted text-uppercase">Si la entrega es fuera de  Barranquilla Seleccione Municipio</label></center>
						<div class="col-xs-12 seleccionePais">						
		                  
						</div>
						<br>

						<div class="form-group row">
							<br>
							<div class="col-md-4">
		                    	<label class="control-label text-muted text-uppercase">Codigo de Asesor: </label>
		                   	</div>
		                   	<div class="col-md-8">
							<?php 
						
							if($_SESSION["tipo"]!="Asesor"){
							
		                     echo '<input type="text" id="comprarcod_asesor" name="comprarcod_asesor" class="form-control" value="'.$_SESSION["cod_asesor"].'" required>';
		                   		}
		                   		else{

		                   		$item3 = "id_asesor";
								$valor3 = $_SESSION["id_asesor"];

								$traerAsesor = ControladorUsuarios::ctrMostrarAsesor($item3, $valor3);

								$codigo = $traerAsesor["codigo_asesor"];
		                   		
		                   		echo '<input type="text" id="comprarcod_asesor" name="comprarcod_asesor" class="form-control"  value="'.$codigo.'" readonly>';

		                   		}
		                    ?>
		                   
		                
		                 	</div>
		                </div>

		                <div class="form-group row">
		                    <div class="col-md-4">
		                    	<label class="control-label text-muted text-uppercase" >Direccion: </label>
		                    </div>
		                    <div class="col-md-8">
		                     <input type="text" id="comprardireccion" name="comprardireccion" class="form-control" value="<?php echo $_SESSION["direccion"]; ?>" placeholder="Ingrese Direccion"  readonly>
		                 	</div>
		                </div>

		                <div class="form-group row">
		                    <div class="col-md-4">
		                    	<label class="control-label text-muted text-uppercase" >Barrio: </label>
		                    </div>
		                    <div class="col-md-8">
		                     <input type="text" id="comprarbarrio" name="comprarbarrio" class="form-control" value="<?php echo $_SESSION["barrio"]; ?>" readonly placeholder="Ingrese Barrio">
		                 	</div>
		                </div>
		                <div class="form-group row">
		                    <div class="col-md-4">
		                    	<label class="control-label text-muted text-uppercase">Celular: </label>
		                    </div>
		                    <div class="col-md-8">
		                     <input type="text" id="comprarcelular" name="comprarcelular" class="form-control" value="<?php echo $_SESSION["celular"]; ?>" placeholder="Ingrese celular" readonly>
		                 	</div>
		                </div>
		                  <div class="form-group row">
		                    <div class="col-md-4">
		                    	<label class="control-label text-muted text-uppercase">Email: </label>
		                    </div>
		                    <div class="col-md-8">
		                     <input type="text" id="compraremail" name="compraremail" class="form-control" value="<?php echo $_SESSION["email"]; ?>" readonly>
		                 	</div>
		                </div>

					</div>



					  <div class="form-group row  divpayu" style="display:none">


		                  
		                 <h5 class="text-center  text-uppercase">Porfavor al pagar con Payu Tiene que Rellenar sus datos de manera Manual en el siguiente formulario</h5>

		                 <center><label class="control-label text-muted text-uppercase">Para este metodo Tiene que seleccionar Region</label></center>
						<div class="col-xs-12 seleccioneCiudad">						
		                  
						</div>
						<br>
						<br>
						<br>

						<div class="form-group row">
							<br>
							<div class="col-md-4">
		                    	<label class="control-label text-muted text-uppercase">Codigo de Asesor: </label>
		                   	</div>
		                   	<div class="col-md-8">
							<?php 
						
							if($_SESSION["tipo"]!="Asesor"){
							
		                     echo '<input type="text" id="comprarcod_asesor" name="comprarcod_asesor" class="form-control" value="'.$_SESSION["cod_asesor"].'" required>';
		                   		}
		                   		else{

		                   		$item3 = "id_asesor";
								$valor3 = $_SESSION["id_asesor"];

								$traerAsesor = ControladorUsuarios::ctrMostrarAsesor($item3, $valor3);

								$codigo = $traerAsesor["codigo_asesor"];
		                   		
		                   		echo '<input type="text" id="comprarcod_asesor" name="comprarcod_asesor" class="form-control"  value="'.$codigo.'" readonly>';

		                   		}
		                    ?>
		                   
		                
		                 	</div>
		                </div>

						<div class="form-group row">
		                    <div class="col-md-4">
		                    	<label class="control-label text-muted text-uppercase" >Direccion: </label>
		                    </div>
		                    <div class="col-md-8">
		                     <input type="text" id="comprardireccion" name="comprardireccion" class="form-control" value="<?php echo $_SESSION["direccion"]; ?>" placeholder="Ingrese Direccion"  readonly>
		                 	</div>
		                </div>

		                 <div class="form-group row">
		                    <div class="col-md-4">
		                    	<label class="control-label text-muted text-uppercase">Celular: </label>
		                    </div>
		                    <div class="col-md-8">
		                     <input type="text" id="comprarcelular" name="comprarcelular" class="form-control" value="<?php echo $_SESSION["celular"]; ?>" placeholder="Ingrese celular" readonly>
		                 	</div>
		                </div>

		                <div class="form-group row">
		                    <div class="col-md-4">
		                    	<label class="control-label text-muted text-uppercase">Email: </label>
		                    </div>
		                    <div class="col-md-8">
		                     <input type="text" id="comprarcelular" name="compraremail" class="form-control" value="<?php echo $_SESSION["email"]; ?>" placeholder="Ingrese celular" readonly>
		                 	</div>
		                </div>

		                 

		            </div>


					<div class="listaProductos row">
						
						<h4 class="text-center well text-muted text-uppercase">Productos a comprar</h4>

						<table class="table table-striped tablaProductos">
							
							 <thead>
							 	
								<tr>		
									<th>Producto</th>
									<th>Cantidad</th>
									<th>Precio</th>
								</tr>

							 </thead>

							 <tbody>
							 	


							 </tbody>

						</table>

						<div class="col-sm-6 col-xs-12 pull-right">
							
							<table class="table table-striped tablaTasas">
								
								<tbody>
									
									<tr>
										<td>Subtotal</td>	
										<td><span class="cambioDivisa">COP</span> $<span class="valorSubtotal" valor="0">0</span></td>	
									</tr>

									<tr>
										<td>Envío</td>	
										<td><span class="cambioDivisa">COP</span> $<span class="valorTotalEnvio" valor="0">0</span></td>	
									</tr>

									<tr>
										<td>Impuesto</td>	
										<td><span class="cambioDivisa">COP</span> $<span class="valorTotalImpuesto" valor="0">0</span></td>	
									</tr>

									<tr>
										<td><strong>Total</strong></td>	
										<td><strong><span class="cambioDivisa">COP</span> $<span class="valorTotalCompra" valor="0">0</span></strong></td>	
									</tr>

								</tbody>	

							</table>

							<!-- <div class="divisa">

							 	<select class="form-control" id="cambiarDivisa" name="divisa" readonly>
							 		
								

							 	</select>	

							 	<br>

							 </div>
							-->

						</div>

						<div class="clearfix"></div>
					
						<form class="formPayu"   style="display:none">
					 
						<input name="merchantId" type="hidden" value=""/>
						<input name="accountId" type="hidden" value=""/>
						<input name="description" type="hidden" value=""/>
						<input name="referenceCode" type="hidden" value=""/>	
						<input name="amount" type="hidden" value=""/>
						<input name="tax" type="hidden" value=""/>
						<input name="taxReturnBase" type="hidden" value=""/>
						<input name="shipmentValue" type="hidden" value=""/>
						<input name="currency" type="hidden" value=""/>
						<input name="lng" type="hidden" value="es"/>
						<input name="confirmationUrl" type="hidden" value="" />
						<input name="responseUrl" type="hidden" value=""/>
						<input name="declinedResponseUrl" type="hidden" value=""/>
						<input name="displayShippingInformation" type="hidden" value=""/>
						<input name="test" type="hidden" value="" />
						<input name="signature" type="hidden" value=""/>
						
						<!--<input name="shippingAddress" type="hidden" value="portal del prado Carrera #46"/>

						<input name="shippingCity" type="hidden" value="Bogota"/>
						<input name="shippingCountry" type="hidden" value="CO"/>-->

					  <input name="Submit" class="btn btn-block btn-lg btn-default backColor" type="submit"  value="PAGAR CON PAYU" >
					</form>
									
						
						<button class="btn btn-block btn-lg btn-default backColor btnPagar">REALIZAR COMPRA</button>


						<button class="btn btn-block btn-lg btn-default backColor btnPagarPaypal">PAGARPAYPAL</button>
						
					</div>

				</div>

			</div>

			<div class="modal-footer">
	      	
	      	</div>

		</div>


	

</div>
