
<?php

$url = Ruta::ctrRuta();
$servidor = Ruta::ctrRutaServidor();


if(!isset($_SESSION["validarSesion"])){

	echo '<script>
	
		window.location = "'.$url.'";

	</script>';

	exit();

}



?>

<!--=====================================
BREADCRUMB PERFIL
======================================-->

<div class="container-fluid well well-sm">
	
	<div class="container">
		
		<div class="row">
			
			<ul class="breadcrumb fondoBreadcrumb text-uppercase">
				
				<li><a href="<?php echo $url;  ?>">INICIO</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>

			</ul>

		</div>

	</div>

</div>

<!--=====================================
SECCIÓN PERFIL
======================================-->

<div class="container-fluid">

	<div class="container">

		<ul class="nav nav-tabs">
		  
	  		<li class="active">	  			
			  	<a data-toggle="tab" href="#productos">
			  	<i class="fa fa-list-ul"></i> MIS PRODUCTOS</a>
	  		</li>

	  		<li> 				
		  		<a data-toggle="tab" href="#datos">
		  		<i class="fa fa-gift"></i> MIS DATOS</a>
	  		</li>

	  		<li>				
	  			<a data-toggle="tab" href="#perfil">
	  			<i class="fa fa-user"></i> EDITAR PERFIL</a>
	  		</li>

	  		<!--<li>				
		 	 	<a href="<?php echo $url; ?>ofertas">
		 	 	<i class="fa fa-star"></i>	VER OFERTAS</a>
	  		</li>-->
		
		</ul>

		<div class="tab-content">

			<!--=====================================
			PESTAÑA Productos
			======================================-->

	  		<div id="productos" class="tab-pane fade in active">

				<div class="panel-group">

                    <br>
					<div class="box-header with-border">
         
	        			<button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarProducto">Agregar Producto</button> 
	        		</div>
	        		<br>


				  <?php

					$item = "id_proveedor";
					$valor = $_SESSION["id_proveedor"];
                                                        
					$productos = ControladorProveedor::ctrMostrarProductos($item, $valor);


					if(!$productos){

						echo '<div class="col-xs-12 text-center error404">
				               
				    		<h1><small>¡Vacio!</small></h1>
				    
				    		<h2>Aún no tienes Productos Registrados en la tienda</h2>

				  		</div>';

					}else{
                  
                  	

				  		foreach ($productos as $key => $value2) {
							
								echo '<div class="panel panel-default">
									    
									    <div class="panel-body">

											<div class="col-md-2">

												<figure>
												
													<img class="img-thumbnail" src="'.$servidor.$value2["portada_producto"].'">

												</figure>

											</div>
										
											<div class="col-md-2">
											  <div class="form-group">';
											  if($value2["estado_producto"]==1){
			                                                echo '<br><button type="button" class="btn btn-primary btn-lg disabled">Activo</button>';
											  }else{
											   echo '<br><button type="button" class="btn btn-danger btn-lg disabled" readonly>Desactivado</button>';

												}


											  	echo '<h4 class=""><small>Registrado el '.substr($value2["fecha"],0,-8).'</small></h4>

												</div>
											</div>
											<div class="col-md-5">

												<h1><small>'.$value2["titulo_producto"].'</small></h1>

												<p>Titular: '.$value2["titular_producto"].'</p>
												<p>Descripcion: '.$value2["descripcion_producto"].'</p>
												
																
											</div>

											<div class="col-md-2">
											    <br>
												<p>Precio: '.$value2["precio_producto"].'</p>
												<p>Vistas: '.$value2["vistas_producto"].'</p>
												<p>N° Venta: '.$value2["ventas_producto"].'</p>
												<p>Cantidad: '.$value2["cantidad_producto"].'</p>
												
																
											</div>';



                                        $item3 = "ruta";
					                    $valor3 = $value2["ruta_producto"];

					                    $cabeceras = ControladorCabeceras::ctrMostrarCabeceras($item3, $valor3);

											echo '<input type="hidden" class="form-control "  value="'.$value2["id"].'"  id="idProducto" name="idProducto">
											<input type="hidden" class="form-control "  value="'.$value2["imgOferta"].'"  id="imgOferta" name="imgOferta">
											<input type="hidden" class="form-control "  value="'.$value2["ruta"].'"  id="rutaCabecera" name="rutaCabecera">
											<input type="hidden" class="form-control "  value="'.$cabeceras["portada"].'"  id="imgPortada" name="imgPortada">
											<input type="hidden" class="form-control "  value="'.$value2["portada"].'"  id="imgPrincipal" name="imgPrincipal">';

										

											echo '<br><div class="col-md-1">

                                                        <div class="form-group row">  

												         
												          
												          <button class="btn btn-primary editarProducto" id="editarProducto" data-toggle="modal" data-target="#modalEditarProducto">Editar</button> </div>
												          <br>
                                                          <div class="row">
												          <button type="button" class="btn btn-danger eliminarProducto" id="eliminarProducto" name="eliminarProducto">Eliminar</button>
												          </div>';


                                          

													 // $eliminarProducto = new ControladorProductos();
													  //$eliminarProducto -> ctrEliminarProducto();



												                                                    

                                                     echo '</div>

												</div>
											</div>';

							}
						
						
					}
				?>
				  
				

				</div>

		  	</div>

		  	<!--=====================================
			PESTAÑA DESEOS
			======================================-->

		  	<div id="datos" class="tab-pane fade">
			  	<br>
			    <div class="container">	
			    	<form method="post">
			    	
			   		
					<?php

					$item = $_SESSION["id_proveedor"];

						echo '<input type="hidden" class="form-control "  value="'.$item.'"  id="idProveedor" name="idProveedor">
						    <div class="col-md-6  col-xs-12">
						        	<center><h3>Datos Proveedor</h3></center>';
						if($_SESSION["empresa"]!=null || $_SESSION["descripcion"]!=null|| $_SESSION["tipo_productos"]!=null){
							echo  '<br>
						        <br>
						        	<div class="col-md-4">
			                            <label class="control-label text-muted text-uppercase"> Empresa:</label>
			                            </div>
			                        <div class="col-md-8">
			                           <p> '.$_SESSION["empresa"].'</p>
			                        </div>
			                        <div class="col-md-4">
			                            <label class="control-label text-muted text-uppercase"> Descripcion:</label>
			                            </div>
			                        <div class="col-md-8">
			                           <p> '.$_SESSION["descripcion"].'</p>
			                        </div>
			                        <div class="col-md-4">
			                            <label class="control-label text-muted text-uppercase"> Tipo de Productos:</label>
			                            </div>
			                        <div class="col-md-8">
			                           <p> '.$_SESSION["tipo_productos"].' </p>
			                        </div>';
						    }  else{
						    	echo '<br><br><center><h4>Falta Actualizar Datos<h4></center>';

						    		}

						        echo '</div><div class="col-md-6  col-xs-12">
						           <br>
						           <br>
						           <br>
									<div class="form-group row">
											<div class="col-md-4">
			                                     <label class="control-label text-muted text-uppercase"> Empresa:</label>
			                                </div>
			                                <div class="col-md-8">
												<div class="input-group">
											
													<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
													<input type="text" class="form-control "  value="'.$_SESSION["empresa"].'"  id="editarempresa1" name="editarempresa1">
												</div>
											</div>
										</div>

										<br>

										<div class="form-group row">
											<div class="col-md-4">
			                                     <label class="control-label text-muted text-uppercase">N°  Descripcion:</label>
			                                </div>
			                                <div class="col-md-8">
												<div class="input-group">
											
													<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
													<input type="text" class="form-control "  value="'.$_SESSION["descripcion"].'"  id="editardescripcion1" name="editardescripcion1">
												</div>
											</div>
										</div>

										<br>
										<div class="form-group row">
											<div class="col-md-4">
			                                     <label class="control-label text-muted text-uppercase">N°  Tipo Producto:</label>
			                                </div>
			                                <div class="col-md-8">
												<div class="input-group">
											
													<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
													<input type="text" class="form-control "  value="'.$_SESSION["tipo_productos"].'"  id="editartipo_productos1" name="editartipo_productos1">
												</div>
											</div>
										</div>
										<br>
										<button type="submit" class="btn btn-succes backColor btn-md pull-left">Actualizar Datos</button>
									</div>
								';
						    

						  


						?>
					
	                 
						

								<?php

									$actualizarDatos = new ControladorProveedor();
									$actualizarDatos->ctrActualizarDatos();

								

								?>	
				
                       

					</form>


				</div>


		  	</div>

			<!--=====================================
			PESTAÑA PERFIL
			======================================-->
		  	
		  	<div id="perfil" class="tab-pane fade">
		    	
				<div class="row">
					
					<form method="post" enctype="multipart/form-data">
					
						<div class="col-md-3 col-sm-4 col-xs-12 text-center">
							

							<figure id="imgPerfil1">
								
							<?php

							echo '<input type="hidden" value="'.$_SESSION["id_proveedor"].'" id="idProveedor1" name="idProveedor1">
							      <input type="hidden" value="'.$_SESSION["password"].'" name="passProveedor1" id="passProveedor1">
							      <input type="hidden" value="'.$_SESSION["foto"].'" name="fotoProveedor1" id="fotoProveedor1">
							      <input type="hidden" value="'.$_SESSION["modo"].'" name="modoProveedor1" id="modoProveedor1">';


							if($_SESSION["modo"] == "directo" ){

								if($_SESSION["foto"] != ""){

									echo '<img src="'.$url.$_SESSION["foto"].'" class="img-thumbnail">';

								}else{

									echo '<img src="'.$servidor.'vistas/img/usuarios/default/anonymous.png" class="img-thumbnail">';

								}
					

							}else{

								echo '<img src="'.$_SESSION["foto"].'" class="img-thumbnail">';
							}		

							?>

							</figure>

							<br>

							<div id="subirImagen1">
								
								<input type="file" class="form-control" id="datosImagen1" name="datosImagen1">

								<img class="previsualizar1">

							</div>
							<br>

							<?php

							if($_SESSION["modo"] == "directo"){
							
							echo '<button type="button" class="btn btn-default" id="btnCambiarFoto1" name="btnCambiarFoto1">
									
									Cambiar foto de perfil
									
									</button>';

							}

							?>

							

						</div>	

						<div class="col-md-9 col-sm-8 col-xs-12">

						<br>
							
						<?php

						/*echo '<input type="hidden" class="form-control"  value="'.$_SESSION["tipo"].'" name="editartipo1" id="editartipo1" readonly>';*/

						if($_SESSION["modo"] != "directo"){

							echo '<div class="form-group row">
										<div class="col-md-4">
											<label class="control-label text-muted text-uppercase">Nombre:</label>
										</div>
										<div class="col-md-8">
											<div class="input-group">
									
												<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
												<input type="text" class="form-control"  value="'.$_SESSION["nombre"].'" name="editarNombre1" id="editarNombre1" readonly >
											</div>
										</div>

									</div>

									<br>
									<div class="form-group row">
										<div class="col-md-4">
											<label class="control-label text-muted text-uppercase">Correo electrónico:</label>
										</div>
										<div class="col-md-8">
											<div class="input-group">
										
												<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
												<input type="text" class="form-control"  value="'.$_SESSION["email"].'" ame="editarCorreo1" id="editarCorreo1"  readonly>
											</div>
										</div>

									</div>

									<br>
                                    <div class="form-group row">
										<div class="col-md-4">
											<label class="control-label text-muted text-uppercase">Modo de registro:</label>
										</div>
										<div class="col-md-8">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-'.$_SESSION["modo"].'"></i></span>
												<input type="text" class="form-control text-uppercase"  value="'.$_SESSION["modo"].'" readonly >
											</div>
									    </div>

									</div>
									<br>
									<div class="form-group row">
										<div class="col-md-4">
		                                     <label class="control-label text-muted text-uppercase">N°  Celular:</label>
		                                </div>
		                                <div class="col-md-8">
											<div class="input-group">
												<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
												<input type="text" class="form-control text-uppercase"  value="'.$_SESSION["celular"].'"  id="editarcelular1" name="editarcelular1">
											</div>
										</div>

									</div>

									<br>
									<div class="form-group row">
										<div class="col-md-4">
		                                     <label class="control-label text-muted text-uppercase">Direccion:</label>
		                                </div>
		                                <div class="col-md-8">
											<div class="input-group">
												<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
												<input type="text" class="form-control "  value="'.$_SESSION["direccion"].'" id="editardireccion1" name="editardireccion1" >
											</div>
										</div>

									</div>
									<br>
									<div class="form-group row">
										<div class="col-md-4">
		                                     <label class="control-label text-muted text-uppercase">Barrio:</label>
		                                </div>
		                                <div class="col-md-8">
											<div class="input-group">
										
												<span class="input-group-addon"><i class="glyphicon glyphicon-road"></i></span>
												<input type="text" class="form-control "  id="editarmunicipio1" name="barrio" value="'.$_SESSION["municipio"].'" >

											</div>
										</div>
									</div>
									<br>
									<button type="submit" class="btn btn-default backColor btn-md pull-left">Actualizar Datos</button>

									';

		

						}else{

							echo '<div class="form-group row">
										<div class="col-md-3">
											<label class="control-label text-muted text-uppercase" for="editarNombre">Cambiar Nombre:</label>
										</div>
										<div class="col-md-8">
											<div class="input-group">
										
												<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
												<input type="text" class="form-control" id="editarNombre1" name="editarNombre1" value="'.$_SESSION["nombre"].'">

											</div>
										</div>
								   </div>

								<br>
								<div class="form-group row">
									<div class="col-md-3">

										<label class="control-label text-muted text-uppercase" for="editarEmail">Correo Electrónico:</label>
									</div>
	                                <div class="col-md-8">
										<div class="input-group">
										
												<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
												<input type="text" class="form-control" id="editarEmail1" name="editarEmail1" value="'.$_SESSION["email"].'" readonly>

										</div>
									</div>
								</div>

								<br>
                                <div class="form-group row">
									<div class="col-md-3">
										<label class="control-label text-muted text-uppercase" for="editarPassword">Contraseña:</label>
                                    </div>
                                    <div class="col-md-8">
										<div class="input-group">
										
												<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
												<input type="text" class="form-control" id="editarPassword1" name="editarPassword1" placeholder="Escribe la nueva contraseña" >

										</div>

									</div>
								</div>

								<br>
                                 <div class="form-group row">
									<div class="col-md-3">
										<label class="control-label text-muted text-uppercase" for="editarcelular">Celular:</label>
									</div>
									<div class="col-md-8">
										<div class="input-group">
										
												<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
												<input type="text" class="form-control" id="editarcelular1" name="editarcelular1" placeholder="Escribe la N° Celular"  value="'.$_SESSION["celular"].'">

										</div>
									</div>
								</div>

								<br>

                                 <div class="form-group row">
									<div class="col-md-3">
										<label class="control-label text-muted text-uppercase" for="editardireccion">Direccion:</label>
									</div>
									<div class="col-md-8">

										<div class="input-group">
										
												<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
												<input type="text" class="form-control" id="editardireccion1" name="editardireccion1" placeholder="Escribe la nueva Direccion" value="'.$_SESSION["direccion"].'">

										</div>
									</div>
							    </div>

								<br>

								<div class="form-group row">
									<div class="col-md-3">

										<label class="control-label text-muted text-uppercase" for="editarbarrio">Municipio:</label>
									</div>
									<div class="col-md-8">
										<div class="input-group">
										
												<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
												<input type="text" class="form-control" id="editarmunicipio1" name="editarmunicipio1" placeholder="Escribe la nuevo Barrio" value="'.$_SESSION["municipio"].'">

										</div>
									</div>
								</div>

								<br>

								<div class="form-group row">
									<div class="col-md-3">

										<label class="control-label text-muted text-uppercase" for="editarempresa">Nombre de la Empresa:</label>
									</div>
									<div class="col-md-8">
										<div class="input-group">
										
												<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
												<input type="text" class="form-control" id="editarempresa1" name="editarempresa1" placeholder="Nombre de Empresa" value="'.$_SESSION["empresa"].'">

										</div>
									</div>
								</div>

								<br>

								<button type="submit" class="btn btn-default backColor btn-md pull-left">Actualizar Datos</button>';

						}

						?>

						</div>

						<?php

							$actualizarPerfil = new ControladorProveedor();
							$actualizarPerfil->ctrActualizarPerfil();

						

						?>					

					</form>

					<button class="btn btn-danger btn-md pull-right" id="eliminarUsuario" readonly>Eliminar cuenta</button>

					<?php

							

						?>	

				</div>

		  	</div>


		</div>

	</div>

</div>


<div id="modalAgregarProducto" class="modal fade" role="dialog">
  
   <div class="modal-dialog">
     
     <div class="modal-content">
       
       <!-- <form role="form" method="post" enctype="multipart/form-data"> -->
         
         <!--=====================================
        CABEZA DEL MODAL
        ======================================-->
        <div class="modal-header" style="background:#E50039; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <center><h4 class="modal-title">Agregar producto</h4></center>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!--=====================================
            ENTRADA PARA EL TÍTULO
            ======================================-->

            <div class="form-group">
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-product-hunt"></i></span> 

                  <input type="text" class="form-control input-lg validarProducto tituloProducto"  placeholder="Ingresar título producto" id="tituloProducto" name="tituloProducto">

                </div>

            </div>

            <!--=====================================
            ENTRADA PARA LA RUTA DEL PRODUCTO
            ======================================-->

            <div class="form-group">
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-link"></i></span> 

                  <input type="text" class="form-control input-lg rutaProducto" placeholder="Ruta url del producto" readonly>

                </div>

            </div>

          
            

            <!--=====================================
            ENTRADA PARA AGREGAR MULTIMEDIA
            ======================================-->

            <div class="form-group agregarMultimedia"> 
             
              <!--=====================================
              SUBIR MULTIMEDIA DE PRODUCTO FÍSICO
              ======================================-->
              
              <div class="multimediaFisica needsclick dz-clickable" id="multimediaFisica" >

                <div class="dz-message needsclick">
                  
                  Arrastrar o dar click para subir imagenes.

                </div>

              </div>

            </div>




            <!--=====================================
            AGREGAR DETALLES 
            ======================================-->  
     

            <div class="detallesFisicos" >
              
              <div class="panel"><center>DETALLES</center></div>

              <!-- TALLA -->
       
              
		                <div class="form-group row">

		                  <div class="col-xs-3">
		                    <input class="form-control input-lg" type="text" value="Talla" readonly>
		                  </div>

		                  <div class="col-xs-9">
		                    <input class="form-control  tagsInput detalleTalla" id="detalleTalla" name="detalleTalla" data-role="tagsinput" type="text" placeholder="Separe valores con coma">
		                  </div>

		              </div>

		              <!-- COLOR -->

		              <div class="form-group row">

		                <div class="col-xs-3">
		                  <input class="form-control input-lg" type="text" value="Color" readonly>
		                </div>

		                <div class="col-xs-9">
		                    <input class="form-control input-lg tagsInput detalleColor" id="detalleColor" name="detalleColor" data-role="tagsinput" type="text" placeholder="Separe valores con coma">
		                </div>

		              </div>

		              <!-- MARCA -->

		              <div class="form-group row">

		                <div class="col-xs-3">
		                  <input class="form-control input-lg" type="text" value="Marca" readonly>
		                </div>

		                <div class="col-xs-9">
		                    <input class="form-control input-lg tagsInput detalleMarca" id="detalleMarca" name="detalleMarca" data-role="tagsinput" type="text" placeholder="Separe valores con coma">
		                </div>

		              </div>

		            </div> 
		  


           <!--=====================================
            AGREGAR CATEGORÍA
            ======================================-->

            <div class="form-group">
                
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-th"></i></span> 

                  <select class="form-control input-lg seleccionarCategoria">
                  
                    <option value="">Selecionar categoría</option>

                    <?php

                    $item = null;
                    $valor = null;

                    $categorias = ControladorProveedor::ctrMostrarCategorias($item, $valor);

                    foreach ($categorias as $key => $value) {
                      
                      echo '<option value="'.$value["id_categoria"].'">'.$value["categoria_categoria"].'</option>';
                    }

                    ?>

                  </select>

                </div>

            </div>

            <!--=====================================
            AGREGAR SUBCATEGORÍA
            ======================================-->

            <div class="form-group  entradaSubcategoria" style="display:none">
              
               <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-th"></i></span> 

                  <select class="form-control input-lg seleccionarSubCategoria">

                  </select>

                </div>

            </div>

           <!--=====================================
            AGREGAR DESCRIPCIÓN
            ======================================-->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-pencil"></i></span> 

                <textarea type="text" maxlength="320" rows="3" class="form-control input-lg descripcionProducto" placeholder="Ingresar descripción producto"></textarea>

              </div>

            </div>

            <!--=====================================
            AGREGAR PALABRAS CLAVES
            ======================================-->

            <div class="form-group">
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-key"></i></span> 

                  <input type="text" class="form-control input-lg tagsInput pClavesProducto" data-role="tagsinput"  placeholder="Ingresar palabras claves">

                </div>

            </div>

            <!--=====================================
            AGREGAR FOTO DE PORTADA
            ======================================-->

            <div class="form-group">
              
              <div class="panel">SUBIR FOTO PORTADA</div>

              <input type="file" class="fotoPortada">

              <p class="help-block">Tamaño recomendado 1280px * 720px <br> Peso máximo de la foto 2MB</p>

              <img src="vistas/img/cabeceras/default/default.jpg" class="img-thumbnail previsualizarPortada" width="100%">

            </div>

            <!--=====================================
            AGREGAR FOTO DE MULTIMEDIA
            ======================================-->

            <div class="form-group">
                
              <div class="panel">SUBIR FOTO PRINCIPAL DEL PRODUCTO</div>

              <input type="file" class="fotoPrincipal">

              <p class="help-block">Tamaño recomendado 400px * 450px <br> Peso máximo de la foto 2MB</p>

              <img src="vistas/img/productos/default/default.jpg" class="img-thumbnail previsualizarPrincipal" width="200px">

            </div>

            <!--=====================================
            AGREGAR PRECIO, PESO Y ENTREGA
            ======================================-->

            <div class="form-group row">
               
              <!-- PRECIO -->

              <div class="col-md-4 col-xs-12">
  
                <div class="panel">PRECIO PROVEEDOR</div>
                
                <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span> 

                  <input type="number" class="form-control input-lg precio" min="0" step="any">

                </div>

              </div>

              <!-- PESO -->

              <div class="col-md-4 col-xs-12">
  
                <div class="panel">PESO</div>
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-balance-scale"></i></span> 

                  <input type="number" class="form-control input-lg peso" min="0" step="any" value="0">

                </div>

              </div>

              <!-- ENTREGA -->

              <div class="col-md-4 col-xs-12">
  
                <div class="panel">DÍAS DE ENTREGA</div>
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-truck"></i></span> 

                  <input type="number" class="form-control input-lg entrega" min="0" value="0">

                </div>

              </div>

            </div>


            <div class="form-group row">
               
              <!-- PRECIO -->

             

              <!-- PESO -->

              <div class="col-md-4 col-xs-12">
  
                <div class="panel">CANTIDAD </div>
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-calculator"></i></span> 

                  <input type="number" class="form-control input-lg cantidad" min="0" step="any" value="0">

                </div>

              </div>

               <div class="col-md-4 col-xs-12">
  
                <input type="hidden" class="form-control input-lg precio_venta" min="0" step="any">
                <!--<div class="panel">PRECIO VENTA</div>
                
                <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span> 

                 

                </div>-->

              </div>


            </div>


            <!--=====================================
            AGREGAR OFERTAS
            ======================================-->

            <div class="form-group">
              
              <select class="form-control input-lg selActivarOferta">
                
                <option value="">No tiene oferta</option>
                <option value="oferta">Activar oferta</option>
               
              </select>

            </div>

            <div class="datosOferta" style="display:none">
            
              <!--=====================================
              VALOR OFERTAS
              ======================================-->

              <div class="form-group row">
                  
                <div class="col-xs-6">

                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="ion ion-social-usd"></i></span> 
                    
                    <input class="form-control input-lg valorOferta precioOferta" tipo="oferta" type="number" value="0"   min="0" placeholder="Precio">

                  </div>

                </div>

                <div class="col-xs-6">
                     
                  <div class="input-group">
                       
                    <input class="form-control input-lg valorOferta descuentoOferta" tipo="descuento" type="number" value="0"  min="0" placeholder="Descuento">
                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>

                  </div>

                </div>

              </div>

              <!--=====================================
              FECHA FINALIZACIÓN OFERTA
              ======================================-->

             <div class="form-group">
                  
	                <div class="input-group date">
	                      
	                  <input type='text' class="form-control datepicker input-lg valorOferta finOferta">
	                      
	                  <span class="input-group-addon">
	                          
	                      <span class="glyphicon glyphicon-calendar"></span>
	                      
	                  </span>
	                 
	                </div>
              
              </div>
              <!--=====================================
              FOTO OFERTA
              ======================================-->

              <div class="form-group">
                
                <div class="panel">SUBIR FOTO OFERTA</div>

                <input type="file" class="fotoOferta valorOferta">

                <p class="help-block">Tamaño recomendado 640px * 430px <br> Peso máximo de la foto 2MB</p>

                <img src="vistas/img/ofertas/default/default.jpg" class="img-thumbnail previsualizarOferta" width="100px">

              </div>

            </div>
          
          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">
  
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="button" class="btn btn-primary guardarProducto" id="guardarProducto" name="guardarProducto">Guardar producto</button>

        </div>

       <!-- </form> -->

     </div>

   </div>

</div>


<div id="modalEditarProducto" class="modal fade" role="dialog">
  
   <div class="modal-dialog">
     
     <div class="modal-content">
       
       <!-- <form role="form" method="post" enctype="multipart/form-data"> -->
         
         <!--=====================================
        CABEZA DEL MODAL
        ======================================-->
        <div class="modal-header" style="background:#E50039; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <center><h4 class="modal-title">Editar producto</h4></center>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!--=====================================
            ENTRADA PARA EL TÍTULO
            ======================================-->

            <div class="form-group">
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-product-hunt"></i></span> 

                  <input type="text" class="form-control input-lg validarProducto tituloProducto"  placeholder="Ingresar título producto" id="tituloProducto" name="tituloProducto">

                </div>

            </div>

            <!--=====================================
            ENTRADA PARA LA RUTA DEL PRODUCTO
            ======================================-->

            <div class="form-group">
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-link"></i></span> 

                  <input type="text" class="form-control input-lg rutaProducto" placeholder="Ruta url del producto" readonly>

                </div>

            </div>

          
            

            <!--=====================================
            ENTRADA PARA AGREGAR MULTIMEDIA
            ======================================-->

            <div class="form-group agregarMultimedia"> 

              <!--=====================================
              SUBIR MULTIMEDIA DE PRODUCTO VIRTUAL
              ======================================-->
              
              <div class="input-group multimediaVirtual" style="display:none">
                
                <span class="input-group-addon"><i class="fa fa-youtube-play"></i></span> 
              
                 <input type="text" class="form-control input-lg multimedia" placeholder="Ingresar código video youtube">

              </div>

              <!--=====================================
              SUBIR MULTIMEDIA DE PRODUCTO FÍSICO
              ======================================-->
              
              <div class="multimediaFisica needsclick dz-clickable" id="multimediaFisica" style="display:none">

                <div class="dz-message needsclick">
                  
                  Arrastrar o dar click para subir imagenes.

                </div>

              </div>

            </div>




            <!--=====================================
            AGREGAR DETALLES 
            ======================================-->  
     

            <div class="detallesFisicos" style="display:none">
              
              <div class="panel"><center>DETALLES</center></div>

              <!-- TALLA -->
       
              
		                <div class="form-group row">

		                  <div class="col-xs-3">
		                    <input class="form-control input-lg" type="text" value="Talla" readonly>
		                  </div>

		                  <div class="col-xs-9">
		                    <input class="form-control  tagsInput detalleTalla" id="detalleTalla" data-role="tagsinput" type="text" placeholder="Separe valores con coma">
		                  </div>

		              </div>

		              <!-- COLOR -->

		              <div class="form-group row">

		                <div class="col-xs-3">
		                  <input class="form-control input-lg" type="text" value="Color" readonly>
		                </div>

		                <div class="col-xs-9">
		                    <input class="form-control input-lg tagsInput detalleColor" id="detalleColor"data-role="tagsinput" type="text" placeholder="Separe valores con coma">
		                </div>

		              </div>

		              <!-- MARCA -->

		              <div class="form-group row">

		                <div class="col-xs-3">
		                  <input class="form-control input-lg" type="text" value="Marca" readonly>
		                </div>

		                <div class="col-xs-9">
		                    <input class="form-control input-lg tagsInput detalleMarca" id="detalleMarca" data-role="tagsinput" type="text" placeholder="Separe valores con coma">
		                </div>

		              </div>

		            </div> 
		  


           <!--=====================================
            AGREGAR CATEGORÍA
            ======================================-->

            <div class="form-group">
                
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-th"></i></span> 

                  <select class="form-control input-lg seleccionarCategoria">
                  
                    <option value="">Selecionar categoría</option>

                    <?php

                    $item = null;
                    $valor = null;

                    $categorias = ControladorProveedor::ctrMostrarCategorias($item, $valor);

                    foreach ($categorias as $key => $value) {
                      
                      echo '<option value="'.$value["id_categoria"].'">'.$value["categoria_categoria"].'</option>';
                    }

                    ?>

                  </select>

                </div>

            </div>

            <!--=====================================
            AGREGAR SUBCATEGORÍA
            ======================================-->

            <div class="form-group  entradaSubcategoria" style="display:none">
              
               <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-th"></i></span> 

                  <select class="form-control input-lg seleccionarSubCategoria">

                  </select>

                </div>

            </div>

           <!--=====================================
            AGREGAR DESCRIPCIÓN
            ======================================-->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-pencil"></i></span> 

                <textarea type="text" maxlength="320" rows="3" class="form-control input-lg descripcionProducto" placeholder="Ingresar descripción producto"></textarea>

              </div>

            </div>

            <!--=====================================
            AGREGAR PALABRAS CLAVES
            ======================================-->

            <div class="form-group">
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-key"></i></span> 

                  <input type="text" class="form-control input-lg tagsInput pClavesProducto" data-role="tagsinput"  placeholder="Ingresar palabras claves">

                </div>

            </div>

            <!--=====================================
            AGREGAR FOTO DE PORTADA
            ======================================-->

            <div class="form-group">
              
              <div class="panel">SUBIR FOTO PORTADA</div>

              <input type="file" class="fotoPortada">

              <p class="help-block">Tamaño recomendado 1280px * 720px <br> Peso máximo de la foto 2MB</p>

              <img src="vistas/img/cabeceras/default/default.jpg" class="img-thumbnail previsualizarPortada" width="100%">

            </div>

            <!--=====================================
            AGREGAR FOTO DE MULTIMEDIA
            ======================================-->

            <div class="form-group">
                
              <div class="panel">SUBIR FOTO PRINCIPAL DEL PRODUCTO</div>

              <input type="file" class="fotoPrincipal">

              <p class="help-block">Tamaño recomendado 400px * 450px <br> Peso máximo de la foto 2MB</p>

              <img src="vistas/img/productos/default/default.jpg" class="img-thumbnail previsualizarPrincipal" width="200px">

            </div>

            <!--=====================================
            AGREGAR PRECIO, PESO Y ENTREGA
            ======================================-->

            <div class="form-group row">
               
              <!-- PRECIO -->

              <div class="col-md-4 col-xs-12">
  
                <div class="panel">PRECIO PROVEEDOR</div>
                
                <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span> 

                  <input type="number" class="form-control input-lg precio" min="0" step="any">

                </div>

              </div>

              <!-- PESO -->

              <div class="col-md-4 col-xs-12">
  
                <div class="panel">PESO</div>
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-balance-scale"></i></span> 

                  <input type="number" class="form-control input-lg peso" min="0" step="any" value="0">

                </div>

              </div>

              <!-- ENTREGA -->

              <div class="col-md-4 col-xs-12">
  
                <div class="panel">DÍAS DE ENTREGA</div>
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-truck"></i></span> 

                  <input type="number" class="form-control input-lg entrega" min="0" value="0">

                </div>

              </div>

            </div>


            <div class="form-group row">
               
              <!-- PRECIO -->

              

              <!-- PESO -->

              <div class="col-md-4 col-xs-12">
  
                <div class="panel">CANTIDAD </div>
              
                <div class="input-group">
              
                  <span class="input-group-addon"><i class="fa fa-calculator"></i></span> 

                  <input type="number" class="form-control input-lg cantidad" min="0" step="any" value="0">

                </div>

              </div>
              <div class="col-md-4 col-xs-12">
  

                  <input type="hidden" class="form-control input-lg precio_venta" min="0" step="any" readonly>
                <!--<div class="panel">Hiden</div>
                
                <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span> 

                  <input type="number" class="form-control input-lg precio_venta" min="0" step="any" readonly>

                </div>-->

              </div>


            </div>


            <!--=====================================
            AGREGAR OFERTAS
            ======================================-->

            <div class="form-group">
              
              <select class="form-control input-lg selActivarOferta">
                
                <option value="">No tiene oferta</option>
                <option value="oferta">Activar oferta</option>
               
              </select>

            </div>

            <div class="datosOferta" style="display:none">
            
              <!--=====================================
              VALOR OFERTAS
              ======================================-->

              <div class="form-group row">
                  
                <div class="col-xs-6">

                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="ion ion-social-usd"></i></span> 
                    
                    <input class="form-control input-lg valorOferta precioOferta" tipo="oferta" type="number" value="0"   min="0" placeholder="Precio">

                  </div>

                </div>

                <div class="col-xs-6">
                     
                  <div class="input-group">
                       
                    <input class="form-control input-lg valorOferta descuentoOferta" tipo="descuento" type="number" value="0"  min="0" placeholder="Descuento">
                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>

                  </div>

                </div>

              </div>

              <!--=====================================
              FECHA FINALIZACIÓN OFERTA
              ======================================-->
			<div class="form-group">
                  
	                <div class="input-group date">
	                      
	                  <input type='text' class="form-control datepicker input-lg valorOferta finOferta">
	                      
	                  <span class="input-group-addon">
	                          
	                      <span class="glyphicon glyphicon-calendar"></span>
	                      
	                  </span>
	                 
	                </div>
              
              </div>

              <!--=====================================
              FOTO OFERTA
              ======================================-->

              <div class="form-group">
                
                <div class="panel">SUBIR FOTO OFERTA</div>

                <input type="file" class="fotoOferta valorOferta">

                <p class="help-block">Tamaño recomendado 640px * 430px <br> Peso máximo de la foto 2MB</p>

                <img src="vistas/img/ofertas/default/default.jpg" class="img-thumbnail previsualizarOferta" width="100px">

              </div>

            </div>
          
          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">
  
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="button" class="btn btn-primary guardarProducto" id="guardarProducto" name="guardarProducto">Guardar producto</button>

        </div>

       <!-- </form> -->

     </div>

   </div>

</div>


<div  class="modal fade modalFormulario" id="modalComentarios" role="dialog">
	
	<div class="modal-content modal-dialog">
		
		<div class="modal-body modalTitulo">
			
			<h3 class="backColor">CALIFICA ESTE PRODUCTO</h3>

			<button type="button" class="close" data-dismiss="modal">&times;</button>

			<form method="post" onsubmit="return validarComentario()">

				<input type="hidden" value="" id="idComentario" name="idComentario">
				
				<h1 class="text-center" id="estrellas">

		       		<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>

				</h1>

				<div class="form-group text-center">

		       		<label class="radio-inline"><input type="radio" name="puntaje" value="0.5">0.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="1.0">1.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="1.5">1.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="2.0">2.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="2.5">2.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="3.0">3.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="3.5">3.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="4.0">4.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="4.5">4.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="5.0" checked>5.0</label>

				</div>

				<div class="form-group">
			  		
			  		<label for="comment" class="text-muted">Tu opinión acerca de este producto: <span><small>(máximo 300 caracteres)</small></span></label>
			  		
			  		<textarea class="form-control" rows="5" id="comentario" name="comentario" maxlength="300" required></textarea>

			  		<br>
					
					<input type="submit" class="btn btn-default backColor btn-block" value="ENVIAR">

				</div>

				<!--<?php 

					$actualizarComentario = new ControladorProveedor();
					$actualizarComentario -> ctrActualizarComentario();

				?>-->

			</form>

		</div>

		<div class="modal-footer">
      	
      	</div>

	</div>

</div>

<?php

  $eliminarProducto = new ControladorProductos();
  $eliminarProducto -> ctrEliminarProducto();

?>


