<?php

$servidor = Ruta::ctrRutaServidor();
$url = Ruta::ctrRuta();

?>

<!--=====================================
BREADCRUMB INFOPRODUCTOS
======================================-->
<div class="container-fluid well well-sm">
	
	<div class="container">
		
		<div class="row">
			
			<ul class="breadcrumb fondoBreadcrumb text-uppercase">
				
				<li><a href="<?php echo $url;  ?>">INICIO</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>

			</ul>

		</div>

	</div>

</div>


<input name="tipo_user" id="tipo_user" type="hidden" value="<?php echo $_SESSION['tipo'];  ?>"/>

<!--=====================================
INFOPRODUCTOS
======================================-->

<div class="container-fluid infoproducto">
	
	<div class="container">
		
		<div class="row">

			<?php

				$item =  "ruta_producto";
				$valor = $rutas[0];
				$infoproducto = ControladorProductos::ctrMostrarInfoProducto($item, $valor);

				$multimedia = json_decode($infoproducto["multimedia_producto"],true);


				/*=============================================
				VISOR DE IMÁGENES
				=============================================*/

				if($infoproducto["tipo_producto"] == "fisico"){

					echo '<div class="col-md-5 col-sm-6 col-xs-12 visorImg">
						
							<figure class="visor">';

							if($multimedia != null){

								for($i = 0; $i < count($multimedia); $i ++){

									echo '<img id="lupa'.($i+1).'" class="img-thumbnail" src="'.$servidor.$multimedia[$i]["foto"].'">';

								}								

								echo '</figure>

								<div class="flexslider">
								  
								  <ul class="slides">';

								for($i = 0; $i < count($multimedia); $i ++){

									echo '<li>
								     	<img value="'.($i+1).'" class="img-thumbnail" src="'.$servidor.$multimedia[$i]["foto"].'" alt="'.$infoproducto["titulo_producto"].'">
								    </li>';

								}

							}		
							    						 
							  echo '</ul>

							</div>

						</div>';			

				}else{

					/*=============================================
					VISOR DE VIDEO
					=============================================*/

					echo '<div class="col-sm-6 col-xs-12">
							
						<iframe class="videoPresentacion" src="https://www.youtube.com/embed/'.$infoproducto["multimedia_producto"].'?rel=0&autoplay=0" width="100%" frameborder="0" allowfullscreen></iframe>

					</div>';

				}			

			?>

			<!--=====================================
			PRODUCTO
			======================================-->

			<?php

				if($infoproducto["tipo_producto"] == "fisico"){

					echo '<div class="col-md-7 col-sm-6 col-xs-12">';

				}else{

					echo '<div class="col-sm-6 col-xs-12">';
				}

			?>

				<!--=====================================
				REGRESAR A LA TIENDA
				======================================-->

				<div class="col-xs-6">
					
					<h6>
						
						<a href="javascript:history.back()" class="text-muted">
							
							<i class="fa fa-reply"></i> Continuar Comprando

						</a>

					</h6>

				</div>

				<!--=====================================
				COMPARTIR EN REDES SOCIALES
				======================================-->

				<div class="col-xs-6">
					
					<h6>
						
						<a class="dropdown-toggle pull-right text-muted" data-toggle="dropdown" href="">
							
							<i class="fa fa-plus"></i> Compartir

						</a>

						<ul class="dropdown-menu pull-right compartirRedes">

							<li>
								<p class="btnFacebook">
									<i class="fa fa-facebook"></i>
									Facebook
								</p>
							</li>

							<li>
								<p class="btnGoogle">
									<i class="fa fa-google"></i>
									Google
								</p>
							</li>
							
						</ul>

					</h6>

				</div>

				<div class="clearfix"></div>

				<!--=====================================
				ESPACIO PARA EL PRODUCTO
				======================================-->

				<?php

					echo '<div class="comprarAhora" style="display:none">


						<button class="btn btn-default backColor quitarItemCarrito" idProducto="'.$infoproducto["id_producto"].'" peso="'.$infoproducto["peso_producto"].'"></button>

						<p class="tituloCarritoCompra text-left">'.$infoproducto["titulo_producto"].'</p>';


						if($infoproducto["oferta_producto"] == 0){

							echo'<input class="cantidadItem" value="1" tipo="'.$infoproducto["tipo_producto"].'" precio="'.$infoproducto["precio_producto"].'" idProducto="'.$infoproducto["id_producto"].'">

							<p class="subTotal'.$infoproducto["id_producto"].' subtotales">
						
								<strong>COP $<span>'.$infoproducto["precio_venta_producto"].'</span></strong>

							</p>

							<div class="sumaSubTotal"><span>'.$infoproducto["precio_venta_producto"].'</span></div>';


						}else{

							echo'<input class="cantidadItem" value="1" tipo="'.$infoproducto["tipo_producto"].'" precio="'.$infoproducto["precioOferta_producto"].'" idProducto="'.$infoproducto["id_producto"].'">

							<p class="subTotal'.$infoproducto["id_producto"].' subtotales">
						
								<strong>COP $<span>'.$infoproducto["precioOferta_producto"].'</span></strong>

							</p>

							<div class="sumaSubTotal"><span>'.$infoproducto["precioOferta_producto"].'</span></div>';


						}

					




					echo '</div>';

					/*=============================================
					TITULO
					=============================================*/				
					
					if($infoproducto["oferta_producto"] == 0){

						$fecha = date('Y-m-d');
						$fechaActual = strtotime('-30 day', strtotime($fecha));
						$fechaNueva = date('Y-m-d', $fechaActual);

						if($fechaNueva > $infoproducto["fecha_producto"]){

							echo '<h1 class="text-muted text-uppercase">'.$infoproducto["titulo_producto"].'</h1>';

						}else{

							echo '<h1 class="text-muted text-uppercase">'.$infoproducto["titulo_producto"].'

							<br>

							<small>
						
								<span class="label label-warning">Nuevo</span>

							</small>

							</h1>';

						}

					}else{

						$fecha = date('Y-m-d');
						$fechaActual = strtotime('-30 day', strtotime($fecha));
						$fechaNueva = date('Y-m-d', $fechaActual);

						if($fechaNueva > $infoproducto["fecha"]){

							echo '<h1 class="text-muted text-uppercase">'.$infoproducto["titulo_producto"].'

							<br>';

							if($infoproducto["precio_venta_producto"] != 0){

								echo '<small>
							
									<span class="label label-warning">'.$infoproducto["descuentoOferta_producto"].'% off</span>

								</small>';

							}
							
							echo '</h1>';

						}else{

							echo '<h1 class="text-muted text-uppercase">'.$infoproducto["titulo_producto"].'

							<br>';

							if($infoproducto["precio_venta_producto"] != 0){

								echo '<small>
									<span class="label label-warning">Nuevo</span> 
									<span class="label label-warning">'.$infoproducto["descuentoOferta_producto"].'% off</span> 

								</small>';

							}
							
							echo '</h1>';

						}
					}

					/*=============================================
					TITULO
					=============================================*/	

					if($infoproducto["precio_venta_producto"] == 0){

						echo '<h2 class="text-muted">COP $'.$infoproducto["precio_producto"].' x Mayor</h2>';

					}else{

						if($infoproducto["oferta_producto"] == 0){

							echo '<h2 class="text-muted">COP $'.$infoproducto["precio_venta_producto"].'</h2>';

						}else{

							echo '<h2 class="text-muted">

								<span>
								
									<strong class="oferta">COP $'.$infoproducto["precio_venta_producto"].'</strong>

								</span>

								<span>
									
									$'.$infoproducto["precioOferta_producto"].'

								</span>

							</h2>';

						}

					}

					/*=============================================
					DESCRIPCIÓN
					=============================================*/		

					echo '<p>'.$infoproducto["descripcion_producto"].'</p>';

				?>
				
				<!--=====================================
				CARACTERÍSTICAS DEL PRODUCTO
				======================================-->

				<hr>

				<div class="form-group row">
					
				<?php

					if($infoproducto["detalles_producto"] != null){

						$detalles = json_decode($infoproducto["detalles_producto"], true);

						if($infoproducto["tipo_producto"] == "fisico"){

							if($detalles["Talla"]!=null){

								echo '<div class="col-md-3 col-xs-12">

									<select class="form-control seleccionarDetalle" id="seleccionarTalla">
										
										<option value="">Talla</option>';

										for($i = 0; $i <= count($detalles["Talla"]); $i++){

											echo '<option value="'.$detalles["Talla"][$i].'">'.$detalles["Talla"][$i].'</option>';

										}

									echo '</select>

								</div>';

							}

							if($detalles["Color"]!=null){

								echo '<div class="col-md-3 col-xs-12">

									<select class="form-control seleccionarDetalle" id="seleccionarColor">
										
										<option value="">Color</option>';

										for($i = 0; $i <= count($detalles["Color"]); $i++){

											echo '<option value="'.$detalles["Color"][$i].'">'.$detalles["Color"][$i].'</option>';

										}

									echo '</select>

								</div>';

							}

							if($detalles["Marca"]!=null){

								echo '<div class="col-md-3 col-xs-12">

									<select class="form-control seleccionarDetalle" id="seleccionarMarca">
										
										<option value="">Marca</option>';

										for($i = 0; $i <= count($detalles["Marca"]); $i++){

											echo '<option value="'.$detalles["Marca"][$i].'">'.$detalles["Marca"][$i].'</option>';

										}

									echo '</select>

								</div>';

							}

						}else{

							echo '<div class="col-xs-12">

								<li>
									<i style="margin-right:10px" class="fa fa-play-circle"></i> '.$detalles["Clases"].'
								</li>
								<li>
									<i style="margin-right:10px" class="fa fa-clock-o"></i> '.$detalles["Tiempo"].'
								</li>
								<li>
									<i style="margin-right:10px" class="fa fa-check-circle"></i> '.$detalles["Nivel"].'
								</li>
								<li>
									<i style="margin-right:10px" class="fa fa-info-circle"></i> '.$detalles["Acceso"].'
								</li>
								<li>
									<i style="margin-right:10px" class="fa fa-desktop"></i> '.$detalles["Dispositivo"].'
								</li>
								<li>
									<i style="margin-right:10px" class="fa fa-trophy"></i> '.$detalles["Certificado"].'
								</li>

							</div>';

						}

					}

					/*=============================================
					ENTREGA
					=============================================*/

					if($infoproducto["entrega_producto"] == 0){

						if($infoproducto["precio_venta_producto"] == 0){

							echo '<h4 class="col-md-12 col-sm-0 col-xs-0">

								<hr>

								<span class="label label-default" style="font-weight:100">

									<i class="fa fa-clock-o" style="margin-right:5px"></i>
									Entrega inmediata | 
									<i class="fa fa-shopping-cart" style="margin:0px 5px"></i>
									'.$infoproducto["ventasGratis_producto"].' inscritos |
									<i class="fa fa-eye" style="margin:0px 5px"></i>
									Visto por <span class="vistas" tipo="'.$infoproducto["precio_producto"].'">'.$infoproducto["vistasGratis_producto"].'</span> personas

								</span>

							</h4>

							<h4 class="col-lg-0 col-md-0 col-xs-12">

								<hr>

								<small>

									<i class="fa fa-clock-o" style="margin-right:5px"></i>
									Entrega inmediata <br>
									<i class="fa fa-shopping-cart" style="margin:0px 5px"></i>
									'.$infoproducto["ventasGratis_producto"].' inscritos <br>
									<i class="fa fa-eye" style="margin:0px 5px"></i>
									Visto por <span class="vistas" tipo="'.$infoproducto["precio_producto"].'">'.$infoproducto["vistasGratis_producto"].'</span> personas

								</small>

							</h4>';

						}else{

							echo '<h4 class="col-md-12 col-sm-0 col-xs-0">

								<hr>

								<span class="label label-default" style="font-weight:100">

									<i class="fa fa-clock-o" style="margin-right:5px"></i>
									Entrega inmediata |
									<i class="fa fa-shopping-cart" style="margin:0px 5px"></i>
									'.$infoproducto["ventas_producto"].' ventas |
									<i class="fa fa-eye" style="margin:0px 5px"></i>
									Visto por <span class="vistas" tipo="'.$infoproducto["precio_producto"].'">'.$infoproducto["vistas_producto"].' </span> personas

								</span>

							</h4>

							<h4 class="col-lg-0 col-md-0 col-xs-12">

								<hr>

								<small>

									<i class="fa fa-clock-o" style="margin-right:5px"></i>
									Entrega inmediata <br> 
									<i class="fa fa-shopping-cart" style="margin:0px 5px"></i>
									'.$infoproducto["ventas_producto"].' ventas <br>
									<i class="fa fa-eye" style="margin:0px 5px"></i>
									Visto por <span class="vistas" tipo="'.$infoproducto["precio_producto"].'">'.$infoproducto["vistas_producto"].'</span> personas

								</small>

							</h4>';

						}

					}else{

						if($infoproducto["precio_venta_producto"] == 0){

							echo '<h4 class="col-md-12 col-sm-0 col-xs-0">

								<hr>

								<span class="label label-default" style="font-weight:100">
								
									<i class="fa fa-clock-o" style="margin-right:5px"></i>
									'.$infoproducto["entrega_producto"].' días hábiles para la entrega  |
									<i class="fa fa-shopping-cart" style="margin:0px 5px"></i>
									'.$infoproducto["ventasGratis_producto"].' solicitudes  |
									<i class="fa fa-eye" style="margin:0px 5px"></i>
									Visto por <span class="vistas" tipo="'.$infoproducto["precio_producto"].'">'.$infoproducto["vistasGratis_producto"].'</span> personas  

								</span>

							</h4>

							<h4 class="col-lg-0 col-md-0 col-xs-12">

								<hr>

								<small>
								
									<i class="fa fa-clock-o" style="margin-right:5px"></i>
									'.$infoproducto["entrega_producto"].' días hábiles para la entrega  <br>
									<i class="fa fa-shopping-cart" style="margin:0px 5px"></i>
									'.$infoproducto["ventasGratis_producto"].' solicitudes  <br>
									<i class="fa fa-eye" style="margin:0px 5px"></i>
									Visto por <span class="vistas" tipo="'.$infoproducto["precio_venta"].'">'.$infoproducto["vistasGratis_producto"].' </span> personas 

								</small>

							</h4>';

						}else{

							echo '<h4 class="col-md-12 col-sm-0 col-xs-0">

								<hr>

								<span class="label label-default" style="font-weight:100">

									<i class="fa fa-clock-o" style="margin-right:5px"></i>
									'.$infoproducto["entrega_producto"].' días hábiles para la entrega |
									<i class="fa fa-shopping-cart" style="margin:0px 5px"></i>
									'.$infoproducto["ventas_producto"].' ventas |
									<i class="fa fa-eye" style="margin:0px 5px"></i>
									Visto por <span class="vistas" tipo="'.$infoproducto["precio_venta_producto"].'">'.$infoproducto["vistas_producto"].' </span> personas

								</span>

							</h4>

							<h4 class="col-lg-0 col-md-0 col-xs-12">

								<hr>

								<small>

									<i class="fa fa-clock-o" style="margin-right:5px"></i>
									'.$infoproducto["entrega_producto"].' días hábiles para la entrega <br>
									<i class="fa fa-shopping-cart" style="margin:0px 5px"></i>
									'.$infoproducto["ventas_producto"].' ventas <br>
									<i class="fa fa-eye" style="margin:0px 5px"></i>
									Visto por <span class="vistas" tipo="'.$infoproducto["precio_venta_producto"].'">'.$infoproducto["vistas_producto"].'</span> personas

								</small>

							</h4>';

						}

					}				

				?>

				</div>

				<!--=====================================
				BOTONES DE COMPRA
				======================================-->

				<div class="row botonesCompra">

				<?php

					if($infoproducto["precio_producto"]==0){

						echo '<div class="col-md-6 col-xs-12">';

						if(isset($_SESSION["validarSesion"]) && $_SESSION["validarSesion"] == "ok"){

							if($infoproducto["tipo"]=="virtual"){
						
								echo '<button class="btn btn-default btn-block btn-lg backColor agregarGratis" idProducto="'.$infoproducto["id_producto"].'" idUsuario="'.$_SESSION["id"].'" tipo="'.$infoproducto["tipo_producto"].'" titulo="'.$infoproducto["titulo_producto"].'">ACCEDER AHORA</button>';

							}else{

								echo '<button class="btn btn-default btn-block btn-lg backColor agregarGratis" idProducto="'.$infoproducto["id_producto"].'" idUsuario="'.$_SESSION["id"].'" tipo="'.$infoproducto["tipo_producto"].'" titulo="'.$infoproducto["titulo_producto"].'">SOLICITAR AHORA</button>

									<br>

									<div class="col-xs-12 panel panel-info text-left">

									<strong>¡Atención!</strong>

										El producto a solicitar es totalmente gratuito y se enviará a la dirección solicitada, sólo se cobrará los cargos de envío.

									</div>
								';

							}

						}else{

							echo '<a href="#modalIngreso" data-toggle="modal">

								<button class="btn btn-default btn-block btn-lg backColor">	SOLICITAR AHORA</button>

							</a>';

						}

						echo '</div>';

					}else{

						if($infoproducto["tipo_producto"]=="virtual"){

							echo '<div class="col-md-6 col-xs-12">';

							if(isset($_SESSION["validarSesion"])){

								if($_SESSION["validarSesion"] == "ok"){

									echo '<a id="btnCheckout" href="#modalComprarAhora" data-toggle="modal" idUsuario="'.$_SESSION["id"].'"><button class="btn btn-default btn-block btn-lg">
									<small>COMPRAR AHORA</small></button></a>';

								}

							}else{

								echo '<a href="#modalIngreso" data-toggle="modal"><button class="btn btn-default btn-block btn-lg">
									<small>COMPRAR AHORA</small></button></a>';
			
							}

							echo '</div>

								<div class="col-md-6 col-xs-12">';

								if($infoproducto["oferta_producto"] != 0){
									
									echo '<button class="btn btn-default btn-block btn-lg backColor agregarCarrito"  idProducto="'.$infoproducto["id_producto"].'" imagen="'.$servidor.$infoproducto["portada_producto"].'" titulo="'.$infoproducto["titulo_producto"].'" precio="'.$infoproducto["precioOferta_producto"].'" tipo="'.$infoproducto["tipo_producto"].'" peso="'.$infoproducto["peso_producto"].'">';

								}else{

									echo '<button class="btn btn-default btn-block btn-lg backColor agregarCarrito"  idProducto="'.$infoproducto["id_producto"].'" imagen="'.$servidor.$infoproducto["portada_producto"].'" titulo="'.$infoproducto["titulo_producto"].'" precio="'.$infoproducto["precio_venta_producto"].'" tipo="'.$infoproducto["tipo_producto"].'" peso="'.$infoproducto["peso_producto"].'">';


								}

								echo   '<small>ADICIONAR AL CARRITO</small> 

									<i class="fa fa-shopping-cart col-md-0"></i>

									</button>

								</div>';
						}else{

							echo '<div class="col-lg-6 col-md-8 col-xs-12">';

							if($infoproducto["oferta_producto"] != 0){
									
									echo '<button class="btn btn-default btn-block btn-lg backColor agregarCarrito"  idProducto="'.$infoproducto["id_producto"].'" imagen="'.$servidor.$infoproducto["portada_producto"].'" titulo="'.$infoproducto["titulo_producto"].'" precio="'.$infoproducto["precioOferta_producto"].'" tipo="'.$infoproducto["tipo_producto"].'" peso="'.$infoproducto["peso_producto"].'">';

								}else{

									echo '<button class="btn btn-default btn-block btn-lg backColor agregarCarrito"  idProducto="'.$infoproducto["id_producto"].'" imagen="'.$servidor.$infoproducto["portada_producto"].'" titulo="'.$infoproducto["titulo_producto"].'" precio="'.$infoproducto["precio_venta_producto"].'" tipo="'.$infoproducto["tipo_producto"].'" peso="'.$infoproducto["peso_producto"].'">';

								}


									echo 'ADICIONAR AL CARRITO 

									<i class="fa fa-shopping-cart"></i>

									</button>

								</div>';

						}

					}

				?>

				</div>
				
				<!--=====================================
				ZONA DE LUPA
				======================================-->

				<figure class="lupa">
					
					<img src="">

				</figure>

			</div>
			
		</div>

		<!--=====================================
		COMENTARIOS
		======================================-->

		<br>
        <br>
        <br>

		<div class="row">
          
			<?php

			$datos = array("id_usuario"=>"",
						   "id_producto"=>$infoproducto["id_producto"]);

			$comentarios = ControladorUsuarios::ctrMostrarComentariosPerfil($datos);
			$cantidad = 0;

			foreach ($comentarios as $key => $value){
				
				if($value["comentario_comentario"] != ""){

					$cantidad += count($value["id_comentario"]);

				}
			}

			?>
			
			<ul class="nav nav-tabs">

			<?php

				$cantidadCalificacion = 0;

				if($cantidad == 0){

					echo '<li class="active"><a>ESTE PRODUCTO NO TIENE COMENTARIOS</a></li>
						  <li></li>';

				}else{

					echo '<li class="active"><a>COMENTARIOS '.$cantidad.'</a></li>
						  <li><a id="verMas" href="">Ver más</a></li>';


					$sumaCalificacion = 0;

					foreach ($comentarios as $key => $value){

						if($value["calificacion_comentario"] != 0){

							$cantidadCalificacion += count($value["id_comentario"]);

							$sumaCalificacion += $value["calificacion_comentario"];

						}

					}

					$promedio = round($sumaCalificacion/$cantidadCalificacion,1);

					echo '<li class="pull-right"><a class="text-muted">PROMEDIO DE CALIFICACIÓN: '.$promedio.' | ';

					if($promedio >= 0 && $promedio < 0.5){

						echo '<i class="fa fa-star-half-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>';

					}

					else if($promedio >= 0.5 && $promedio < 1){

						echo '<i class="fa fa-star text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>';

					}

					else if($promedio >= 1 && $promedio < 1.5){

						echo '<i class="fa fa-star text-success"></i>
							  <i class="fa fa-star-half-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>';

					}

					else if($promedio >= 1.5 && $promedio < 2){

						echo '<i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>';

					}

					else if($promedio >= 2 && $promedio < 2.5){

						echo '<i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star-half-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>';

					}

					else if($promedio >= 2.5 && $promedio < 3){

						echo '<i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>';

					}

					else if($promedio >= 3 && $promedio < 3.5){

						echo '<i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star-half-o text-success"></i>
							  <i class="fa fa-star-o text-success"></i>';

					}

					else if($promedio >= 3.5 && $promedio < 4){

						echo '<i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star-o text-success"></i>';

					}

					else if($promedio >= 4 && $promedio < 4.5){

						echo '<i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star-half-o text-success"></i>';

					}else{

						echo '<i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>
							  <i class="fa fa-star text-success"></i>';

					}


				}


			?>

					
				</a></li>

			</ul>

			<br>

		</div>

		<div class="row comentarios">

		<?php

		foreach ($comentarios as $key => $value) {
			
			if($value["comentario_comentario"] != ""){

				$item = "id_usuario";
				$valor = $value["id_usuario_comentario"];

				$usuario = ControladorUsuarios::ctrMostrarUsuario($item, $valor);

				echo '<div class="panel-group col-md-3 col-sm-6 col-xs-12 alturaComentarios">
				
					<div class="panel panel-default">
				      
				      <div class="panel-heading text-uppercase">

				      	'.$usuario["nombre_usuario"].'
				      	<span class="text-right">';

				      	if($usuario["modo_usuario"] == "directo"){

				      		if($usuario["foto_usuario"] == ""){

				      			echo '<img class="img-circle pull-right" src="'.$servidor.'vistas/img/usuarios/default/anonymous.png" width="20%">';	

				      		}else{

				      			echo '<img class="img-circle pull-right" src="'.$url.$usuario["foto_usuario"].'" width="20%">';	

				      		}
				      	
				      	}else{

				      		echo '<img class="img-circle pull-right" src="'.$usuario["foto_usuario"].'" width="20%">';	

				      	}

				      	echo '</span>

				      </div>
				     
				      <div class="panel-body"><small>'.$value["comentario_comentario"].'</small></div>

				      <div class="panel-footer">';
				      	
				      	switch($value["calificacion_comentario"]){

							case 0.5:
							echo '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;

							case 1.0:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;

							case 1.5:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;

							case 2.0:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;

							case 2.5:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;

							case 3.0:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;

							case 3.5:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;

							case 4.0:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;

							case 4.5:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>';
							break;

							case 5.0:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>';
							break;

						}

				      echo '</div>
				    
				    </div>

				</div>';

			}
		}

		?>

		</div>

		<hr>

	</div>

</div>

<!--=====================================
ARTÏCULOS RELACIONADOS
======================================-->
<div class="container-fluid productos">
	
	<div class="container">

		<div class="row">

			<div class="col-xs-12 tituloDestacado">

				<div class="col-sm-6 col-xs-12">
			
					<h1><small>PRODUCTOS RELACIONADOS</small></h1>

				</div>

				<div class="col-sm-6 col-xs-12">

				<?php

					$item = "id_subcategoria";
					$valor = $infoproducto["id_subcategoria_producto"];

					$rutaArticulosDestacados = ControladorProductos::ctrMostrarSubcategorias($item, $valor);

					echo '<a href="'.$url.$rutaArticulosDestacados[0]["ruta_subcategoria"].'">
						
						<button class="btn btn-default backColor pull-right">
							
							VER MÁS <span class="fa fa-chevron-right"></span>

						</button>

					</a>';

				?>

				</div>

			</div>

			<div class="clearfix"></div>

			<hr>

		</div>

		<?php

			


			$ordenar = "";
			$item = "id_subcategoria_producto";
			$valor = $infoproducto["id_subcategoria_producto"];
			$base = 0;
			$tope = 4;
			$modo = "Rand()";

			$relacionados = ControladorProductos::ctrMostrarProductos($ordenar, $item, $valor, $base, $tope, $modo);

			if(!$relacionados){

				echo '<div class="col-xs-12 error404">

					<h1><small>¡Oops!</small></h1>

					<h2>No hay productos relacionados</h2>

				</div>';

			}else{

				echo '<ul class="grid0">';

				foreach ($relacionados as $key => $value) {

				if($value["estado_producto"] != 0){
				
					echo '<li class="col-md-3 col-sm-6 col-xs-12">

						<figure>
							
							<a href="'.$url.$value["ruta_producto"].'" class="pixelProducto">
								
								<img src="'.$servidor.$value["portada_producto"].'" class="img-responsive">

							</a>

						</figure>

						<h4>
				
							<small>
								
								<a href="'.$url.$value["ruta_producto"].'" class="pixelProducto">
									
									'.$value["titulo_producto"].'<br>

									<span style="color:rgba(0,0,0,0)">-</span>';

									$fecha = date('Y-m-d');
									$fechaActual = strtotime('-30 day', strtotime($fecha));
									$fechaNueva = date('Y-m-d', $fechaActual);

									if($fechaNueva < $value["fecha_producto"]){

										echo '<span class="label label-warning fontSize">Nuevo</span> ';

									}

									if($value["oferta_producto"] != 0 && $value["precio_venta_producto"] != 0){

										echo '<span class="label label-warning fontSize">'.$value["descuentoOferta_producto"].'% off</span>';

									}

								echo '</a>	

							</small>			

						</h4>

						<div class="col-xs-6 precio">';

						if($value["precio_producto"] == 0){

							echo '<h2><small>OFERTA</small></h2>';

						}else{

							if($value["oferta_producto"] != 0){

								echo '<h2>

										<small>
					
											<strong class="oferta">COP $'.$value["precio_venta_producto"].'</strong>

										</small>

										<small>$'.$value["precioOferta_producto"].'</small>
									
									</h2>';

							}else{

								echo '<h2><small>COP $'.$value["precio_venta_producto"].'</small></h2>';

							}
							
						}
										
						echo '</div>

						<div class="col-xs-6 enlaces">
							
							<div class="btn-group pull-right">
								
								<button type="button" class="btn btn-default btn-xs deseos" idProducto="'.$value["id_producto"].'" data-toggle="tooltip" title="Agregar a mi lista de deseos">
									
									<i class="fa fa-heart" aria-hidden="true"></i>

								</button>';

								if($value["tipo_producto"] == "virtual" && $value["precio_venta_producto"] != 0){

									if($value["oferta_producto"] != 0){

										echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id_producto"].'" imagen="'.$servidor.$value["portada_producto"].'" titulo="'.$value["titulo_producto"].'" precio="'.$value["precioOferta_producto"].'" tipo="'.$value["tipo_producto"].'" peso="'.$value["peso_producto"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

										<i class="fa fa-shopping-cart" aria-hidden="true"></i>

										</button>';

									}else{

										echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id_producto"].'" imagen="'.$servidor.$value["portada_producto"].'" titulo="'.$value["titulo_producto"].'" precio="'.$value["precio_venta"].'" tipo="'.$value["tipo_producto"].'" peso="'.$value["peso_producto"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

										<i class="fa fa-shopping-cart" aria-hidden="true"></i>

										</button>';

									}

								}

								echo '<a href="'.$url.$value["ruta_producto"].'" class="pixelProducto">
								
									<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" title="Ver producto">
										
										<i class="fa fa-eye" aria-hidden="true"></i>

									</button>	
								
								</a>

							</div>

						</div>

					</li>';

				}
			}

			echo '</ul>';

		}

		?>

	</div>

</div>

<!--=====================================
VENTANA MODAL PARA CHECKOUT
======================================-->

<div id="modalComprarAhora" class="modal fade modalFormulario" role="dialog">
	
	 <div class="modal-content modal-dialog">
	 	
		<div class="modal-body modalTitulo">
			
			<h3 class="backColor">REALIZAR PAGO</h3>

			<button type="button" class="close" data-dismiss="modal">&times;</button>

			<div class="contenidoCheckout">

				<?php

				$respuesta = ControladorCarrito::ctrMostrarTarifas();

				echo '<input type="hidden" id="tasaImpuesto" value="'.$respuesta["impuesto"].'">
					  <input type="hidden" id="envioNacional" value="'.$respuesta["envioNacional"].'">
				      <input type="hidden" id="envioInternacional" value="'.$respuesta["envioInternacional"].'">
				      <input type="hidden" id="tasaMinimaNal" value="'.$respuesta["tasaMinimaNal"].'">
				      <input type="hidden" id="tasaMinimaInt" value="'.$respuesta["tasaMinimaInt"].'">
				      <input type="hidden" id="tasaPais" value="'.$respuesta["pais"].'">

				';

				?>
				
				<div class="formEnvio row">
					
					<h4 class="text-center well text-muted text-uppercase">Información de envío</h4>

					<div class="col-xs-12 seleccionePais">
						
						

					</div>

				</div>

				<br>

				<div class="formaPago row">
					
					<h4 class="text-center well text-muted text-uppercase">Elige la forma de pago</h4>

					<figure class="col-xs-6">
						
						<center>
							
							<input id="checkPaypal" type="radio" name="pago" value="paypal" checked>

						</center>	
						
						<img src="<?php echo $url; ?>vistas/img/plantilla/paypal.jpg" class="img-thumbnail">		

					</figure>

					<figure class="col-xs-6">
						
						<center>
							
							<input id="checkPayu" type="radio" name="pago" value="payu">

						</center>

						<img src="<?php echo $url; ?>vistas/img/plantilla/payu.jpg" class="img-thumbnail">

					</figure>

				</div>

				<br>

				<div class="listaProductos row">
					
					<h4 class="text-center well text-muted text-uppercase">Productos a comprar</h4>

					<table class="table table-striped tablaProductos">
						
						 <thead>
						 	
							<tr>		
								<th>Producto</th>
								<th>Cantidad</th>
								<th>Precio</th>
							</tr>

						 </thead>

						 <tbody>
						 	


						 </tbody>

					</table>

					<div class="col-sm-6 col-xs-12 pull-right">
						
						<table class="table table-striped tablaTasas">
							
							<tbody>
								
								<tr>
									<td>Subtotal</td>	
									<td><span class="cambioDivisa">COP</span> $<span class="valorSubtotal" valor="0">0</span></td>	
								</tr>

								<tr>
									<td>Envío</td>	
									<td><span class="cambioDivisa">COP</span> $<span class="valorTotalEnvio" valor="0">0</span></td>	
								</tr>

								<tr>
									<td>Impuesto</td>	
									<td><span class="cambioDivisa">COP</span> $<span class="valorTotalImpuesto" valor="0">0</span></td>	
								</tr>

								<tr>
									<td><strong>Total</strong></td>	
									<td><strong><span class="cambioDivisa">COP</span> $<span class="valorTotalCompra" valor="0">0</span></strong></td>	
								</tr>

							</tbody>	

						</table>

						 <div class="divisa">

						 	<select class="form-control" id="cambiarDivisa" name="divisa">
						 		
							

						 	</select>	

						 	<br>

						 </div>

					</div>

					<div class="clearfix"></div>

					<form class="formPayu" style="display:none">
					 
						<input name="merchantId" type="hidden" value=""/>
						<input name="accountId" type="hidden" value=""/>
						<input name="description" type="hidden" value=""/>
						<input name="referenceCode" type="hidden" value=""/>	
						<input name="amount" type="hidden" value=""/>
						<input name="tax" type="hidden" value=""/>
						<input name="taxReturnBase" type="hidden" value=""/>
						<input name="shipmentValue" type="hidden" value=""/>
						<input name="currency" type="hidden" value=""/>
						<input name="lng" type="hidden" value="es"/>
						<input name="confirmationUrl" type="hidden" value="" />
						<input name="responseUrl" type="hidden" value=""/>
						<input name="declinedResponseUrl" type="hidden" value=""/>
						<input name="displayShippingInformation" type="hidden" value=""/>
						<input name="test" type="hidden" value="" />
						<input name="signature" type="hidden" value=""/>

					  <input name="Submit" class="btn btn-block btn-lg btn-default backColor" type="submit"  value="PAGAR" >
					</form>
					
					<button class="btn btn-block btn-lg btn-default backColor btnPagar">PAGAR</button>

				</div>

			</div>

		</div>

		<div class="modal-footer">
      	
      	</div>

	</div>

</div>


<?php

if($infoproducto["tipo_producto"] == "fisico"){

	echo '<script type="application/ld+json">

			{
			  "@context": "http://schema.org/",
			  "@type": "Product",
			  "name": "'.$infoproducto["titulo_producto"].'",
			  "image": [';

			  for($i = 0; $i < count($multimedia); $i ++){

			  	echo $servidor.$multimedia[$i]["foto_producto"].',';

			  }
			
			  echo '],
			  "description": "'.$infoproducto["descripcion_producto"].'"
	  
			}

		</script>';

}else{

	echo '<script type="application/ld+json">

			{
			  "@context": "http://schema.org",
			  "@type": "Course",
			  "name": "'.$infoproducto["titulo_producto"].'",
			  "description": "'.$infoproducto["descripcion_producto"].'",
			  "provider": {
			    "@type": "Organization",
			    "name": "Tu Logo",
			    "sameAs": "'.$url.$infoproducto["ruta_producto"].'"
			  }
			}

		</script>';

}

?>
