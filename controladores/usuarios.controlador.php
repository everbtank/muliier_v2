<?php

class ControladorUsuarios
{

    /*=============================================
    REGISTRO DE USUARIO
    =============================================*/

    public function ctrRegistroUsuario()
    {



        if (isset($_POST["regUsuario"])) {

            if (preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["regUsuario"]) &&
                preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["regEmail"]) &&
                preg_match('/^[a-zA-Z0-9]+$/', $_POST["regPassword"])) {

                $encriptar = crypt($_POST["regPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$
                    $2a$07$asxx54ahjppf45sd87a5auxq/SS293XhTEeizKWMnfhnpfay0AALe');

                $encriptarEmail = md5($_POST["regEmail"]);

                $id_cliente = 0;
                $id_asesor  = 0;

                $tipo = $_POST["tipousuario"];


                  
                if ($tipo == "Asesor") {

                    $cod_asesor = "MU-" . strtoupper(substr($_POST["regUsuario"], 0, 1)) . rand(5, 10000);

                    $comision = 0.15;

                    $datosasesor = array("codigo" => $cod_asesor, "comision" => $comision);

                    $tabla = "asesores";

                    $respuesta_asesor = ModeloUsuarios::mdlRegistroAsesor($tabla, $datosasesor);

                    if ($respuesta_asesor == "ok") {

                        $tabla = "asesores";

                        $item = "codigo_asesor";

                        $asesor = ModeloUsuarios::mdlMostrarAsesor($tabla, $item, $cod_asesor);

                        $id_asesor = (int) $asesor["id_asesor"];
                        $tipo      = "";

                    }

                } else if ($tipo == "Cliente") {

                    $estado_afiliacion = 0;
                    $fecha             = date("Y-m-d H:i:s");

                    $datoscliente = array("estado_afiliacion" => $estado_afiliacion, "fecha" => $fecha);

                    $tabla = "clientes";

                    $respuesta_cliente = ModeloUsuarios::mdlRegistroCliente($tabla, $datoscliente);

                    if ($respuesta_cliente == "ok") {

                        $tabla = "clientes";

                        $item = "fecha_cliente";

                        $cliente_usuario = ModeloUsuarios::mdlMostrarCliente($tabla, $item, $fecha);

                        $id_cliente = (int) $cliente_usuario["id_cliente"];

                    }

                }

              



                $datos = array("nombre" => $_POST["regUsuario"],
                    "password"              => $encriptar,
                    "email"                 => $_POST["regEmail"],
                    "foto"                  => "",
                    "modo"                  => "directo",
                    "verificacion"          => 1,
                    "emailEncriptado"       => $encriptarEmail,
                    "tipo"                  => $_POST["tipousuario"]);

                $tabla = "usuarios";

                $respuesta = ModeloUsuarios::mdlRegistroUsuario($tabla, $datos, $id_asesor, $id_cliente);

                if ($respuesta == "ok") {

                    /*=============================================
                    ACTUALIZAR NOTIFICACIONES NUEVOS USUARIOS
                    =============================================*/

                    $traerNotificaciones = ControladorNotificaciones::ctrMostrarNotificaciones();

                    $nuevoUsuario = $traerNotificaciones["nuevosUsuarios"] + 1;

                    ModeloNotificaciones::mdlActualizarNotificaciones("notificaciones", "nuevosUsuarios", $nuevoUsuario);

                    /*=============================================
                    VERIFICACIÓN CORREO ELECTRÓNICO
                    =============================================*/

                    date_default_timezone_set("America/Bogota");

                    $url = Ruta::ctrRuta();

                    $mail = new PHPMailer;

                    $mail->CharSet = 'UTF-8';

                    $mail->isMail();

                    $mail->setFrom('usuarios@muliier.com', 'Muliier');

                    $mail->addReplyTo('usuarios@muliier.com', 'Muliier');

                    $mail->Subject = "Por favor verifique su dirección de correo electrónico";

                    $mail->addAddress($_POST["regEmail"]);

                    $mail->msgHTML('<div style="width:100%; background:#eee; position:relative;  font-family: serif;  padding-bottom:40px">
                    
                    
                    	<div  class="caja1"  style="background-image: url(https://www.muliier.com/vistas/img/plantilla/banner.jpeg); position:relative; margin:auto; width:950px; padding:0px" >
								
								
							<div style="position:relative; margin:auto; width:400px; background:white; padding:50px">
                     	
                     
                     	
                     	    <center>
                     	    <hr style="border:1px solid #ccc; width:80%">
                     	    <hr style="border:1px solid #ccc; width:80%">
                     	

                          	<img style="padding:5px; width:53%" src="https://www.muliier.com/vistas/img/plantilla/muliier.png">

                        </center>

                     
                       <center>
                       
                           <hr style="border:1px solid #ccc; width:80%">
                          <hr style="border:1px solid #ccc; width:80%">

                            <h3 style="font-weight:100; color:#E50039">VERIFIQUE SU DIRECCIÓN DE CORREO ELECTRÓNICO</h3>

                            <hr style="border:1px solid #ccc; width:80%">

                            <h4 style="font-weight:100; color:#757373; padding:0 20px">Para comenzar a usar su cuenta de Muliier.com, debe confirmar su dirección de correo electrónico</h4>

                            <a href="' . $url . 'verificar/' . $encriptarEmail . '" target="_blank" style="text-decoration:none">

                            <div style="line-height:60px; background:#E50039; width:60%; color:white">Verifique su dirección de correo electrónico</div>

                            </a>

                            <br>

                            <hr style="border:1px solid #ccc; width:80%">

                            <h5 style="font-weight:100; color:#757373">Si no se inscribió en esta cuenta, puede ignorar este correo electrónico y la cuenta se eliminará.</h5>
                            
                            </div>

                        </center>

                        </div>
                        
                    
            

                </div>
                    
            </div>');

                    $envio = $mail->Send();

                    if (!$envio) {

                        echo '<script>

                            swal({
                                  title: "¡ERROR!",
                                  text: "¡Ha ocurrido un problema enviando verificación de correo electrónico a ' . $_POST["regEmail"] . $mail->ErrorInfo . '!",
                                  type:"error",
                                  confirmButtonText: "Cerrar",
                                  closeOnConfirm: false
                                },

                                function(isConfirm){

                                    if(isConfirm){
                                        history.back();
                                    }
                            });

                        </script>';

                    } else {

                        /*    session_start();

                        $tabla = "usuarios";
                        $item = "email";
                        $valor = $_POST["regEmail"];

                        $respuesta = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);

                        $_SESSION["verificacion"] = $respuesta["verificacion"];

                        $_SESSION["tipo"] = $respuesta["tipo"];*/

                        echo '<script>

                            swal({
                                  title: "¡OK!",
                                  text: "¡Por favor revise la bandeja de entrada o la carpeta de SPAM de su correo electrónico ' . $_POST["regEmail"] . ' para verificar la cuenta!",
                                  type:"success",
                                  confirmButtonText: "Cerrar",
                                  closeOnConfirm: false
                                },

                                function(isConfirm){

                                    if(isConfirm){
                                        history.back();
                                    }
                            });

                        </script>';

                    }

                }

            } else {

                echo '<script>

                        swal({
                              title: "¡ERROR!",
                              text: "¡Error al registrar el usuario, no se permiten caracteres especiales!",
                              type:"error",
                              confirmButtonText: "Cerrar",
                              closeOnConfirm: false
                            },

                            function(isConfirm){

                                if(isConfirm){
                                    history.back();
                                }
                        });

                </script>';

            }

        }

    }

    /*=============================================
    MOSTRAR USUARIO
    =============================================*/

    public static function ctrMostrarUsuario($item, $valor)
    {

        $tabla = "usuarios";

        $respuesta = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);

        return $respuesta;

    }

    /*=============================================
    ACTUALIZAR USUARIO
    =============================================*/

    public static function ctrActualizarUsuario($id, $item, $valor)
    {

        $tabla = "usuarios";

        $respuesta = ModeloUsuarios::mdlActualizarUsuario($tabla, $id, $item, $valor);

        return $respuesta;

    }

    public static function ctrActualizarAsesorClientes($id, $item, $valor)
    {

        $tabla = "asesores";

        $respuesta = ModeloUsuarios::mdlActualizarAsesorClientes($tabla, $id, $item, $valor);

        return $respuesta;

    }

    /*=============================================
    INGRESO DE USUARIO
    =============================================*/

    public function ctrIngresoUsuario()
    {
        if (isset($_POST["ingEmail"])) {

            if (preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["ingEmail"]) &&
                preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingPassword"])) {

                $encriptar = crypt($_POST["ingPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

                $tabla = "usuarios";
                $item  = "email_usuario";
                $valor = $_POST["ingEmail"];

                $respuesta = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);
                
                

                if (strtolower($respuesta["email_usuario"]) == strtolower($_POST["ingEmail"]) && $respuesta["password_usuario"] == $encriptar) {

                    if ($respuesta["verificacion_usuario"] == 1) {

                        echo '<script>

                            swal({
                                  title: "¡NO HA VERIFICADO SU CORREO ELECTRÓNICO!",
                                  text: "¡Por favor revise la bandeja de entrada o la carpeta de SPAM de su correo para verififcar la dirección de correo electrónico ' . $respuesta["email_usuario"] . '!",
                                  type: "error",
                                  confirmButtonText: "Cerrar",
                                 closeOnConfirm: false
                            },

                            function(isConfirm){
                                     if (isConfirm) {
                                        history.back();
                                      }
                            });

                            </script>';

                    } else {

                        /*if(isset($_POST["g-recaptcha-response"])){

                        $secret = "6LeFe9QUAAAAAPNnZgGzjHp4z6gLirOEJU8_I5Nh";

                        $response = $_POST["g-recaptcha-response"];

                        $remoteip = $_SERVER["REMOTE_ADDR"];

                        $result = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$remoteip");

                        $array = json_decode($result,TRUE);*/

                        if($array["success"])

                        $_SESSION["validarSesion"] = "ok";
                        $_SESSION["id"]            = $respuesta["id_usuario"];
                        $_SESSION["nombre"]        = $respuesta["nombre_usuario"];
                        $_SESSION["foto"]          = $respuesta["foto_usuario"];
                        $_SESSION["email"]         = $respuesta["email_usuario"];
                        $_SESSION["password"]      = $respuesta["password_usuario"];
                        $_SESSION["modo"]          = $respuesta["modo_usuario"];
                        $_SESSION["celular"]       = $respuesta["celular_usuario"];
                        $_SESSION["direccion"]     = $respuesta["direccion_usuario"];
                        $_SESSION["barrio"]        = $respuesta["barrio_usuario"];
                        $_SESSION["tipo"]          = $respuesta["tipo_usuario"];
                        $_SESSION["id_asesor"]     = $respuesta["id_asesor_usuario"];

                        if ($_SESSION["tipo"] == "Cliente") {
                            $tabla1            = "clientes";
                            $item1             = "id_cliente";
                            $valor1            = $respuesta["id_cliente_usuario"];
                            $respuesta_cliente = ModeloUsuarios::mdlMostrarCliente($tabla1, $item1, $valor1);

                            $_SESSION["cod_asesor"] = $respuesta_cliente["cod_asesor_cliente"];

                        }

                        if ($_SESSION["celular"] != "" && $_SESSION["direccion"] != "" && $_SESSION["barrio"] != "") {

                            echo '<script>

                                    window.location = "perfil";

                                </script>';

                        } else {

                            echo '<script>

                                    swal({
                                          title: "¡ Actualizar sus datos!",
                                          text: "¡Bienvenido le sugirimos actualizar sus datos para poder Realizar sus Compras!",
                                          type:"success",
                                          confirmButtonText: "Cerrar",
                                          closeOnConfirm: false
                                        },

                                        function(isConfirm){

                                            if(isConfirm){
                                                history.back();
                                                window.location = "perfil";
                                            }
                                    });

                                </script>';

                        }

                        /*}else{

                    echo'<script>

                    swal({
                    title: "¡ERROR AL INGRESAR!",
                    text: "¡Debe demostrar que no es un Robot!",
                    confirmButtonText: "Cerrar",
                    closeOnConfirm: false
                    },

                    function(isConfirm){
                    if (isConfirm) {
                    window.location = localStorage.getItem("rutaActual");
                    }
                    });

                    </script>';
                    }*/

                    }

                } else {

                    echo '<script>

                            swal({
                                  title: "¡ERROR AL INGRESAR!",
                                  text: "¡Por favor revise que el email exista o la contraseña coincida con la registrada!",
                                  type: "error",
                                  confirmButtonText: "Cerrar",
                                  closeOnConfirm: false
                            },

                            function(isConfirm){
                                     if (isConfirm) {
                                        window.location = localStorage.getItem("rutaActual");
                                      }
                            });

                            </script>';

                }

            } else {

                echo '<script>

                        swal({
                              title: "¡ERROR!",
                              text: "¡Error al ingresar al sistema, no se permiten caracteres especiales!",
                              type:"error",
                              confirmButtonText: "Cerrar",
                              closeOnConfirm: false
                            },

                            function(isConfirm){

                                if(isConfirm){
                                    history.back();
                                }
                        });

                </script>';

            }

        }

    }

    /*=============================================
    OLVIDO DE CONTRASEÑA
    =============================================*/

    public function ctrOlvidoPassword()
    {

        if (isset($_POST["passEmail"])) {

            if (preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["passEmail"])) {

                /*=============================================
                GENERAR CONTRASEÑA ALEATORIA
                =============================================*/

                function generarPassword($longitud)
                {

                    $key     = "";
                    $pattern = "1234567890abcdefghijklmnopqrstuvwxyz";

                    $max = strlen($pattern) - 1;

                    for ($i = 0; $i < $longitud; $i++) {

                        $key .= $pattern{mt_rand(0, $max)};

                    }

                    return $key;

                }

                $nuevaPassword = generarPassword(11);

                $encriptar = crypt($nuevaPassword, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

                $tabla = "usuarios";

                $item1  = "email_usuario";
                $valor1 = $_POST["passEmail"];

                $respuesta1 = ModeloUsuarios::mdlMostrarUsuario($tabla, $item1, $valor1);

                if ($respuesta1) {

                    $id     = $respuesta1["id_usuario"];
                    $item2  = "password_usuario";
                    $valor2 = $encriptar;

                    $respuesta2 = ModeloUsuarios::mdlActualizarUsuario($tabla, $id, $item2, $valor2);

                    if ($respuesta2 == "ok") {

                        /*=============================================
                        CAMBIO DE CONTRASEÑA
                        =============================================*/

                        date_default_timezone_set("America/Bogota");

                        $url = Ruta::ctrRuta();

                        $mail = new PHPMailer;

                        $mail->CharSet = 'UTF-8';

                        $mail->isMail();

                        $mail->setFrom('usuarios@muliier.com', 'Muliier');

                        $mail->addReplyTo('usuarios@muliier.com', 'Muliier');

                        $mail->Subject = "Solicitud de nueva contraseña";

                        $mail->addAddress($_POST["passEmail"]);

                        $mail->msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">

                                <center>

                                    <img style="padding:20px; width:10%" src="https://www.muliier.com/vistas/img/plantilla/logo.png">

                                </center>

                                <div style="position:relative; margin:auto; width:600px; background:white; padding:20px">

                                    <center>

                                    <img style="padding:20px; width:15%" src="https://www.muliier.com/vistas/img/plantilla/icon-pass.png">

                                    <h3 style="font-weight:100; color:#E50039">SOLICITUD DE NUEVA CONTRASEÑA</h3>

                                    <hr style="border:1px solid #ccc; width:80%">

                                    <h4 style="font-weight:100; color:#E50039; padding:0 20px"><strong>Su nueva contraseña: </strong>' . $nuevaPassword . '</h4>

                                    <a href="' . $url . '" target="_blank" style="text-decoration:none">

                                    <div style="line-height:60px; background:#0aa; width:60%; color:white">Ingrese nuevamente al sitio</div>

                                    </a>

                                    <br>

                                    <hr style="border:1px solid #E50039; width:80%">

                                    <h5 style="font-weight:100; color:#999">Si no se inscribió en esta cuenta, puede ignorar este correo electrónico y la cuenta se eliminará.</h5>

                                    </center>

                                </div>

                            </div>');

                        $envio = $mail->Send();

                        if (!$envio) {

                            echo '<script>

                                swal({
                                      title: "¡ERROR!",
                                      text: "¡Ha ocurrido un problema enviando cambio de contraseña a ' . $_POST["passEmail"] . $mail->ErrorInfo . '!",
                                      type:"error",
                                      confirmButtonText: "Cerrar",
                                      closeOnConfirm: false
                                    },

                                    function(isConfirm){

                                        if(isConfirm){
                                            history.back();
                                        }
                                });

                            </script>';

                        } else {

                            echo '<script>

                                swal({
                                      title: "¡OK!",
                                      text: "¡Por favor revise la bandeja de entrada o la carpeta de SPAM de su correo electrónico ' . $_POST["passEmail"] . ' para su cambio de contraseña!",
                                      type:"success",
                                      confirmButtonText: "Cerrar",
                                      closeOnConfirm: false
                                    },

                                    function(isConfirm){

                                        if(isConfirm){
                                            history.back();
                                        }
                                });

                            </script>';

                        }

                    }

                } else {

                    echo '<script>

                        swal({
                              title: "¡ERROR!",
                              text: "¡El correo electrónico no existe en el sistema!",
                              type:"error",
                              confirmButtonText: "Cerrar",
                              closeOnConfirm: false
                            },

                            function(isConfirm){

                                if(isConfirm){
                                    history.back();
                                }
                        });

                    </script>';

                }

            } else {

                echo '<script>

                        swal({
                              title: "¡ERROR!",
                              text: "¡Error al enviar el correo electrónico, está mal escrito!",
                              type:"error",
                              confirmButtonText: "Cerrar",
                              closeOnConfirm: false
                            },

                            function(isConfirm){

                                if(isConfirm){
                                    history.back();
                                }
                        });

                </script>';

            }

        }

    }

    /*=============================================
    REGISTRO CON REDES SOCIALES
    =============================================*/
    public static function ctrRegistroRedesSociales($datos)
    {

        $tabla         = "usuarios";
        $item          = "email_usuario";
        $valor         = $datos["email_usuario"];
        $emailRepetido = false;
        $id_cliente    = 0;
        $id_asesor     = 0;
        $tipo          = $datos["tipo_usuario"];

        $respuesta0 = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);

        if ($respuesta0) {

            if ($respuesta0["modo_usuario"] != $datos["modo_usuario"]) {

                echo '<script>

                        swal({
                              title: "¡ERROR!",
                              text: "¡El correo electrónico ' . $datos["email_usuario"] . ', ! ya está registrado en el sistema con un método diferente a Google!",
                              type:"error",
                              confirmButtonText: "Cerrar",
                              closeOnConfirm: false
                            },

                            function(isConfirm){

                                if(isConfirm){
                                    history.back();
                                }
                        });

                </script>';

                $emailRepetido = false;

                return;

            }

            $emailRepetido = true;

        } else {

            if ($tipo == "Cliente") {

                $estado_afiliacion = 0;
                $fecha             = date("Y-m-d H:i:s");

                $datoscliente = array("estado_afiliacion" => $estado_afiliacion, "fecha" => $fecha);

                $tabla2 = "clientes";

                $respuesta_cliente = ModeloUsuarios::mdlRegistroCliente($tabla2, $datoscliente);

                if ($respuesta_cliente == "ok") {

                    $item = "fecha_cliente";

                    $cliente_usuario = ModeloUsuarios::mdlMostrarCliente($tabla2, $item, $fecha);

                    $id_cliente = (int) $cliente_usuario["id_cliente"];

                }

            }

            $respuesta1 = ModeloUsuarios::mdlRegistroUsuario($tabla, $datos, $id_asesor, $id_cliente);

            /*=============================================
            ACTUALIZAR NOTIFICACIONES NUEVOS USUARIOS
            =============================================*/

            $tabla1 = "notificaciones";

            $traerNotificaciones = ControladorNotificaciones::ctrMostrarNotificaciones($tabla1);

            $nuevoUsuario = $traerNotificaciones["nuevosUsuarios"] + 1;

            ModeloNotificaciones::mdlActualizarNotificaciones("notificaciones", "nuevosUsuarios", $nuevoUsuario);

            session_start();

            $tabla = "usuarios";
            $item  = "email_usuario";
            $valor = $datos["email"];

            $respuesta = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);

            $_SESSION["verificacion"] = $respuesta["verificacion_usuario"];

            $_SESSION["tipo"] = $respuesta["tipo_usuario"];

        }

        if ($emailRepetido || $respuesta1 == "ok") {

            $respuesta2 = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);

            if ($respuesta2["modo_usuario"] == "facebook") {

                session_start();

                $_SESSION["validarSesion"] = "ok";
                $_SESSION["id"]            = $respuesta2["id_usuario"];
                $_SESSION["nombre"]        = $respuesta2["nombre_usuario"];
                $_SESSION["foto"]          = $respuesta2["foto_usuario"];
                $_SESSION["email"]         = $respuesta2["email_usuario"];
                $_SESSION["password"]      = $respuesta2["password_usuario"];
                $_SESSION["modo"]          = $respuesta2["modo_usuario"];
                $_SESSION["tipo"]          = $respuesta2["tipo_usuario"];
                $_SESSION["celular"]       = $respuesta2["celular_usuario"];
                $_SESSION["direccion"]     = $respuesta2["direccion_usuario"];
                $_SESSION["barrio"]        = $respuesta2["barrio_usuario"];
                #    $_SESSION["id_asesor"] = $respuesta2["id_asesor"];

                if ($_SESSION["tipo"] == "Cliente") {

                    $id_cliente = $respuesta2["id_cliente_usuario"];
                    $tabla2     = "clientes";
                    $item2      = "id_cliente";

                    $respuesta_cliente = ModeloUsuarios::mdlMostrarUsuario($tabla2, $item2, $id_cliente);

                    $_SESSION["cod_asesor_usuario"] = $respuesta_cliente["cod_asesor_usuario"];

                }

                echo "ok";

            } else if ($respuesta2["modo_usuario"] == "google") {

                $_SESSION["validarSesion"] = "ok";
                $_SESSION["id"]            = $respuesta2["id_usuario"];
                $_SESSION["nombre"]        = $respuesta2["nombre_usuario"];
                $_SESSION["foto"]          = $respuesta2["foto_usuario"];
                $_SESSION["email"]         = $respuesta2["email_usuario"];
                $_SESSION["password"]      = $respuesta2["password_usuario"];
                $_SESSION["modo"]          = $respuesta2["modo_usuario"];
                $_SESSION["tipo"]          = $respuesta2["tipo_usuario"];
                $_SESSION["celular"]       = $respuesta2["celular_usuario"];
                $_SESSION["direccion"]     = $respuesta2["direccion_usuario"];
                $_SESSION["barrio"]        = $respuesta2["barrio_usuario"];

                if ($_SESSION["tipo"] == "Cliente") {

                    $tabla2     = "clientes";
                    $item2      = "id_cliente";
                    $id_cliente = $respuesta2["id_cliente_usuario"];

                    $respuesta_cliente = ModeloUsuarios::mdlMostrarUsuario($tabla2, $item2, $id_cliente);

                    $_SESSION["cod_asesor_usuario"] = $respuesta_cliente["cod_asesor_usuario"];

                }

                if ($_SESSION["celular_usuario"] != "" && $_SESSION["direccion_usuario"] != "" && $_SESSION["barrio_usuario"] != "") {

                    echo '<script>

                                    window.location = "perfil";

                                </script>';

                } else {

                    echo '<script>

                                    swal({
                                          title: "¡ Actualizar sus datos!",
                                          text: "¡Bienvenido le sugirimos actualizar sus datos para poder Realizar sus Compras!",
                                          type:"success",
                                          confirmButtonText: "Cerrar",
                                          closeOnConfirm: false
                                        },

                                        function(isConfirm){

                                            if(isConfirm){
                                                history.back();
                                                 window.location = "perfil";

                                            }
                                    });

                                </script>';

                }

                echo "<span style='color:white'>ok</span>";

            } else {

                echo "";
            }

        }
    }

    /*=============================================
    ACTUALIZAR PERFIL
    =============================================*/

    public function ctrActualizarPerfil()
    {

        if ($_SESSION["modo"] == "directo") {

            if (isset($_POST["editarNombre"])) {

                /*=============================================
                VALIDAR IMAGEN
                =============================================*/

                $ruta = $_POST["fotoUsuario"];

                if (isset($_FILES["datosImagen"]["tmp_name"]) && !empty($_FILES["datosImagen"]["tmp_name"])) {

                    /*=============================================
                    PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD
                    =============================================*/

                    $directorio = "vistas/img/usuarios/" . $_POST["idUsuario"];

                    if (!empty($_POST["fotoUsuario"])) {

                        unlink($_POST["fotoUsuario"]);

                    } else {

                        mkdir($directorio, 0755);

                    }

                    /*=============================================
                    GUARDAMOS LA IMAGEN EN EL DIRECTORIO
                    =============================================*/

                    list($ancho, $alto) = getimagesize($_FILES["datosImagen"]["tmp_name"]);

                    $nuevoAncho = 500;
                    $nuevoAlto  = 500;

                    $aleatorio = mt_rand(100, 999);

                    if ($_FILES["datosImagen"]["type"] == "image/jpeg") {

                        $ruta = "vistas/img/usuarios/" . $_POST["idUsuario"] . "/" . $aleatorio . ".jpg";

                        /*=============================================
                        MOFICAMOS TAMAÑO DE LA FOTO
                        =============================================*/

                        $origen = imagecreatefromjpeg($_FILES["datosImagen"]["tmp_name"]);

                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

                        imagejpeg($destino, $ruta);

                    }

                    if ($_FILES["datosImagen"]["type"] == "image/png") {

                        $ruta = "vistas/img/usuarios/" . $_POST["idUsuario"] . "/" . $aleatorio . ".png";

                        /*=============================================
                        MOFICAMOS TAMAÑO DE LA FOTO
                        =============================================*/

                        $origen = imagecreatefrompng($_FILES["datosImagen"]["tmp_name"]);

                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

                        imagealphablending($destino, false);

                        imagesavealpha($destino, true);

                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

                        imagepng($destino, $ruta);

                    }

                }

                if ($_POST["editarPassword"] == "") {

                    $password = $_POST["passUsuario"];

                } else {

                    $password = crypt($_POST["editarPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

                }

                $datos = array("nombre_usuario" => $_POST["editarNombre"],
                    "email_usuario"                 => $_POST["editarEmail"],
                    "password_usuario"              => $password,
                    "foto_usuario"                  => $ruta,
                    "celular_usuario"               => $_POST["editarcelular"],
                    "direccion_usuario"             => $_POST["editardireccion"],
                    "barrio_usuario"                => $_POST["editarbarrio"],
                    "id_usuario"                    => $_POST["idUsuario"]);

                $tabla = "usuarios";

                $respuesta = ModeloUsuarios::mdlActualizarPerfil($tabla, $datos);

                if ($respuesta == "ok") {

                    $_SESSION["validarSesion"] = "ok";
                    $_SESSION["id"]            = $datos["id_usuario"];
                    $_SESSION["nombre"]        = $datos["nombre_usuario"];
                    $_SESSION["foto"]          = $datos["foto_usuario"];
                    $_SESSION["email"]         = $datos["email_usuario"];
                    $_SESSION["password"]      = $datos["password_usuario"];
                    $_SESSION["celular"]       = $datos["celular_usuario"];
                    $_SESSION["direccion"]     = $datos["direccion_usuario"];
                    $_SESSION["barrio"]        = $datos["barrio_usuario"];
                    $_SESSION["modo"]          = $_POST["modoUsuario_usuario"];
                    $_SESSION["cod_asesor"]    = $_POST["cod_asesor_usuario"];

                    if ($_SESSION["tipo"] == "Cliente") {

                        $tabla1            = "usuarios";
                        $item1             = "email_usuario";
                        $valor1            = $datos["email_usuario"];
                        $respuesta_usuario = ModeloUsuarios::mdlMostrarUsuario($tabla1, $item1, $valor1);

                        $tabla2     = "clientes";
                        $item2      = "id_cliente";
                        $id_cliente = $respuesta_usuario["id_cliente_usuario"];

                        $respuesta_cliente = ModeloUsuarios::mdlMostrarCliente($tabla2, $item2, $id_cliente);

                        $_SESSION["cod_asesor_usuario"] = $respuesta_cliente["cod_asesor_cliente"];

                    }

                    echo '<script>

                            swal({
                                  title: "¡OK!",
                                  text: "¡Su cuenta ha sido actualizada correctamente!",
                                  type:"success",
                                  confirmButtonText: "Cerrar",
                                  closeOnConfirm: false
                                },

                                function(isConfirm){

                                    if(isConfirm){
                                        history.back();
                                    }
                            });

                    </script>';

                }

            }
        } else {

            if (isset($_POST["editarNombre1"])) {

                /*=============================================
                VALIDAR IMAGEN
                =============================================*/

                $ruta = $_POST["fotoUsuario"];

                if (isset($_FILES["datosImagen"]["tmp_name"]) && !empty($_FILES["datosImagen"]["tmp_name"])) {

                    /*=============================================
                    PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD
                    =============================================*/

                    $directorio = "vistas/img/usuarios/" . $_POST["idUsuario"];

                    if (!empty($_POST["fotoUsuario"])) {

                        unlink($_POST["fotoUsuario"]);

                    } else {

                        mkdir($directorio, 0755);

                    }

                    /*=============================================
                    GUARDAMOS LA IMAGEN EN EL DIRECTORIO
                    =============================================*/

                    list($ancho, $alto) = getimagesize($_FILES["datosImagen"]["tmp_name"]);

                    $nuevoAncho = 500;
                    $nuevoAlto  = 500;

                    $aleatorio = mt_rand(100, 999);

                    if ($_FILES["datosImagen"]["type"] == "image/jpeg") {

                        $ruta = "vistas/img/usuarios/" . $_POST["idUsuario"] . "/" . $aleatorio . ".jpg";

                        /*=============================================
                        MOFICAMOS TAMAÑO DE LA FOTO
                        =============================================*/

                        $origen = imagecreatefromjpeg($_FILES["datosImagen"]["tmp_name"]);

                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

                        imagejpeg($destino, $ruta);

                    }

                    if ($_FILES["datosImagen"]["type"] == "image/png") {

                        $ruta = "vistas/img/usuarios/" . $_POST["idUsuario"] . "/" . $aleatorio . ".png";

                        /*=============================================
                        MOFICAMOS TAMAÑO DE LA FOTO
                        =============================================*/

                        $origen = imagecreatefrompng($_FILES["datosImagen"]["tmp_name"]);

                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

                        imagealphablending($destino, false);

                        imagesavealpha($destino, true);

                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

                        imagepng($destino, $ruta);

                    }

                }

                if ($_POST["editarPassword1"] == "") {

                    $password = $_POST["passUsuario"];

                } else {

                    $password = crypt($_POST["editarPassword1"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

                }

                $datos = array("nombre" => $_POST["editarNombre1"],
                    "email"                 => $_POST["editarCorreo1"],
                    "password"              => $password,
                    "foto"                  => $ruta,
                    "celular"               => $_POST["editarcelular1"],
                    "direccion"             => $_POST["editardireccion1"],
                    "barrio"                => $_POST["editarbarrio1"],
                    "cod_asesor"            => $_POST["cod_asesor"],
                    "id"                    => $_POST["idUsuario"]);

                $tabla = "usuarios";
                

                $respuesta = ModeloUsuarios::mdlActualizarPerfil($tabla, $datos);
                
               
                 
                   /* $tabla2="cliente";
                    
                    $datos_cliente = array(
                    "cod_asesor"            => $_POST["cod_asesor"],
                    "id"                    => $_POST["idcliente"]);
                     
                    $respuesta_cliente = ModeloUsuarios::mdlActualizarCliente($tabla2,$datos_cliente);*/

                if ($respuesta == "ok") {

                    $_SESSION["validarSesion"] = "ok";
                    $_SESSION["id"]            = $datos["id_usuario"];
                    $_SESSION["nombre"]        = $datos["nombre_usuario"];
                    $_SESSION["foto"]          = $datos["foto_usuario"];
                    $_SESSION["email"]         = $datos["email_usuario"];
                    $_SESSION["password"]      = $datos["password_usuario"];
                    $_SESSION["celular"]       = $datos["celular_usuario"];
                    $_SESSION["direccion"]     = $datos["direccion_usuario"];
                    $_SESSION["barrio"]        = $datos["barrio_usuario"];
                    $_SESSION["modo"]          = $_POST["modoUsuario_usuario"];
                    
                    
                    //$_SESSION["cod_asesor"]    = $datoscliente["cod_asesor"];
                    
                    

                   
                    $tabla1            = "usuarios";
                    $item1             = "email_usuario";
                    $email             = $datos["email_usuario"];
                    $respuesta_usuario = ModeloUsuarios::mdlMostrarUsuario($tabla1, $item1, $email);
                    $id_cliente        = $respuesta_usuario["id_cliente_usuario"];

                    $tabla2             = "clientes";
                    $item2              = "id_cliente";
                    $valor2             = $id_cliente;
                    $respuesta_cliente  = ModeloUsuarios::mdlMostrarUsuario($tabla2, $item2, $valor2);
                    $estado_afiliacion  = $respuesta_cliente["estado_afiliacion_cliente"];
                    $cod_asesor_cliente = $respuesta_cliente["cod_asesor_cliente"];

                    if ($estado_afiliacion == 0) {

                        $cod_asesor    = $_POST["cod_asesor"];
                        $estado        = 1;
                        $datos_cliente = array("cod_asesor_cliente" => $cod_asesor, "estado_cliente" => $estado,
                            "id"                                => $id_cliente);

                        $respuesta_asesor_afiliados = ModeloUsuarios::mdlActualizarCliente($tabla2, $datos_cliente);

                        if ($respuesta_asesor_afiliados == "ok") {

                            $tabla1            = "asesores";
                            $item3             = "codigo_asesor";
                            $respuesta_asesor1 = ModeloUsuarios::mdlMostrarAsesor($tabla1, $item3, $cod_asesor);

                            $cantidad_clientes = $respuesta_asesor1["cantidad_asesor"] + 1;
                            $id_asesor         = $respuesta_asesor1["id_asesor"];

                            $datos3 = array("cantidad_asesor" => $cantidad_clientes,
                                "id"                       => $id_asesor);

                            $respuesta_afiliados = ModeloUsuarios::mdlActualizarAsesorClientes($tabla1, $datos3);
                            if ($respuesta_afiliados == "ok") {

                                $_SESSION["cod_asesor"] = $cod_asesor;

                            }

                            $_SESSION["cod_asesor"] = $cod_asesor;

                        }

                    }else{
                        
                           $cod_asesor    = $_POST["cod_asesor"];
                        $datos_cliente = array("cod_asesor" => $cod_asesor,
                            "id"                                => $id_cliente);

                        $respuesta_codigo = ModeloUsuarios::mdlActualizarClienteCodigo($tabla2, $datos_cliente);
                        
                          if(respuesta_codigo=="ok") {

                                $_SESSION["cod_asesor"] = $cod_asesor;

                            }
                        
                        
                        
                        
                    }

                    echo '<script>

                            swal({
                                  title: "¡OK!",
                                  text: "¡Su cuenta ha sido actualizada correctamente!",
                                  type:"success",
                                  confirmButtonText: "Cerrar",
                                  closeOnConfirm: false
                                },

                                function(isConfirm){

                                    if(isConfirm){
                                        history.back();
                                    }
                            });

                    </script>';

                }

            }

        }

    }

    /*=============================================
    AFILIARSE A UN ASESOR
    =============================================*/
    
    

   /* public function ctrAgregarAfiliado()
    {

        $estado_afiliacion = 0;
        $tabla1            = "usuarios";
        $item1             = "email";
        $email             = $_SESSION["email"];
        $respuesta_usuario = ModeloUsuarios::mdlMostrarUsuario($tabla1, $item1, $email);
        $id_cliente        = $respuesta_usuario["id_cliente"];

        $tabla2             = "cliente";
        $item2              = "id";
        $valor2             = $id_cliente;
        $respuesta_cliente  = ModeloUsuarios::mdlMostrarUsuario($tabla2, $item2, $valor2);
        $estado_afiliacion  = $respuesta_cliente["estado_afiliacion"];
        $cod_asesor_cliente = $respuesta_cliente["cod_asesor"];

        if ($estado_afiliacion == 0) {

            $cod_asesor    = $_POST["cod_asesor"];
            $estado        = 1;
            $datos_cliente = array("cod_asesor" => $cod_asesor, "estado" => $estado,
                "id"                                => $id_cliente);

            $respuesta_asesor_afiliados = ModeloUsuarios::mdlActualizarCliente($tabla2, $datos_cliente);

            if ($respuesta_asesor_afiliados == "ok") {

                $tabla1            = "asesor";
                $item3             = "codigo";
                $respuesta_asesor1 = ModeloUsuarios::mdlMostrarAsesor($tabla1, $item3, $cod_asesor);

                $cantidad_clientes = $respuesta_asesor1["cantidad"] + 1;
                $id_asesor         = $respuesta_asesor1["id"];

                $datos3 = array("cantidad" => $cantidad_clientes,
                    "id"                       => $id_asesor);

                $respuesta_afiliados = ModeloUsuarios::mdlActualizarAsesorClientes($tabla1, $datos3);
                if ($respuesta_afiliados == "ok") {

                    $_SESSION["cod_asesor"] = $cod_asesor;

                }

                return $cod_asesor;
                $_SESSION["cod_asesor"] = $cod_asesor;

            }

        }

    }*/

    /*public static function ctrQuitarAfiliados($datos)
    {

    $tabla              = "cliente";
    $item               = "cod_asesor";
    $valor              = $datos["cod_asesor"];
    $respuesta_cliente  = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);
    $estado_afiliacion  = $respuesta_cliente["estado_afiliacion"];
    $id_cliente         = $respuesta_cliente["id"];
    $cod_asesor_cliente = $respuesta_cliente["cod_asesor"];

    if ($estado_afiliacion == 1) {
    $estado     = 0;
    $cod_asesor = "";

    $datos_cliente = array("cod_asesor" => $cod_asesor, "estado" => $estado,
    "id"                                => $id_cliente);

    $respuesta_afiliados = ModeloUsuarios::mdlActualizarCliente($tabla, $datos_cliente);
    if ($respuesta_afiliados == "ok") {

    $_SESSION["cod_asesor"] = "";
    }
    return $respuesta_afiliados;
    }

    }*/

    /*=============================================
    MOSTRAR COMPRAS
    =============================================*/

    public static function ctrMostrarCompras($item, $valor)
    {

        $tabla = "compras";

        $respuesta = ModeloUsuarios::mdlMostrarCompras($tabla, $item, $valor);

        return $respuesta;

    }

    /*=============================================
    MOSTRAR COMENTARIOS EN PERFIL
    =============================================*/

    public static function ctrMostrarComentariosPerfil($datos)
    {

        $tabla = "comentarios";

        $respuesta = ModeloUsuarios::mdlMostrarComentariosPerfil($tabla, $datos);

        return $respuesta;

    }

    /*=============================================
    ACTUALIZAR COMENTARIOS
    =============================================*/

    public function ctrActualizarComentario()
    {

        if (isset($_POST["idComentario"])) {

            if (preg_match('/^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["comentario"])) {

                if ($_POST["comentario"] != "") {

                    $tabla = "comentarios";

                    $datos = array("id_comentario" => $_POST["idComentario"],
                        "calificacion_comentario"      => $_POST["puntaje"],
                        "comentario_comentario"        => $_POST["comentario"]);

                    $respuesta = ModeloUsuarios::mdlActualizarComentario($tabla, $datos);

                    if ($respuesta == "ok") {

                        echo '<script>

                                swal({
                                      title: "¡GRACIAS POR COMPARTIR SU OPINIÓN!",
                                      text: "¡Su calificación y comentario ha sido guardado!",
                                      type: "success",
                                      confirmButtonText: "Cerrar",
                                      closeOnConfirm: false
                                },

                                function(isConfirm){
                                         if (isConfirm) {
                                           history.back();
                                          }
                                });

                              </script>';

                    }

                } else {

                    echo '<script>

                        swal({
                              title: "¡ERROR AL ENVIAR SU CALIFICACIÓN!",
                              text: "¡El comentario no puede estar vacío!",
                              type: "error",
                              confirmButtonText: "Cerrar",
                              closeOnConfirm: false
                        },

                        function(isConfirm){
                                 if (isConfirm) {
                                   history.back();
                                  }
                        });

                      </script>';

                }

            } else {

                echo '<script>

                    swal({
                          title: "¡ERROR AL ENVIAR SU CALIFICACIÓN!",
                          text: "¡El comentario no puede llevar caracteres especiales!",
                          type: "error",
                          confirmButtonText: "Cerrar",
                          closeOnConfirm: false
                    },

                    function(isConfirm){
                             if (isConfirm) {
                               history.back();
                              }
                    });

                  </script>';

            }

        }

    }

    /*=============================================
    AGREGAR A LISTA DE DESEOS
    =============================================*/

    public static function ctrAgregarDeseo($datos)
    {

        $tabla = "deseos";

        $respuesta = ModeloUsuarios::mdlAgregarDeseo($tabla, $datos);

        return $respuesta;

    }

    /*=============================================
    MOSTRAR LISTA DE DESEOS
    =============================================*/

    public static function ctrMostrarDeseos($item)
    {

        $tabla = "deseos";

        $respuesta = ModeloUsuarios::mdlMostrarDeseos($tabla, $item);

        return $respuesta;

    }

    public static function ctrMostrarAsesor($item, $valor)
    {

        $tabla = "asesores";

        $respuesta = ModeloUsuarios::mdlMostrarAsesor($tabla, $item, $valor);

        return $respuesta;

    }


    public static function ctrMostrarCliente($item, $valor)
    {

        $tabla = "clientes";

        $respuesta = ModeloUsuarios::mdlMostrarCliente($tabla, $item, $valor);

        return $respuesta;

    }

    /*=============================================
    QUITAR PRODUCTO DE LISTA DE DESEOS
    =============================================*/
    public static function ctrQuitarDeseo($datos)
    {

        $tabla = "deseos";

        $respuesta = ModeloUsuarios::mdlQuitarDeseo($tabla, $datos);

        return $respuesta;

    }

    /*=============================================
    ELIMINAR USUARIO
    =============================================*/

    public function ctrEliminarUsuario()
    {

        if (isset($_GET["id"])) {

            $tabla1 = "usuarios";
            $tabla2 = "comentarios";
            $tabla3 = "compras";
            $tabla4 = "deseos";

            $id = $_GET["id"];

            if ($_GET["foto"] != "") {

                unlink($_GET["foto"]);
                rmdir('vistas/img/usuarios/' . $_GET["id"]);

            }

            $respuesta = ModeloUsuarios::mdlEliminarUsuario($tabla1, $id);

            ModeloUsuarios::mdlEliminarComentarios($tabla2, $id);

            ModeloUsuarios::mdlEliminarCompras($tabla3, $id);

            ModeloUsuarios::mdlEliminarListaDeseos($tabla4, $id);

            if ($respuesta == "ok") {

                $url = Ruta::ctrRuta();

                echo '<script>

                        swal({
                              title: "¡SU CUENTA HA SIDO BORRADA!",
                              text: "¡Debe registrarse nuevamente si desea ingresar!",
                              type: "success",
                              confirmButtonText: "Cerrar",
                              closeOnConfirm: false
                        },

                        function(isConfirm){
                                 if (isConfirm) {
                                   window.location = "' . $url . 'salir";
                                  }
                        });

                      </script>';

            }

        }

    }

    /*=============================================
    FORMULARIO CONTACTENOS
    =============================================*/

    public function ctrFormularioContactenos()
    {

        if (isset($_POST['mensajeContactenos'])) {

            if (preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nombreContactenos"]) &&
                preg_match('/^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["mensajeContactenos"]) &&
                preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["emailContactenos"])) {

                /*=============================================
                ENVÍO CORREO ELECTRÓNICO
                =============================================*/

                date_default_timezone_set("America/Bogota");

                $url = Ruta::ctrRuta();

                $mail = new PHPMailer;

                $mail->CharSet = 'UTF-8';

                $mail->isMail();

                $mail->setFrom('usuarios@muliier.com', 'Muliier');

                $mail->addReplyTo('usuarios@muliier.com', 'Muliier');

                $mail->Subject = "Ha recibido una consulta";

                $mail->addAddress("yosoymuliier@gmail.com");

                $mail->msgHTML('
                            <div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">



                        <center><img style="padding:20px; width:10%" src="https://www.muliier.com/vistas/img/plantilla/logo.png"></center>

                        <div style="position:relative; margin:auto; width:600px; background:white; padding-bottom:20px">

                            <center>

                            <img style="padding-top:20px; width:15%" src="https://www.muliier.com/vistas/img/plantilla/icon-email.png">


                            <h3 style="font-weight:100; color:#757373;">HA RECIBIDO UNA CONSULTA</h3>

                            <hr style="width:80%; border:1px solid #757373">

                            <h4 style="font-weight:100; color:#757373; padding:0px 20px; text-transform:uppercase">' . $_POST["nombreContactenos"] . '</h4>

                            <h4 style="font-weight:100; color:#757373; padding:0px 20px;">De: ' . $_POST["emailContactenos"] . '</h4>

                            <h4 style="font-weight:100; color:#757373; padding:0px 20px">Celular: ' . $_POST["numeroContactenos"] . '</h4>
                            <h4 style="font-weight:100; color:#757373; padding:0px 20px">Mensaje: ' . $_POST["mensajeContactenos"] . '</h4>

                            <hr style="width:80%; border:1px solid #E50039 ">

                            </center>

                        </div>

                    </div>');

                $envio = $mail->Send();

                if (!$envio) {

                    echo '<script>

                            swal({
                                  title: "¡ERROR!",
                                  text: "¡Ha ocurrido un problema enviando el mensaje!",
                                  type:"error",
                                  confirmButtonText: "Cerrar",
                                  closeOnConfirm: false
                                },

                                function(isConfirm){

                                    if(isConfirm){
                                        history.back();
                                    }
                            });

                        </script>';

                } else {

                    echo '<script>

                            swal({
                              title: "¡OK!",
                              text: "¡Su mensaje ha sido enviado, muy pronto le responderemos!",
                              type: "success",
                              confirmButtonText: "Cerrar",
                              closeOnConfirm: false
                            },

                            function(isConfirm){
                                     if (isConfirm) {
                                            history.back();
                                        }
                            });

                        </script>';

                }

            } else {

                echo '<script>

                    swal({
                          title: "¡ERROR!",
                          text: "¡Problemas al enviar el mensaje, revise que no tenga caracteres especiales!",
                          type: "error",
                          confirmButtonText: "Cerrar",
                          closeOnConfirm: false
                    },

                    function(isConfirm){
                             if (isConfirm) {
                                window.location =  history.back();
                              }
                    });

                    </script>';

            }

        }

    }

    public function ctrVerificacion($verificacion)
    {

        if ($verificacion == 0) {

            if ($_SESSION["tipo"] == "Asesor") {

                date_default_timezone_set("America/Bogota");

                $url = Ruta::ctrRuta();

                $mail = new PHPMailer;

                $mail->CharSet = 'UTF-8';

                $mail->isMail();

                $mail->setFrom('usuarios@muliier.com', 'Muliier');

                $mail->addReplyTo('usuarios@muliier.com', 'Muliier');

                $mail->Subject = "Bienvenido a Muliier.com Gracias por Registrase..! ";

                $mail->addAddress($_SESSION["email"]);

                $mail->msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">

                            <center>

                                <img style="padding:20px; width:20%" src="https://www.muliier.com/vistas/img/plantilla/logo.png">

                            </center>

                            <div style="position:relative; margin:auto; width:600px; background:white; padding:20px">

                                <center>


                                <hr style="border:1px solid #ccc; width:80%">

                                <h2 style="font-weight:100;  padding:20px; color:#E50039; ">Hola ' . $_SESSION["nombre"] . '</h2>

                                <br>

                                <h3 style="font-weight:100;  padding:20px; color:#E50039;"> Te damos la bienvenida a nuestro equipo de trabajo de ahora en adelante podrás realizar tus pedidos de una forma rápida , fácil y segura en muliier contarás con nuestro equipo de trabajo para pasar al siguiente nivel de ingresos sin invertir dinero.</h3>


                                <br>

                                <hr style="border:1px solid #ccc; width:80%">

                                <h3 style="font-weight:100; color:#E50039;">Muchas Gracias Por Escogernos ...!</h3>

                                </center>

                            </div>

                        </div>');

                $envio = $mail->Send();

                session_destroy();

            } else {

                date_default_timezone_set("America/Bogota");

                $url = Ruta::ctrRuta();

                $mail = new PHPMailer;

                $mail->CharSet = 'UTF-8';

                $mail->isMail();

                $mail->setFrom('usuarios@muliier.com', 'Muliier');

                $mail->addReplyTo('usuarios@muliier.com', 'Muliier');

                $mail->Subject = "Bienvenido a Muliier.com Gracias por Registrase..!";

                $mail->addAddress($_SESSION["email"]);

                $mail->msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">

                            <center>

                                <img style="padding:20px; width:20%" src="https://www.muliier.com/vistas/img/plantilla/logo.png">

                            </center>

                            <div style="position:relative; margin:auto; width:600px; background:white; padding:20px">

                                <center>


                                <hr style="border:1px solid #ccc; width:80%">

                                <h2 style="font-weight:100; color:#E50039; padding:0 20px">Hola ' . $_SESSION["nombre"] . '</h2>

                                <br>

                                <h3 style="font-weight:100;  color:#757373; ">Bienvenido a muliier de ahora en adelante podrás encontrar aquí todo lo referente a tendencias en moda desde tus accesorios hasta que calzado comprar , todo esto y mucho más lo podrás encontrar aquí ! </h3>

                                <br>

Success!
                                <h3 style="font-weight:100;   color:#757373;">¡ Empieza ahora realizando tu primera compra</h3>



                                <br>

                                <hr style="border:1px solid #ccc; width:80%">

                                <h4 style="font-weight:100; color:#E50039;">Muchas Gracias Por Escogernos ...!</h4>

                                </center>

                            </div>

                        </div>');

                $envio = $mail->Send();

                session_destroy();

            }

        }

    }

}
