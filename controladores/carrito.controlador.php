<?php

class ControladorCarrito{

	/*=============================================
	MOSTRAR TARIFAS
	=============================================*/

	static public function ctrMostrarTarifas(){

		$tabla = "comercio";

		$respuesta = ModeloCarrito::mdlMostrarTarifas($tabla);

		return $respuesta;

	}	

	/*=============================================
	NUEVAS COMPRAS
	=============================================*/

	public function ctrNuevasCompras($datos){
		
		$tabla = "compras";

     
		$respuesta = ModeloCarrito::mdlNuevasCompras($tabla, $datos);
		
		//return $respuesta;

		if($respuesta == "ok"){
		    
		    
		    
		    
		    $tabla7 = "usuarios";
			$item7 = "id_usuario";
			$valor7 = $datos["idusuario"];

			$respuestauser = ModeloUsuarios::mdlMostrarUsuario($tabla7, $item7, $valor7);
			
			$direccion=$respuestauser["direccion_usuario"];
            $nombre=$respuestauser["nombre_usuario"];
            $celular=$respuestauser["celular_usuario"];
			

		    $tabla = "comentarios";

	
			ModeloUsuarios::mdlIngresoComentarios($tabla, $datos);

			/*=============================================
			ACTUALIZAR NOTIFICACIONES NUEVAS VENTAS
			=============================================*/

			$traerNotificaciones = ControladorNotificaciones::ctrMostrarNotificaciones();

			$nuevaVenta = $traerNotificaciones["nuevasVentas"] + 1;

			ModeloNotificaciones::mdlActualizarNotificaciones("notificaciones", "nuevasVentas", $nuevaVenta);
	
				$tabla2="asesores";
				$item2="codigo_asesor";
				$valor2=$datos["cod_asesor"];
			

				$respuesta_asesor = ModeloUsuarios::mdlMostrarAsesor($tabla2, $item2, $valor2);

				$tabla3 = "usuarios";
			    $item3 = "id_asesor_usuario";
			    $valor3 = $respuesta_asesor["id_asesor"];

			$respuesta_usuario = ModeloUsuarios::mdlMostrarUsuario($tabla3, $item3, $valor3);
			$tipo=$respuesta_usuario["tipo_usuario"];

	
   			if($tipo=="Asesor"){


   				$tabla4="asesores";
				//$idasesor = $respuesta_asesor["id"];
				$cantidad_vendidos=$respuesta_asesor["cantidad_vendidos_asesor"]+1;

	            $puntaje =$respuesta_asesor["puntaje_asesor"];
				if($cantidad_vendidos<=2){
    					$puntaje=1;
    				}else if($cantidad_vendidos<=6){
    					$puntaje=2;
    				}else if($cantidad_vendidos<=20){
    					$puntaje=3;
    				}else if($cantidad_vendidos<=50){
    					$puntaje=4;
    				}else if($cantidad_vendidos<=100){
    					$puntaje=5;
    			}


					$datos4 = array("cantidad_vendidos_asesor" => $cantidad_vendidos,
								   "puntaje_asesor" => $puntaje,
								    "id_asesor" => $valor3
								   );

				$resultado_asesor= ModeloUsuarios::mdlActualizarAsesor($tabla4, $datos4);

				
			


				$periodo=1;
				if($respuesta_asesor["cantidad_vendidos_asesor"]>=1){


		  					$tabla="ganancias_asesor";
							$item="id_asesor";
							$valor=$respuesta_asesor["id_asesor"];
					  		$verificar_ganancias = ModeloUsuarios::mdlMostrarGanancia($tabla, $item, $valor);


	                if($verificar_ganancias["id_asesor"]==null){
		              
		                $tabla5="ganancias";
		             	$fecha_inicio=date("Y-m-d H:i:s"); 
		             	$fecha_final=date("Y-m-t H:i:s"); 


						$datos5 = array("periodo" => $periodo,
										   "fecha_inicio" => $fecha_inicio,
										   "fecha_final" => $fecha_final,
										    "id_asesor" => $respuesta_asesor["id_asesor"]
										   );

						$resultado_ganancias= ModeloUsuarios::mdlIngresarGanancia($tabla5, $datos5);
						
						if($resultado_ganancias=="ok"){

	                        
							$tabla6="ganancias";
							$item6="id_asesor";
							$valor6=$respuesta_asesor["id_asesor"];
					  		$mostrar_ganancias = ModeloUsuarios::mdlMostrarGanancia($tabla6, $item6, $valor6);

						
		             	 $ganancia=$datos["pago"];
		             	 $cantidad_vendidos=$mostrar_ganancias["cantidad_vendidos"]+1;

						 $datos6 = array("ganancias_total" => $ganancia,
										   "cantidad_vendidos" => $cantidad_vendidos,
										    "id" => $mostrar_ganancias["id"]
										   );

						 $resultado_ganancias= ModeloUsuarios::mdlActualizarGanancia($tabla6, $datos6);

						}

				    }
					
			    else{


						$tabla6="ganancias";
						$item6="id_asesor_ganancia";
						$valor6=$respuesta_asesor["id_asesor"];
					  	$mostrar_ganancias = ModeloUsuarios::mdlMostrarGanancia($tabla6, $item6, $valor6);

						
		             	$ganancia=$mostrar_ganancias["ganancias_total_ganancia"]+$datos["pago"];
		             	$cantidad_vendidos=$mostrar_ganancias["cantidad_vendidos_ganancia"]+1;

						$datos6 = array("ganancias_total" => $ganancia,
										   "cantidad_vendidos" => $cantidad_vendidos,
										    "id" => $mostrar_ganancias["id_ganancia"]
										   );

						$resultado_ganancias= ModeloUsuarios::mdlActualizarGanancia($tabla6, $datos6);

					

					}


				}
				
   			}

		
           
			 /*=============================================
					ENVIO CORREO ELECTRÓNICO
			=============================================*/


					//Correo a Comprador


					date_default_timezone_set("America/Bogota");

					$url = Ruta::ctrRuta();	

					$mail = new PHPMailer;

					$mail->CharSet = 'UTF-8';

					$mail->isMail();

					$mail->setFrom('usuarios@muliier.com', 'Muliier');

					$mail->addReplyTo('usuarios@muliier.com', 'Muliier');

					$mail->Subject = "Acaban de realizar una compra CONTRAENTREGA";
					
					$mail->addAddress("yosoymuliier@gmail.com");

					//$mail->addAddress("everbtank@gmail.com");

				
				$mail->msgHTML('<div style="width:100%; background:#eee; position:relative;  font-family: serif; padding-bottom:0px">
				
                         		
						<div  class="caja1"  style="background-image: url(https://www.muliier.com/vistas/img/plantilla/banner.jpeg); position:relative; margin:auto; width:950px; padding:0px" >
								
								
							<div style="position:relative; margin:auto; width:400px; background:white; padding:50px">
							
								<center>
								<hr style="border:1px solid #ccc; width:80%">
								<hr style="border:1px solid #ccc; width:80%">

								
								<img style="padding:5px; width:53%" src="https://www.muliier.com/vistas/img/plantilla/muliier.png">

							</center>
							
								<center>
								

								<hr style="border:1px solid #ccc; width:80%">
								<hr style="border:1px solid #ccc; width:80%">

								<h2 style="font-weight:100; color:#E50039; padding:0 20px">Hola, querida administradora de Muliier. </h2>

							

								<h4 style="font-weight:100;  color:#757373; ">Te informamos amablemente que:   '.$nombre.', acaba de realizar su compra por el precio de   '.$datos["pago"].' COP. El método de compra fue contra entrega. Si deseas puedes verificar sus datos en panel de administración: backend.muliier.com</a>  </h4>
								
							
								<h4 style="font-weight:100;   color:#757373;"> Celular: '.$celular.' </h4>
								<h4 style="font-weight:100;   color:#757373;"> Dirección : '.$direccion.' </h4>
								<h4 style="font-weight:100;   color:#757373;"> Municipio: '.$datos["municipio"].' </h4>

								<hr style="border:1px solid #ccc; width:80%">

								<h3 style="font-weight:100; color:#E50039;">! Muchas Gracias! Contamos contigo…Siempre.</h3>

								</center>

							</div>
						</div>

					</div>');
					$envio = $mail->Send();


					
					date_default_timezone_set("America/Bogota");

					$url1 = Ruta::ctrRuta();	

					$mail1 = new PHPMailer;

					$mail1->CharSet = 'UTF-8';

					$mail1->isMail();

					$mail1->setFrom('usuarios@muliier.com', 'Muliier');

					$mail1->addReplyTo('usuarios@muliier.com', 'Muliier');

					$mail1->Subject = "Se acaba de registrar su compra CONTRAENTREGA";

					$mail1->addAddress($datos["email"]);

				
				$mail1->msgHTML(' 
				
				<div style="width:100%; background:#eee; position:relative;  font-family: serif;     padding-bottom:0px">
							
					<div  class="caja1"  style="background-image: url(https://www.muliier.com/vistas/img/plantilla/banner.jpeg); position:relative; margin:auto; width:950px; padding:0px" >
								
							<div style="position:relative; margin:auto; width:400px; background:white; padding:50px">
							  	<hr style="border:1px solid #ccc; width:80%">
							  	<hr style="border:1px solid #ccc; width:80%">
							  	
    							<center>
    								<img style="padding:5px; width:50%; height:75%;"   src="https://www.muliier.com/vistas/img/plantilla/muliier.png">
    
    							</center>
							
								<center>
								

								<hr style="border:1px solid #ccc; width:80%">
								<hr style="border:1px solid #ccc; width:80%">
								
                         
                            
								<h1 style="font-weight:100; color:#E50039; padding:0 20px;">! Hola  '.$nombre.' !   </h1>
								
								<h2 style="font-weight:100; color:#E50039; padding:0 20px">Recibe un cordial saludo</h2>

								<hr style="border:1px solid #ccc; width:100%">
								<hr style="border:1px solid #ccc; width:100%">

								<h3 style="font-weight:100;  color:#757373; "><strong> De parte del equipo de Muliier te informamos que acabas de realizar tu compra por valor de   '.$datos["pago"].' COP.  Por el método de pago fue CONTRA ENTREGA. </strong></h3>
								
								<!--<h3 style="font-weight:100;  color:#757373; ">Si deseas verificar tus datos en el perfil  utiliza el siguiente usuario: muliier.com/perfil  </h3>-->
								
							
								
								<p style="font-weight:100;   color:#757373;">celular: '.$celular.' </p>
								<p style="font-weight:100;   color:#757373;">
								      direccion : '.$direccion.'</p>
								<p style="font-weight:100;   color:#757373;">municipio: '.$datos["municipio"].' </p>
							
                               	<hr style="border:1px solid #ccc; width:100%">
								<hr style="border:1px solid #ccc; width:100%">
								
								<h4 style="font-weight:100;   color:#757373;"> Un asesor del equipo Muliier, te contactará a la mayor brevedad posible para organizar los detalles de la compra CONTRA ENTREGA. Si deseas puedes escribirnos vía  What’s App con nombre y número de pedido que aparece en el módulo compras de tu perfil. </h4>
								
								<h5 style="font-weight:100;   color:#757373;"> Recuerda que contamos con todas las medidas de seguridad y protección sanitarias para tu envío.</h5>
                               <!--<br>
                               
							   <h4  style="font-weight:100; color:#7375B6;">Por ahora en método de compra contra entrega solo está disponible para Barranquilla y los municipios Soledad, Malambo, Puerto Colombia, Villa Campestre y la playa.</h4>

								<br>-->

								<hr style="border:1px solid #ccc; width:80%">

								<h2 style="font-weight:100; color:#E50039;">Muchas Gracias por confiar en nosotros. ¡Los mejores deseos!</h2>
								<hr style="border:1px solid #ccc; width:80%">

								</center>
							 </div>
					

					    </div>

					</div>');

					$envio = $mail1->Send();
			
        	
        
         
       
        	
		   
        
           

					
		        

			   return $respuesta;
    
			
		    }  
		   else{

			echo '<script> 

							swal({
								  title: "¡ERROR!",
								  text: "¡Ha ocurrido al Registrarse la Compra porfavor Intente de nuevo si Continua el error comuniquese al numero o email de contacto con nuestro equipo. !Gracias por la Compresion",
								  type:"error",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
								},

								function(isConfirm){

									if(isConfirm){
										history.back();
									}
							});

						</script>';

	       }
		

		
	//	return $respuesta;

	}


	

	/*=============================================
	VERIFICAR PRODUCTO COMPRADO
	=============================================*/

	static public function ctrVerificarProducto($datos){

		$tabla = "compras";

		$respuesta = ModeloCarrito::mdlVerificarProducto($tabla, $datos);
	 
	    return $respuesta;

		
	}

}