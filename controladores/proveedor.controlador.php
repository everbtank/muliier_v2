<?php

class ControladorProveedor{

	/*=============================================
	REGISTRO DE USUARIO
	=============================================*/

	public function ctrRegistroProveedor(){


	
		if(isset($_POST["regProveedor"])){


			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["regProveedor"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["regEmailproveedor"]) &&
			   preg_match('/^[a-zA-Z0-9]+$/', $_POST["regPasswordproveedor"])){

			   	$encriptar = crypt($_POST["regPasswordproveedor"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$
			   		$2a$07$asxx54ahjppf45sd87a5auxq/SS293XhTEeizKWMnfhnpfay0AALe');

			   	$encriptarEmail = md5($_POST["regProveedor"]);
               
                
				$datos = array("nombre"=>$_POST["regProveedor"],
							   "password"=> $encriptar,
							   "email"=> $_POST["regEmailproveedor"],
							   "foto"=>"",
							   "modo"=> "directo",
							   "verificacion"=> 1,
							   "emailEncriptado"=>$encriptarEmail);

				$tabla = "proveedores";
				

				//echo '<script>alert("hola ever01");</script>';
        
				$respuesta = ModeloProveedor::mdlRegistroProveedor($tabla, $datos);
			

				if($respuesta == "ok"){

				

				

					$traerNotificaciones = ControladorNotificaciones::ctrMostrarNotificaciones();

					$nuevosProveedor = $traerNotificaciones["nuevosProveedor"] + 1;

					ModeloNotificaciones::mdlActualizarNotificaciones("notificaciones", "nuevosProveedor", $nuevosProveedor);

                  	//echo '<script>alert("hola ever01");</script>';
					/*=============================================
					VERIFICACIÓN CORREO ELECTRÓNICO
					=============================================*/

					date_default_timezone_set("America/Bogota");

					$url = Ruta::ctrRuta();	

					$mail = new PHPMailer;

					$mail->CharSet = 'UTF-8';

					$mail->isMail();

					$mail->setFrom('usuarios@muliier.com', 'Muliier');

					$mail->addReplyTo('usuarios@muliier.com', 'Muliier');


					$mail->Subject = "Por favor verifique su dirección de correo electrónico";

				
					$mail->addAddress($_POST["regEmailproveedor"]);

					$mail->msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">
						
						<center>
							
							<img style="padding:20px; width:10%" src="https://www.muliier.com/vistas/img/plantilla/logo.png">

						</center>

						<div style="position:relative; margin:auto; width:600px; background:white; padding:20px">
						
							<center>
							
							<img style="padding:20px; width:15%" src="https://www.muliier.com/vistas/img/plantilla/icon-email.png">

							<h3 style="font-weight:100; color:#E50039">VERIFIQUE SU DIRECCIÓN DE CORREO ELECTRÓNICO</h3>

							<hr style="border:1px solid #E50039; width:80%">

							<h4 style="font-weight:100; color:#E50039; padding:0 20px">Para comenzar a usar su cuenta de Tienda Virtual, debe confirmar su dirección de correo electrónico</h4>

							<a href="'.$url.'verificar/'.$encriptarEmail.'" target="_blank" style="text-decoration:none">

							<div style="line-height:60px; background:#E50039; width:60%; color:white">Verifique su dirección de correo electrónico</div>

							</a>

							<br>

							<hr style="border:1px solid #E50039; width:80%">

							<h5 style="font-weight:100; color:#E50039">Si no se inscribió en esta cuenta, puede ignorar este correo electrónico y la cuenta se eliminará.</h5>

							</center>

						</div>

					</div>');

					$envio = $mail->Send();

					if(!$envio){

						echo '<script> 

							swal({
								  title: "¡ERROR!",
								  text: "¡Ha ocurrido un problema enviando verificación de correo electrónico a '.$_POST["regEmailproveedor"].$mail->ErrorInfo.'!",
								  type:"error",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
								},

								function(isConfirm){

									if(isConfirm){
										history.back();
									}
							});

						</script>';

					}else{

						echo '<script> 

							swal({
								  title: "¡OK!",
								  text: "¡Por favor revise la bandeja de entrada o la carpeta de SPAM de su correo electrónico '.$_POST["regEmailproveedor"].' para verificar la cuenta!",
								  type:"success",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
								},

								function(isConfirm){

									if(isConfirm){
										history.back();
									}
							});

						</script>';

					}

				}

			}else{

				echo '<script> 

						swal({
							  title: "¡ERROR!",
							  text: "¡Error al registrar el usuario, no se permiten caracteres especiales!",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},

							function(isConfirm){

								if(isConfirm){
									history.back();
								}
						});

				</script>';

			}

		}

	}

	/*=============================================
	MOSTRAR PROVEEDOR
	=============================================*/

	static public function ctrMostrarProveedor($item, $valor){

		$tabla = "proveedores";

		$respuesta = ModeloProveedor::mdlMostrarProveedor($tabla, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	ACTUALIZAR USUARIO
	=============================================*/

	static public function ctrActualizarProveedor($id, $item, $valor){

		$tabla = "proveedores";

		$respuesta = ModeloProveedor::mdlActualizarProveedor($tabla, $id, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	INGRESO DE USUARIO
	=============================================*/

	public function ctrIngresoProveedor(){

		if(isset($_POST["ingEmailproveedor"])){
          
          $_SESSION["tipo"] = "Proveedor";

			if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["ingEmailproveedor"]) &&
			   preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingPasswordproveedor"])){

				$encriptar = crypt($_POST["ingPasswordproveedor"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
			 
				$tabla = "proveedores";
				$item = "email_proveedor";
				$valor = $_POST["ingEmailproveedor"];

				$respuesta = ModeloProveedor::mdlMostrarProveedor($tabla, $item, $valor);

				if($respuesta["email_proveedor"] == $_POST["ingEmailproveedor"] && $respuesta["password_proveedor"] == $encriptar){
					
					if($respuesta["verificacion_proveedor"] == 1){

						echo'<script>

							swal({
								  title: "¡NO HA VERIFICADO SU CORREO ELECTRÓNICO!",
								  text: "¡Por favor revise la bandeja de entrada o la carpeta de SPAM de su correo para verififcar la dirección de correo electrónico '.$respuesta["email"].'!",
								  type: "error",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
							},

							function(isConfirm){
									 if (isConfirm) {	   
									    history.back();
									  } 
							});

							</script>';

					}else{

                            if(isset($_POST["g-recaptcha-response"])){
	
							$secret = "6LeFe9QUAAAAAPNnZgGzjHp4z6gLirOEJU8_I5Nh";

							$response = $_POST["g-recaptcha-response"];

							$remoteip = $_SERVER["REMOTE_ADDR"];	

							$result = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$remoteip");

							$array = json_decode($result,TRUE);

							if($array["success"])


									$_SESSION["validarSesion"] = "ok";
									$_SESSION["id_proveedor"] = $respuesta["id_proveedor"];
									$_SESSION["nombre"] = $respuesta["nombre_proveedor"];
									$_SESSION["foto"] = $respuesta["foto_proveedor"];
									$_SESSION["email"] = $respuesta["email_proveedor"];
									$_SESSION["password"] = $respuesta["password_proveedor"];
									$_SESSION["modo"] = $respuesta["modo_proveedor"];
									$_SESSION["tipo"] = "Proveedor";
									
									$_SESSION["celular"] = $respuesta["celular_proveedor"];
									$_SESSION["direccion"] = $respuesta["direccion_proveedor"];
									$_SESSION["municipio"] = $respuesta["municipio_proveedor"];
									$_SESSION["empresa"] = $respuesta["empresa_proveedor"];
									$_SESSION["descripcion"] = $respuesta["descripcion_proveedor"];
									$_SESSION["tipo_productos"] = $respuesta["tipo_productos_proveedor"];


								if(	$_SESSION["celular"]!="" && $_SESSION["direccion"]!=""&& $_SESSION["empresa"]!="" ){
								    
								    	echo '<script>
									
									window.location = localStorage.getItem("rutaActual");

								</script>';
								    
								} 
								else{
									
                                   echo '<script> 

									swal({
										  title: "¡ Actualizar sus datos!",
										  text: "¡Bienvenido le sugirimos actualizar sus datos para poder Realizar sus Registros de productos como Proveedor!",
										  type:"success",
										  confirmButtonText: "Cerrar",
										  closeOnConfirm: false
										},

										function(isConfirm){

											if(isConfirm){
												history.back();
												window.location = "perfil";
												 $("#perfil").tabs({ active: 2 });
											}
									});

								</script>';


									}
                            }

								else{

											echo'<script>

									swal({
										  title: "¡ERROR AL INGRESAR!",
										  text: "¡Debe demostrar que no es un Robot!",
										  type: "error",
										  confirmButtonText: "Cerrar",
										  closeOnConfirm: false
									},

									function(isConfirm){
											 if (isConfirm) {	   
											    window.location = localStorage.getItem("rutaActual");
											  } 
									});

									</script>';
								


								}



						}

				}else{

					echo'<script>

							swal({
								  title: "¡ERROR AL INGRESAR!",
								  text: "¡Por favor revise que el email exista o la contraseña coincida con la registrada!",
								  type: "error",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
							},

							function(isConfirm){
									 if (isConfirm) {	   
									    window.location = localStorage.getItem("rutaActual");
									  } 
							});

							</script>';

				}

			}else{

				echo '<script> 

						swal({
							  title: "¡ERROR!",
							  text: "¡Error al ingresar al sistema, no se permiten caracteres especiales!",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},

							function(isConfirm){

								if(isConfirm){
									history.back();
								}
						});

				</script>';

			}

		}

	}

	/*=============================================
	OLVIDO DE CONTRASEÑA
	=============================================*/

	public function ctrOlvidoPassword(){

		if(isset($_POST["passEmail"])){

			if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["passEmail"])){

				/*=============================================
				GENERAR CONTRASEÑA ALEATORIA
				=============================================*/

				function generarPassword($longitud){

					$key = "";
					$pattern = "1234567890abcdefghijklmnopqrstuvwxyz";

					$max = strlen($pattern)-1;

					for($i = 0; $i < $longitud; $i++){

						$key .= $pattern{mt_rand(0,$max)};

					}

					return $key;

				}

				$nuevaPassword = generarPassword(11);

				$encriptar = crypt($nuevaPassword, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				$tabla = "proveedores";

				$item1 = "email_proveedor";
				$valor1 = $_POST["passEmail"];

				$respuesta1 = ModeloProveedor::mdlMostrarProveedor($tabla, $item1, $valor1);

				if($respuesta1){

					$id = $respuesta1["id_proveedor"];
					$item2 = "password_proveedor";
					$valor2 = $encriptar;

					$respuesta2 = ModeloProveedor::mdlActualizarProveedor($tabla, $id, $item2, $valor2);

					if($respuesta2  == "ok"){

						/*=============================================
						CAMBIO DE CONTRASEÑA
						=============================================*/

						date_default_timezone_set("America/Bogota");

						$url = Ruta::ctrRuta();	

						$mail = new PHPMailer;

						$mail->CharSet = 'UTF-8';

						$mail->isMail();

						$mail->setFrom('usuarios@muliier.com', 'Muliier');

						$mail->addReplyTo('usuarios@muliier.com', 'Muliier');

						$mail->Subject = "Solicitud de nueva contraseña";

						$mail->addAddress($_POST["passEmail"]);

						$mail->msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">
	
								<center>
									
									<img style="padding:20px; width:10%" src="https://www.muliier.com/vistas/img/plantilla/logo.png">

								</center>

								<div style="position:relative; margin:auto; width:600px; background:white; padding:20px">
								
									<center>
									
									<img style="padding:20px; width:15%" src="hhttps://www.muliier.com/vistas/img/plantilla/icon-pass.png">

									<h3 style="font-weight:100; color:#999">SOLICITUD DE NUEVA CONTRASEÑA</h3>

									<hr style="border:1px solid #ccc; width:80%">

									<h4 style="font-weight:100; color:#999; padding:0 20px"><strong>Su nueva contraseña: </strong>'.$nuevaPassword.'</h4>

									<a href="'.$url.'" target="_blank" style="text-decoration:none">

									<div style="line-height:60px; background:#0aa; width:60%; color:white">Ingrese nuevamente al sitio</div>

									</a>

									<br>

									<hr style="border:1px solid #ccc; width:80%">

									<h5 style="font-weight:100; color:#999">Si no se inscribió en esta cuenta, puede ignorar este correo electrónico y la cuenta se eliminará.</h5>

									</center>

								</div>

							</div>');

						$envio = $mail->Send();

						if(!$envio){

							echo '<script> 

								swal({
									  title: "¡ERROR!",
									  text: "¡Ha ocurrido un problema enviando cambio de contraseña a '.$_POST["passEmail"].$mail->ErrorInfo.'!",
									  type:"error",
									  confirmButtonText: "Cerrar",
									  closeOnConfirm: false
									},

									function(isConfirm){

										if(isConfirm){
											history.back();
										}
								});

							</script>';

						}else{

							echo '<script> 

								swal({
									  title: "¡OK!",
									  text: "¡Por favor revise la bandeja de entrada o la carpeta de SPAM de su correo electrónico '.$_POST["passEmail"].' para su cambio de contraseña!",
									  type:"success",
									  confirmButtonText: "Cerrar",
									  closeOnConfirm: false
									},

									function(isConfirm){

										if(isConfirm){
											history.back();
										}
								});

							</script>';

						}

					}

				}else{

					echo '<script> 

						swal({
							  title: "¡ERROR!",
							  text: "¡El correo electrónico no existe en el sistema!",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},

							function(isConfirm){

								if(isConfirm){
									history.back();
								}
						});

					</script>';


				}

			}else{

				echo '<script> 

						swal({
							  title: "¡ERROR!",
							  text: "¡Error al enviar el correo electrónico, está mal escrito!",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},

							function(isConfirm){

								if(isConfirm){
									history.back();
								}
						});

				</script>';

			}

		}

	}

	
	/*===========================================
	MOSTRAR PRODUCTOS
	=============================================*/
                         

	static public function ctrMostrarProductos($item, $valor){

	


		$tabla = "productos";

		$respuesta = ModeloProveedor::mdlMostrarProductos($tabla, $item, $valor);

		return $respuesta;

	}


	static public function ctrMostrarCategorias($item, $valor){

		$tabla = "categorias";

		$respuesta = ModeloProveedor::mdlMostrarCategorias($tabla, $item, $valor);

		return $respuesta;

	}

	static public function ctrMostrarSubCategorias($item, $valor){

		$tabla = "subcategorias";

		$respuesta = ModeloProveedor::mdlMostrarSubCategorias($tabla, $item, $valor);

		return $respuesta;
	
	}



	static public function ctrRegistroRedesSociales($datos){

		$tabla = "proveedores";
		$item = "email_proveedor";
		$valor = $datos["email"];
		$emailRepetido = false;


		$respuesta0 = ModeloProveedor::mdlMostrarProveedor($tabla, $item, $valor);

		if($respuesta0){

			if($respuesta0["modo_proveedor"] != $datos["modo"]){

				echo '<script> 

						swal({
							  title: "¡ERROR!",
							  text: "¡El correo electrónico '.$datos["email"].', ya está registrado en el sistema con un método diferente a Google!",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},

							function(isConfirm){

								if(isConfirm){
									history.back();
								}
						});

				</script>';

				$emailRepetido = false;

			}

			$emailRepetido = true;

		}else{

			$respuesta1 = ModeloProveedor::mdlRegistroProveedor($tabla, $datos);

		}

		if($emailRepetido || $respuesta1 == "ok"){

			$respuesta2 = ModeloProveedor::mdlMostrarProveedor($tabla, $item, $valor);

			if($respuesta2["modo_proveedor"] == "facebook"){

				session_start();

				$_SESSION["validarSesion"] = "ok";
				$_SESSION["id"] = $respuesta2["id_proveedor"];
				$_SESSION["nombre"] = $respuesta2["nombre_proveedor"];
				$_SESSION["foto"] = $respuesta2["foto_proveedor"];
				$_SESSION["email"] = $respuesta2["email_proveedor"];
				$_SESSION["password"] = $respuesta2["password_proveedor"];
				$_SESSION["modo"] = $respuesta2["modo_proveedor"];
				
				$_SESSION["celular"] = $respuesta2["celular_proveedor"];
				$_SESSION["direccion"] = $respuesta2["direccion_proveedor"];
				$_SESSION["municipio"] = $respuesta2["municipio_proveedor"];
				$_SESSION["empresa"] = $respuesta2["empresa_proveedor"];
				$_SESSION["descripcion"] = $respuesta2["descripcion_proveedor"];
				$_SESSION["tipo"] = "Proveedor";


				echo "ok";

			}else if($respuesta2["modo"] == "google"){

				$_SESSION["validarSesion"] = "ok";
				$_SESSION["id"] = $respuesta2["id_proveedor"];
				$_SESSION["nombre"] = $respuesta2["nombre_proveedor"];
				$_SESSION["foto"] = $respuesta2["foto_proveedor"];
				$_SESSION["email"] = $respuesta2["email_proveedor"];
				$_SESSION["password"] = $respuesta2["password_proveedor"];
				$_SESSION["modo"] = $respuesta2["modo_proveedor"];
				
				$_SESSION["celular"] = $respuesta2["celular_proveedor"];
				$_SESSION["dirección"] = $respuesta2["dirección_proveedor"];
				$_SESSION["municipio"] = $respuesta2["municipio_proveedor"];
				$_SESSION["empresa"] = $respuesta2["empresa_proveedor"];
				$_SESSION["descripcion"] = $respuesta2["descripcion_proveedor"];
				$_SESSION["tipo"] = "Proveedor";

				echo "<span style='color:white'>ok</span>";

			}

			else{

				echo "";
			}

		}
	}

	/*=============================================
	ACTUALIZAR PERFIL
	=============================================*/

	public function ctrActualizarPerfil(){



		if(isset($_POST["editarNombre1"])){

			/*=============================================
			VALIDAR IMAGEN
			=============================================*/
			
			$_SESSION["tipo"] = "Proveedor";

			$ruta = $_POST["fotoProveedor1"];

          
			if(isset($_FILES["datosImagen1"]["tmp_name"]) && !empty($_FILES["datosImagen1"]["tmp_name"])){
               
              
				/*=============================================
				PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD
				=============================================*/

				$directorio = "vistas/img/proveedor/".$_POST["idProveedor1"];

				if(!empty($_POST["fotoProveedor1"])){

					unlink($_POST["fotoProveedor1"]);
				
				}else{

					mkdir($directorio, 0755);

				}

				/*=============================================
				GUARDAMOS LA IMAGEN EN EL DIRECTORIO
				=============================================*/

				list($ancho, $alto) = getimagesize($_FILES["datosImagen1"]["tmp_name"]);

				$nuevoAncho = 500;
				$nuevoAlto = 500;	

				$aleatorio = mt_rand(100, 999);

				if($_FILES["datosImagen1"]["type"] == "image/jpeg"){

					$ruta = "vistas/img/proveedor/".$_POST["idProveedor1"]."/".$aleatorio.".jpg";

					/*=============================================
					MOFICAMOS TAMAÑO DE LA FOTO
					=============================================*/

					$origen = imagecreatefromjpeg($_FILES["datosImagen1"]["tmp_name"]);

					$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

					imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

					imagejpeg($destino, $ruta);

				}

					
				if($_FILES["datosImagen1"]["type"] == "image/png"){

					$ruta = "vistas/img/proveedor/".$_POST["idProveedor1"]."/".$aleatorio.".png";

					/*=============================================
					MOFICAMOS TAMAÑO DE LA FOTO
					=============================================*/

					$origen = imagecreatefrompng($_FILES["datosImagen1"]["tmp_name"]);

					$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

					imagealphablending($destino, FALSE);
    			
					imagesavealpha($destino, TRUE);

					imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

					imagepng($destino, $ruta);

				}

			}

			if($_SESSION["modo"]=="directo"){

					

				if($_POST["editarPassword1"] == ""){

					$password = $_POST["passProveedor1"];

				}else{

					$password = crypt($_POST["editarPassword1"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				}

				$datos = array("nombre" => $_POST["editarNombre1"],
							   "email" => $_POST["editarEmail1"],
							   "password" => $password,
							   "foto" => $ruta,
							   "celular" => $_POST["editarcelular1"],
							   "direccion" => $_POST["editardireccion1"],
							   "municipio" => $_POST["editarmunicipio1"],
							   "empresa" => $_POST["editarempresa1"],
							   "id" => $_POST["idProveedor1"]);

				$tabla = "proveedor";

				$respuesta = ModeloProveedor::mdlActualizarPerfil($tabla, $datos);

				var_dump($respuesta);

				if($respuesta == "ok"){

					$_SESSION["validarSesion"] = "ok";
					$_SESSION["id"] = $datos["id"];
					$_SESSION["nombre"] = $datos["nombre"];
					$_SESSION["foto"] = $datos["foto"];
					$_SESSION["email"] = $datos["email"];
					$_SESSION["password"] = $datos["password"];
					$_SESSION["celular"] = $datos["celular"];
					$_SESSION["direccion"] = $datos["direccion"];
					$_SESSION["municipio"] = $datos["municipio"];
					$_SESSION["empresa"] = $datos["empresa"];
					$_SESSION["modo"] = "directo";

					echo '<script> 

							swal({
								  title: "¡OK!",
								  text: "¡Su cuenta ha sido actualizada correctamente!",
								  type:"success",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
								},

								function(isConfirm){

									if(isConfirm){
										history.back();
									}
							});

					</script>';

				}


			} else{

					if($_POST["editarPassword1"] == ""){

						$password = $_POST["passProveedor1"];

					}else{

						$password = crypt($_POST["editarPassword1"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

					}

					$datos = array("nombre" => $_POST["editarNombre1"],
								   "email" => $_POST["editarEmail1"],
								   "password" => $password,
								   "foto" => $ruta,
								   "celular" => $_POST["editarcelular1"],
								   "direccion" => $_POST["editardireccion1"],
								   "municipio" => $_POST["editarmunicipio1"],
								   "empresa" => $_POST["editarempresa1"],
								   "id" => $_POST["idProveedor1"]);

					$tabla = "proveedor";

					$respuesta = ModeloProveedor::mdlActualizarPerfil($tabla, $datos);

					var_dump($respuesta);

					if($respuesta == "ok"){

						$_SESSION["validarSesion"] = "ok";
						$_SESSION["id"] = $datos["id"];
						$_SESSION["nombre"] = $datos["nombre"];
						$_SESSION["foto"] = $datos["foto"];
						$_SESSION["email"] = $datos["email"];
						$_SESSION["password"] = $datos["password"];
						$_SESSION["modo"] = $_POST["modoUsuario"];
						$_SESSION["celular"] = $datos["celular"];
						$_SESSION["direccion"] = $datos["direccion"];
						$_SESSION["municipio"] = $datos["municipio"];
						$_SESSION["empresa"] = $datos["empresa"];
						$_SESSION["modo"] = $datos["modo"];

						echo '<script> 

								swal({
									  title: "¡OK!",
									  text: "¡Su cuenta ha sido actualizada correctamente!",
									  type:"success",
									  confirmButtonText: "Cerrar",
									  closeOnConfirm: false
									},

									function(isConfirm){

										if(isConfirm){
											history.back();
										}
								});

						</script>';


					}
				}
			}

		

	}


public function ctrActualizarDatos(){


		if(isset($_POST["editarempresa1"])){

			$datos = array("empresa" => $_POST["editarempresa1"],
							   "descripcion" => $_POST["editardescripcion1"],
							   "tipo_productos" => $_POST["editartipo_productos1"],
							   "id" => $_POST["idProveedor"]);

				$tabla = "proveedores";

				$respuesta = ModeloProveedor::mdlActualizarDatos($tabla, $datos);

				var_dump($respuesta);

				if($respuesta == "ok"){

					$_SESSION["validarSesion"] = "ok";
					$_SESSION["id"] = $datos["id"];
					$_SESSION["empresa"] = $datos["empresa"];
					$_SESSION["descripcion"] = $datos["descripcion"];
					$_SESSION["tipo_productos"] = $datos["tipo_productos"];
					$_SESSION["modo"] = "directo";

					echo '<script> 

							swal({
								  title: "¡OK!",
								  text: "¡Su cuenta ha sido actualizada correctamente!",
								  type:"success",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
								},

								function(isConfirm){

									if(isConfirm){
										history.back();
									}
							});

					</script>';

				}



		}

}


	/*=============================================
	MOSTRAR COMPRAS
	=============================================*/



	/*=============================================
	MOSTRAR COMENTARIOS EN PERFIL
	=============================================*/

	/*static public function ctrMostrarComentariosPerfil($datos){

		$tabla = "comentarios";

		$respuesta = ModeloProveedor::mdlMostrarComentariosPerfil($tabla, $datos);

		return $respuesta;

	}*/


	/*=============================================
	ACTUALIZAR COMENTARIOS
	=============================================*/

	public function ctrActualizarComentario(){

		if(isset($_POST["idComentario"])){

			if(preg_match('/^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["comentario"])){

				if($_POST["comentario"] != ""){

					$tabla = "comentarios";

					$datos = array("id"=>$_POST["idComentario"],
								   "calificacion"=>$_POST["puntaje"],
								   "comentario"=>$_POST["comentario"]);

					$respuesta = ModeloProveedor::mdlActualizarComentario($tabla, $datos);

					if($respuesta == "ok"){

						echo'<script>

								swal({
									  title: "¡GRACIAS POR COMPARTIR SU OPINIÓN!",
									  text: "¡Su calificación y comentario ha sido guardado!",
									  type: "success",
									  confirmButtonText: "Cerrar",
									  closeOnConfirm: false
								},

								function(isConfirm){
										 if (isConfirm) {	   
										   history.back();
										  } 
								});

							  </script>';

					}

				}else{

					echo'<script>

						swal({
							  title: "¡ERROR AL ENVIAR SU CALIFICACIÓN!",
							  text: "¡El comentario no puede estar vacío!",
							  type: "error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
						},

						function(isConfirm){
								 if (isConfirm) {	   
								   history.back();
								  } 
						});

					  </script>';

				}	

			}else{

				echo'<script>

					swal({
						  title: "¡ERROR AL ENVIAR SU CALIFICACIÓN!",
						  text: "¡El comentario no puede llevar caracteres especiales!",
						  type: "error",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
					},

					function(isConfirm){
							 if (isConfirm) {	   
							   history.back();
							  } 
					});

				  </script>';

			}

		}

	}

	/*=============================================
	AGREGAR A LISTA DE DESEOS
	=============================================*/

	static public function ctrAgregarDeseo($datos){

		$tabla = "deseos";

		$respuesta = ModeloProveedor::mdlAgregarDeseo($tabla, $datos);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR LISTA DE DESEOS
	=============================================*/

	static public function ctrMostrarDeseos($item){

		$tabla = "deseos";

		$respuesta = ModeloProveedor::mdlMostrarDeseos($tabla, $item);

		return $respuesta;

	}

	/*=============================================
	QUITAR PRODUCTO DE LISTA DE DESEOS
	=============================================*/
	static public function ctrQuitarDeseo($datos){

		$tabla = "deseos";

		$respuesta = ModeloProveedor::mdlQuitarDeseo($tabla, $datos);

		return $respuesta;

	}

	/*=============================================
	ELIMINAR USUARIO
	=============================================*/

	public function ctrEliminarProveedor(){

		if(isset($_GET["id"])){

			$tabla1 = "proveedores";		
			//$tabla2 = "comentarios";
			//$tabla3 = "compras";
			//$tabla4 = "deseos";

			$id = $_GET["id"];

			if($_GET["foto"] != ""){

				unlink($_GET["foto"]);
				rmdir('vistas/img/proveedor/'.$_GET["id"]);

			}

			$respuesta = ModeloProveedor::mdlEliminarUsuario($tabla1, $id);
			
			//ModeloProveedor::mdlEliminarComentarios($tabla2, $id);

			//ModeloProveedor::mdlEliminarProductos($tabla3, $id);

			//ModeloProveedor::mdlEliminarListaDeseos($tabla4, $id);

			if($respuesta == "ok"){

		    	$url = Ruta::ctrRuta();

		    	echo'<script>

						swal({
							  title: "¡SU CUENTA HA SIDO BORRADA!",
							  text: "¡Debe registrarse nuevamente si desea ingresar!",
							  type: "success",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
						},

						function(isConfirm){
								 if (isConfirm) {	   
								   window.location = "'.$url.'salir";
								  } 
						});

					  </script>';

		    }

		}

	}

}

	