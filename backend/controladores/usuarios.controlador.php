<?php

class ControladorUsuarios{

	/*=============================================
	MOSTRAR TOTAL USUARIOS
	=============================================*/

	static public function ctrMostrarTotalUsuarios($orden){

		$tabla = "usuarios";

		$respuesta = ModeloUsuarios::mdlMostrarTotalUsuarios($tabla, $orden);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR USUARIOS
	=============================================*/

	static public function ctrMostrarUsuarios($item, $valor){

		$tabla = "usuarios";

		$respuesta = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);

		return $respuesta;
	
	}

	


	static public function ctrMostrarAsesor($item, $valor){

		$tabla = "asesores";

		$respuesta = ModeloUsuarios::mdlMostrarAsesor($tabla, $item, $valor);

		return $respuesta;
	
	}

	static public function ctrMostrarClientes($item, $valor){

		$tabla = "clientes";

		$respuesta = ModeloUsuarios::mdlMostrarClientes($tabla, $item, $valor);

		return $respuesta;
	
	}
	static public function ctrMostrarUsuarios_asesor($item, $valor){

		$tabla = "usuarios";

		$respuesta = ModeloUsuarios::mdlMostrarUsuarios_asesor($tabla, $item, $valor);

		return $respuesta;
	
	}

	

}