<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="inicio" class="brand-link">
      <img src="vistas/img/plantilla/icono.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-danger text-danger">
        <center><h2>Muliier</h2></center>
      </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
  
        <div class="image">
        <?php

            if($_SESSION["foto"] == ""){

              echo '<img src="vistas/img/perfiles/default/anonymous.png" heightclass="brand-image img-circle elevation-3" alt="User Image" width="60" height="60">';

            }else{

              echo '<img src="'.$_SESSION["foto"].'" class="brand-image img-circle elevation-2" alt="User Image" width="60" height="60">';

            }


            ?> 
          
        </div>
        <div class="info">
          
          <span class="hidden-xs"><?php echo $_SESSION["nombre"]; ?></span>
        </div>
      </div>

   

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item ">
            <a href="inicio" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Inicio
              </p>
            </a>
          
          </li>
          <li class="nav-item">
            <a href="comercio" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                 Comercio
                <span class="right badge badge-danger">Panel</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="slide" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Slide
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Categorias
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="categorias" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Categoria</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="subcategorias" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Subcategoria</p>
                </a>
              </li>
              
            </ul>
          </li>
          <li class="nav-item">
            <a href="productos" class="nav-link">
              <i class="nav-icon fas fa-tree"></i>
              <p>
                Productos
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="banner" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Banner
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="ventas" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Ventas
               
              </p>
            </a>   
          </li>
          <li class="nav-item">
            <a href="visitas" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Visitas
             
              </p>
            </a>   
          </li>

          <li class="nav-header">Usuarios</li>
          <li class="nav-item">
            <a href="proveedor" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Proveedor
              
              </p>
            </a>
          </li>

           <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Usuarios
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="usuarios" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Usuario</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="asesor" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Asesor</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="clientes" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cliente</p>
                </a>
              </li>
              
            </ul>
          </li>
          
          <li class="nav-item">
            <a href="perfiles" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Perfiles
                <i class="fas fa-angle-left right"></i>
                  <span class="badge badge-info right">Admin</span>
              </p>
            </a>    
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>