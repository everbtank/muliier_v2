<?php

if($_SESSION["perfil"] != "administrador"){

echo '<script>

  window.location = "inicio";

</script>';

return;

}

?>

<div class="content-wrapper">


  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"> Gestor comercio</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Gestor comercio</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

  <section class="content">

    <div class="row">

      <div class="col-md-6 col-xs-12">
        
      <!--=====================================
      BLOQUE 1
      ======================================-->
      
      <?php

        /*=============================================
        ADMINISTRACIÓN DE LOGOTIPO E ICONO
        =============================================*/

        include "comercio/logotipo.php";

        /*=====================================
        ADMINISTRAR COLORES
        ======================================*/
  
        include "comercio/colores.php";

        /*=====================================
        ADMINISTRAR REDES SOCIALES
        ======================================*/
  
        include "comercio/redSocial.php";
        
      ?>
      
      </div>


      <div class="col-md-6">
        
      <!--=====================================
      BLOQUE 2
      ======================================-->

        <?php
        
       /*=====================================
        ADMINISTRAR CÓDIGOS
        ======================================*/
  
        include "comercio/codigos.php";

        /*=====================================
        ADMINISTRAR COMERCIO
        ======================================*/
  
        include "comercio/informacion.php";

        ?>
   
      </div>

    </div>
 
  </section>

</div>