<!--=====================================
PÁGINA DE INICIO
======================================-->

<!-- content-wrapper -->
<div class="content-wrapper">

 
   <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"> Tablero Inicio
            </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Tablero</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  <!-- content-header -->

  <!-- content -->
  <section class="content">
    <div class="container-fluid">
    
    <!-- row -->
    <div class="row">

       <?php

        if($_SESSION["perfil"] == "administrador"){

        include "inicio/cajas-superiores.php";

        }
      
      ?>

    </div>
    <!-- row -->

    <!-- row -->
    <div class="row">

      
        
         <?php

         if($_SESSION["perfil"] == "administrador"){

          echo '<div class="col-lg-6">';
       
          include "inicio/grafico-ventas.php";
          include "inicio/productos-mas-vendidos.php";

          echo '</div>';

          }      

        ?>

 
         <?php

          if($_SESSION["perfil"] == "administrador"){

            echo ' <div class="col-lg-6">';
         
            include "inicio/grafico-visitas.php";
            include "inicio/ultimos-usuarios.php";

            echo '</div>'; 

          }else{

          echo ' <div class="col-lg-12">';
       
          include "inicio/grafico-visitas.php";
          include "inicio/ultimos-usuarios.php";

          echo '</div>';

          }         

        ?>

       <div class="col-lg-12">

        <?php

        include "inicio/productos-recientes.php";

        ?>

      </div>

    </div>
    <!-- row -->
    </div>

 </section>
  <!-- content -->

</div>
<!-- content-wrapper -->

  