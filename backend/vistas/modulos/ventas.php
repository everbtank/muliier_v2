<?php

if($_SESSION["perfil"] != "administrador"){

echo '<script>

  window.location = "inicio";

</script>';

return;

}

?>

<div class="content-wrapper">

  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Gestor ventas<h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Gestor ventas</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  
   


  <section class="content">

    <div class="card"> 

      <div class="card-header with-border">
        
        <?php

        include "inicio/grafico-ventas.php";

        ?>

      </div>

      <div class="box-body">

        <div class="box-tools">

          <a href="vistas/modulos/reportes.php?reporte=compras">
            
              <button class="btn btn-success">Descargar reporte en Excel</button>

          </a>

        </div>

        <br>
        <div class="table-responsive">
        
          <table class="table table-bordered table-striped dt-responsive tablaVentas" width="100%">
          
            <thead>
              
              <tr>
                
                <th style="width:10px">#</th>
                <th>Id</th>
                <th>Producto</th>
                <th>Imagen Producto</th>
                <th>Cliente</th>
                <th>Venta</th>
                <th>Metodo</th>  
                <th>Proceso de envío</th>         
                <th>Celular</th>
                <th>Dirección</th>
                <th>Codigo</th>
                <th>Asesor</th>
                <th>Fecha</th>

              </tr>

            </thead> 


          </table>
          
        </div>


      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL DATOS CLIENTE
======================================-->

<div id="modalDatosCliente" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

        <div class="modal-header" style="background:#E50031; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <center><h4 class="modal-title">DATOS CLIENTE</h4></center>

        </div>


        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon">Nombre</span> 

                <input type="text" class="form-control input-lg nombre" id="nombre" name="nombre"  readonly>

              </div>

            </div>

         

             <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon">Email</span> 

                <input type="email" class="form-control input-lg email" id="email" name="email"  readonly>

              </div>

            </div>

           <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon">Celular:</span> 

                <input type="text" class="form-control input-lg celular" id="celular" name="celular"  readonly>

              </div>

            </div>

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon">Direccion:</span> 

                <input type="text" class="form-control input-lg direccion" id="direccion" name="direccion"  readonly>

              </div>

            </div>

             <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon">Barrio:</span> 

                <input type="text" class="form-control input-lg barrio" id="barrio" name="barrio"  readonly>

              </div>

            </div>


            <center><div class="form-group">              

              <img src="vistas/img/perfiles/default/anonymous.png" class="img-thumbnail previsualizar" width="150px">

            </div></center>

          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Salir</button>

        

        </div>


    </div>

  </div>

</div>