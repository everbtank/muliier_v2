<div class="content-wrapper">

   <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Gestor Asesor<h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Gestor Asesor</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  

  <section class="content">

    <div class="card">  

      <div class="card-header with-border">

      </div>

      <div class="card-body">

        <div class="card-tools">

          <a href="vistas/modulos/reportes.php?reporte=asesor">

            <button class="btn btn-success" style="margin-top:5px">Descargar reporte en Excel</button>

          </a>

        </div> 

        <br>

        <div class="table-responsive">
         
          <table class="table table-bordered table-striped dt-responsive tablaAsesor" width="100%">

            <thead>
              
              <tr>
                
                <th style="width:10px">#</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Foto</th>
                <th>Codigo</th>
                <th>Comision</th>
                <th>Cantidad de vendidos</th>
                <th>Puntaje</th>
                <th>Cantidad Afiliados</th>
              </tr>

            </thead>

          </table>

        </div>

      </div>
        
    </div>

  </section>

</div>



