<?php

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";
require_once "../modelos/rutas.php";

class TablaAsesor{

 	/*=============================================
  	MOSTRAR LA TABLA DE USUARIOS
  	=============================================*/ 

	public function mostrarTabla(){	

		$item = null;
 		$valor = null;


 		$usuarios = ControladorUsuarios::ctrMostrarAsesor($item, $valor);

 		$url = Ruta::ctrRuta();

 		if(count($usuarios) == 0){

	      $datosJson = '{ "data":[]}';

	      echo $datosJson;

	      return;

	    }

 		$datosJson = '{
		 
	 	"data": [ ';

	 	for($i = 0; $i < count($usuarios); $i++){

	 		/*=============================================
			TRAER FOTO USUARIO
			=============================================*/
		

		   $item2 = "id_asesor_usuario";
           $valor2 = $usuarios[$i]["id_asesor"];

            $traerusuarios = ControladorUsuarios::ctrMostrarUsuarios($item2, $valor2);


			if($traerusuarios["foto_usuario"] != ""  && $traerusuarios["modo_usuario"] == "directo"){

				$foto = "<img class='img-circle' src='".$url.$traerusuarios["foto_usuario"]."' width='60px'>";

			}else if($traerusuarios["foto_usuario"] != "" && $traerusuarios["modo_usuario"] != "directo"){

				$foto = "<img class='img-circle' src='".$traerusuarios["foto_usuario"]."' width='60px'>";

			}else{

				$foto = "<img class='img-circle' src='vistas/img/usuarios/default/anonymous.png' width='60px'>";
			}

			

			$datosJson	 .= '[
				      "'.($i+1).'",
				      "'.$traerusuarios["nombre_usuario"].'",
				      "'.$traerusuarios["email_usuario"].'",
				      "'.$foto.'",
				      "'.$usuarios[$i]["codigo_asesor"].'",
				      "'.$usuarios[$i]["comision_asesor"].'",
				      "'.$usuarios[$i]["cantidad_vendidos_asesor"].'",
				      "'.$usuarios[$i]["puntaje_asesor"].'",
				      "'.$usuarios[$i]["cantidad_asesor"].'"       
				    ],';

	 	}

	 	$datosJson = substr($datosJson, 0, -1);

		$datosJson.=  ']
			  
		}'; 

		echo $datosJson;

 	}

}

/*=============================================
ACTIVAR TABLA DE VENTAS
=============================================*/ 
$activar = new TablaAsesor();
$activar -> mostrarTabla();



