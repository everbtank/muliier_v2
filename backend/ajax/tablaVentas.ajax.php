<?php

require_once "../controladores/ventas.controlador.php";
require_once "../modelos/ventas.modelo.php";

require_once "../controladores/productos.controlador.php";
require_once "../modelos/productos.modelo.php";

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

class TablaVentas{

  /*=============================================
  MOSTRAR LA TABLA DE VENTAS
  =============================================*/

  public function mostrarTabla(){	

  	$ventas = ControladorVentas::ctrMostrarVentas();

  	if(count($ventas) == 0){

      $datosJson = '{ "data":[]}';

      echo $datosJson;

      return;

    }


  	$datosJson = '{
		 
	 "data": [ ';

	for($i = 0; $i < count($ventas); $i++){

		/*=============================================
		TRAER PRODUCTO
		=============================================*/

		$item = "id_producto";
		$valor = $ventas[$i]["id_producto_compra"];

		$traerProducto = ControladorProductos::ctrMostrarProductos($item, $valor);

		$producto = $traerProducto["titulo_producto"];

		$imgProducto = "<img class='img-thumbnail' src='".$traerProducto["portada_producto"]."' width='100px'>";

		$tipo = $traerProducto["tipo_producto"];


		/*=============================================
		TRAER CLIENTE
		=============================================*/

		$item2 = "id_usuario";
		$valor2 = $ventas[$i]["id_usuario_compra"];

		$traerUsuario = ControladorUsuarios::ctrMostrarUsuarios($item2, $valor2);

		$cliente = "<a href='#modalDatosCliente' idCliente='".$traerUsuario["id_usuario"]."' data-toggle='modal' id='btnDatosCliente'>".$traerUsuario["nombre_usuario"]."</a>";


		$item3 = "codigo_asesor";
		$valor3 = $ventas[$i]["cod_asesor_compra"];

		$traerAsesor = ControladorUsuarios::ctrMostrarAsesor($item3, $valor3);

		$cod_asesor = $traerAsesor["codigo_asesor"];
		$id_asesor = $traerAsesor["id_asesor"];


		if($traerUsuario["id_asesor_usuario"] == $id_asesor){
            	
			    $asesor=$traerUsuario["nombre_usuario"];
					
		}else{

		$asesor="";

	}


		/*=============================================
		TRAER FOTO CLIENTE
		=============================================*/

		if($traerUsuario["foto_usuario"] != ""){

			$imgCliente = "<img class='img-circle' src='".$traerUsuario["foto_usuario"]."' width='70px'>";

		}else{

			$imgCliente = "<img class='img-circle' src='vistas/img/usuarios/default/anonymous.png' width='70px'>";
		}

		/*=============================================
		TRAER EMAIL CLIENTE
		=============================================*/

		if($ventas[$i]["email_compra"] == ""){

			$email = $traerUsuario["email_usuario"];

		}else{

			$email = $ventas[$i]["email_compra"];
		}

		/*=============================================
		TRAER PROCESO DE ENVÍO
		=============================================*/

		if($ventas[$i]["envio_compra"] == 0 && $tipo == "virtual"){

			$envio = "<button class='btn btn-info btn-xs'>Entrega inmediata</button>";
		
		}else if($ventas[$i]["envio_compra"] == 0 && $tipo == "fisico"){

			$envio ="<button class='btn btn-danger btn-xs btnEnvio' idVenta='".$ventas[$i]["id_compra"]."' etapa='1'>Despachando el producto</button>";

		}else if($ventas[$i]["envio_compra"] == 1 && $tipo == "fisico"){

			$envio = "<button class='btn btn-warning btn-xs btnEnvio' idVenta='".$ventas[$i]["id_compra"]."' etapa='2'>Enviando el producto</button>";

		}else{

			$envio = "<button class='btn btn-success btn-xs'>Producto entregado</button>";

		}

		/*=============================================
		LOGOS PAYPAL Y PAYU
		=============================================*/

		if($ventas[$i]["metodo_compra"] == "paypal"){

			$metodo = "<img class='img-responsive' src='vistas/img/plantilla/paypal.jpg' width='300px'>";
		
		}else if($ventas[$i]["metodo_compra"] == "payu"){

			$metodo = "<img class='img-responsive' src='vistas/img/plantilla/payu.jpg' width='300px'>";
		
		}else{

			$metodo = "CONTRA ENTREGA";

		}


		/*=============================================
		DEVOLVER DATOS JSON
		=============================================*/
		$datosJson	 .= '[
			      		"'.($i+1).'",
			      		"'.$ventas[$i]["id_compra"].'",
			      		"'.$producto.'",
			      		"'.$imgProducto.'",
			      		"'.$cliente.'",
			      		"$ '.number_format($ventas[$i]["pago_compra"],2).'",
			      		"'.$ventas[$i]["metodo_compra"].'",
			      		"'.$envio.'",
			      		"'.$ventas[$i]["celular_compra"].'",
			      		"'.$ventas[$i]["direccion_compra"].'",
			      		"'.$cod_asesor.'",
			      		"'.$asesor.'",
			      		"'.$ventas[$i]["fecha_compra"].'"	
			      		],';

	} 

	$datosJson = substr($datosJson, 0, -1);

	$datosJson.=  ']
		  
	}'; 
  	
  	echo $datosJson;	

  }

}

/*=============================================
ACTIVAR TABLA DE VENTAS
=============================================*/ 
$activar = new TablaVentas();
$activar -> mostrarTabla(); 

