<?php
require_once "../controladores/productos.controlador.php";
require_once "../modelos/productos.modelo.php";

require_once "../controladores/categorias.controlador.php";
require_once "../modelos/categorias.modelo.php";

require_once "../controladores/subcategorias.controlador.php";
require_once "../modelos/subcategorias.modelo.php";

require_once "../controladores/cabeceras.controlador.php";
require_once "../modelos/cabeceras.modelo.php";

class TablaProductos
{
    /*=============================================
    MOSTRAR LA TABLA DE PRODUCTOS
    =============================================*/
    public function mostrarTablaProductos()
    {
        $item      = null;
        $valor     = null;
        $productos = ControladorProductos::ctrMostrarProductos($item, $valor);
        // var_dump($productos);
        if (count($productos)) {
            $datosJson = '{
                "data":[';
            for ($i = 0; $i < count($productos); $i++) {
                /*=============================================
                TRAER LAS CATEGORÍAS
                =============================================*/
                $item       = "id_categoria";
                $valor      = $productos[$i]["id_categoria_producto"];
                $categorias = ControladorCategorias::ctrMostrarCategorias($item, $valor);
                if ($categorias["categoria_categoria"] == "") {
                    $categoria = "SIN CATEGORÍA";
                } else {
                    $categoria = $categorias["categoria_categoria"];
                }

                $item2  = "id_subcategoria";
                $valor2 = $productos[$i]["id_subcategoria_producto"];

                $subcategorias = ControladorSubCategorias::ctrMostrarSubCategorias($item2, $valor2);

               


                if ($subcategorias["subcategoria_subcategoria"] == "") {

                    $subcategoria = "SIN SUBCATEGORÍA";

                } else {

                   $subcategoria = $subcategorias["subcategoria_subcategoria"];
                }

                /*=============================================
                AGREGAR ETIQUETAS DE ESTADO
                =============================================*/
                if ($productos[$i]["estado_producto"] == 0) {
                    $colorEstado    = "btn-danger";
                    $textoEstado    = "Desactivado";
                    $estadoProducto = 1;
                } else {
                    $colorEstado    = "btn-success";
                    $textoEstado    = "Activado";
                    $estadoProducto = 0;
                }
                $estado = "<button class='btn btn-xs btnActivar " . $colorEstado . "' idProducto='" . $productos[$i]["id_producto"] . "' estadoProducto='" . $estadoProducto . "'>" . $textoEstado . "</button>";
               

               
            
                if ($productos[$i]["portada_producto"] != "") {

                    $imagenPortada = "<img src='" . $productos[$i]["portada_producto"] . "' class='img-thumbnail imgPortadaProductos' width='100px'>";

                } else {

                    $imagenPortada = "<img src='vistas/img/cabeceras/default/default.jpg' class='img-thumbnail imgPortadaProductos' width='100px'>";
                }

                $imagenPrincipal = "<img src='" . $productos[$i]["portada_producto"] . "' class='img-thumbnail imgTablaPrincipal' width='100px'>";
                /*=============================================
                TRAER MULTIMEDIA
                =============================================*/
                if ($productos[$i]["multimedia_producto"] != null) {

                    $multimedia = json_decode($productos[$i]["multimedia_producto"], true);

                    if ($multimedia[0]["foto"] != "") {
                        $vistaMultimedia = "<img src='" . $multimedia[0]["foto"] . "' class='img-thumbnail imgTablaMultimedia' width='100px'>";
                    } else {
                        $vistaMultimedia = "<img src='http://i3.ytimg.com/vi/" . $productos[$i]["multimedia_producto"] . "/hqdefault.jpg' class='img-thumbnail imgTablaMultimedia' width='100px'>";
                    }
                } else {
                    $vistaMultimedia = "<img src='vistas/img/multimedia/default/default.jpg' class='img-thumbnail imgTablaMultimedia' width='100px'>";
                }
                /*=============================================
                TRAER DETALLES
                =============================================*/
                $detalles = json_decode($productos[$i]["detalles_producto"], true);

                if ($productos[$i]["tipo_producto"] == "fisico") {

                    $talla         = json_encode($detalles["Talla"]);
                    $color         = json_encode($detalles["Color"]);
                    $marca         = json_encode($detalles["Marca"]);
                    $vistaDetalles = "Talla: " . str_replace(array("[", "]", '"'), "", $talla) . " - Color: " . str_replace(array("[", "]", '"'), "", $color) . " - Marca: " . str_replace(array("[", "]", '"'), "", $marca);
                } else {
                    $vistaDetalles = "Clases: " . $detalles["Clases"] . ", Tiempo: " . $detalles["Tiempo"] . ", Nivel: " . $detalles["Nivel"] . ", Acceso: " . $detalles["Acceso"] . ", Dispositivo: " . $detalles["Dispositivo"] . ", Certificado: " . $detalles["Certificado"];
                }
                /*=============================================
                TRAER PRECIO
                =============================================*/
                if ($productos[$i]["precio_producto"] == 0) {
                    $precio = "0";
                } else {
                    $precio = "$ " . number_format($productos[$i]["precio_producto"], 2);
                }
                /*=============================================
                TRAER ENTREGA
                =============================================*/
                if ($productos[$i]["entrega_producto"] == 0) {
                    $entrega = "Inmediata";
                } else {
                    $entrega = $productos[$i]["entrega_producto"] . " días hábiles";
                }
                /*=============================================
                REVISAR SI HAY OFERTAS
                =============================================*/
                if ($productos[$i]["oferta_producto"] != 0) {
                    if ($productos[$i]["precioOferta_producto"] != 0) {
                        $tipoOferta  = "PRECIO";
                        $valorOferta = "$ " . number_format($productos[$i]["precioOferta_producto"], 2);
                    } else {
                        $tipoOferta  = "DESCUENTO";
                        $valorOferta = $productos[$i]["descuentoOferta_producto"] . " %";
                    }
                } else {
                    $tipoOferta  = "No tiene oferta";
                    $valorOferta = 0;
                }
                /*=============================================
                TRAER IMAGEN OFERTA
                =============================================*/
                if ($productos[$i]["imgOferta_producto"] != "") {
                    $imgOferta = "<img src='" . $productos[$i]["imgOferta_producto"] . "' class='img-thumbnail imgTablaProductos' width='100px'>";
                } else {
                    $imgOferta = "<img src='vistas/img/ofertas/default/default.jpg' class='img-thumbnail imgTablaProductos' width='100px'>";
                }
                /*=============================================
                TRAER LAS ACCIONES
                =============================================*/
                $acciones = "<div class='btn-group'><button class='btn btn-warning btnEditarProducto' idProducto='" . $productos[$i]["id_producto"] . "' data-toggle='modal' data-target='#modalEditarProducto'><i class='fa fa-pencil'></i></button><button class='btn btn-danger btnEliminarProducto' idProducto='" . $productos[$i]["id_producto"] . "' imgOferta='" . $productos[$i]["imgOferta_producto"] . "' rutaCabecera='" . $productos[$i]["ruta_producto"] . "' imgPrincipal='" . $productos[$i]["portada_producto"] . "'><i class='fa fa-times'></i></button></div>";
                /*=============================================
                CONSTRUIR LOS DATOS JSON
                =============================================*/
                $datosJson .= '[
                        "' . ($i + 1) . '",
                        "' . $productos[$i]["titulo_producto"] . '",
                        "' . $productos[$i]["id_producto"] . '",   
                        "' . $categoria . '",
                        "' . $subcategoria . '",
                        "' . $estado . '",
                        "' . $precio . '",
                        "' . $entrega . '",
                        "' . $productos[$i]["precio_venta_producto"] . '",
                        "' . $imagenPortada . '",
                        "' . $imagenPrincipal . '",
                        "' . $vistaMultimedia . '",
                        "' . $vistaDetalles . '",
                        "' . $productos[$i]["peso_producto"] . ' kg",
                        "' . $productos[$i]["fecha_producto"] . '",
                        "' . $entrega . '",
                        "' . $tipoOferta . '",
                        "' . $valorOferta . '",
                        "' . $imgOferta . '",
                        "' . $productos[$i]["finOferta_producto"] . '",
                        "' . $acciones . '"
                    ],';
            }
            $datosJson = substr($datosJson, 0, -1);
            $datosJson .= ']
            }';
            echo $datosJson;} else {
            echo '{"data":[]}';
            return;}
    }
}

/*=============================================
ACTIVAR TABLA DE PRODUCTOS
=============================================*/
$activarProductos = new TablaProductos();
$activarProductos->mostrarTablaProductos();
