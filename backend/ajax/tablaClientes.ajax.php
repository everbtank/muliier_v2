<?php

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";
require_once "../modelos/rutas.php";

class TablaCliente{

 	/*=============================================
  	MOSTRAR LA TABLA DE USUARIOS
  	=============================================*/ 

	public function mostrarTabla(){	

 		//echo '<script>alert("alerdsa");</script>';

		$item = null;
 		$valor = null;

 	
       
 		$clientes = ControladorUsuarios::ctrMostrarClientes($item, $valor);

 		$url = Ruta::ctrRuta();

 		if(count($clientes) == 0){

	      $datosJson = '{ "data":[]}';

	      echo $datosJson;

	      return;

	    }

 		$datosJson = '{
		 
	 	"data": [ ';

	 	for($i = 0; $i < count($clientes); $i++){

	 		/*=============================================
			TRAER FOTO USUARIO
			=============================================*/
		

		   $item2 = "id_cliente_usuario";
           $valor2 = $clientes[$i]["id_cliente"];
           
            $traerusuarios = ControladorUsuarios::ctrMostrarUsuarios($item2, $valor2);


			if($traerusuarios["foto_usuario"] != ""  && $traerusuarios["modo_usuario"] == "directo"){

				$foto = "<img class='img-circle' src='".$url.$traerusuarios["foto_usuario"]."' width='60px'>";

			}else if($traerusuarios["foto_usuario"] != "" && $traerusuarios["modo_usuario"] != "directo"){

				$foto = "<img class='img-circle' src='".$traerusuarios["foto_usuario"]."' width='60px'>";

			}else{

				$foto = "<img class='img-circle' src='vistas/img/usuarios/default/anonymous.png' width='60px'>";
			}


			if($clientes[$i]["estado_afiliacion_cliente"]==1){
            	
            	$estado= "<p><i class='fa fa-check-circle-o' ></i></p>";

          	}else{

               $estado= "<p><i class='fa fa-circle-o'></i></p>";
          	}



			

			$datosJson	 .= '[
				      "'.($i+1).'",
				      "'.$traerusuarios["nombre_usuario"].'",
				      "'.$foto.'",
				      "'.$traerusuarios["email_usuario"].'",
				      "'.$traerusuarios["celular_usuario"].'",
				      "'.$estado.'",
				      "'.$clientes[$i]["cod_asesor_cliente"].'"
				    ],';

	 	}

	 	$datosJson = substr($datosJson, 0, -1);

		$datosJson.=  ']
			  
		}'; 

		echo $datosJson;

 	}

}

/*=============================================
ACTIVAR TABLA DE VENTAS
=============================================*/ 
$activar = new TablaCliente();
$activar -> mostrarTabla();


