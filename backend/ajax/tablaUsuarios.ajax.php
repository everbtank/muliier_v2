<?php

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";
require_once "../modelos/rutas.php";

class TablaUsuarios{

 	/*=============================================
  	MOSTRAR LA TABLA DE USUARIOS
  	=============================================*/ 

	public function mostrarTabla(){	

		$item = null;
 		$valor = null;

 		$usuarios = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

 		$url = Ruta::ctrRuta();

 		if(count($usuarios) == 0){

	      $datosJson = '{ "data":[]}';

	      echo $datosJson;

	      return;

	    }

 		$datosJson = '{
		 
	 	"data": [ ';

	 	for($i = 0; $i < count($usuarios); $i++){

	 		/*=============================================
			TRAER FOTO USUARIO
			=============================================*/

			if($usuarios[$i]["foto_usuario"] != ""  && $usuarios[$i]["modo_usuario"] == "directo"){

				$foto = "<img class='img-circle' src='".$url.$usuarios[$i]["foto_usuario"]."' width='60px'>";

			}else if($usuarios[$i]["foto_usuario"] != "" && $usuarios[$i]["modo_usuario"] != "directo"){

				$foto = "<img class='img-circle' src='".$usuarios[$i]["foto_usuario"]."' width='60px'>";

			}else{

				$foto = "<img class='img-circle' src='vistas/img/usuarios/default/anonymous.png' width='60px'>";
			}

			/*=============================================
  			REVISAR ESTADO
  			=============================================*/

  			if($usuarios[$i]["modo_usuario"] == "directo"){

	  			if( $usuarios[$i]["verificacion_usuario"] == 1){

	  				$colorEstado = "btn-danger";
	  				$textoEstado = "Desactivado";
	  				$estadoUsuario = 0;

	  			}else{

	  				$colorEstado = "btn-success";
	  				$textoEstado = "Activado";
	  				$estadoUsuario = 1;

	  			}

	  			$estado = "<button class='btn btn-xs btnActivar ".$colorEstado."' idUsuario='". $usuarios[$i]["id_usuario"]."' estadoUsuario='".$estadoUsuario."'>".$textoEstado."</button>";

	  		}else{

	  			$estado = "<button class='btn btn-xs btn-info'>Activado</button>";

	  		}


	 		/*=============================================
			DEVOLVER DATOS JSON
			=============================================*/

			$datosJson	 .= '[
				      "'.($i+1).'",
				      "'.$usuarios[$i]["nombre_usuario"].'",
				      "'.$usuarios[$i]["email_usuario"].'",
				      "'.$usuarios[$i]["modo_usuario"].'",
				      "'.$foto.'",
				      "'.$estado.'",
				      "'.$usuarios[$i]["fecha_usuario"].'",
				      "'.$usuarios[$i]["tipo_usuario"].'"    
				    ],';

	 	}

	 	$datosJson = substr($datosJson, 0, -1);

		$datosJson.=  ']
			  
		}'; 

		echo $datosJson;

 	}

}

/*=============================================
ACTIVAR TABLA DE VENTAS
=============================================*/ 
$activar = new TablaUsuarios();
$activar -> mostrarTabla();



