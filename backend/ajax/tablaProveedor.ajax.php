<?php

require_once "../controladores/proveedor.controlador.php";
require_once "../modelos/proveedor.modelo.php";
require_once "../modelos/rutas.php";

class TablaProveedor{

 	/*=============================================
  	MOSTRAR LA TABLA DE USUARIOS
  	=============================================*/ 
  		

	public function mostrarTabla(){	

		$item = null;
 		$valor = null;

      
 		$proveedor = ControladorProveedor::ctrMostrarProveedor($item, $valor);
 		

 		$url = Ruta::ctrRuta();

 		if(count($proveedor) == 0){

	      $datosJson = '{ "data":[]}';

	      echo $datosJson;

	      return;

	    }

 		$datosJson = '{
		 
	 	"data": [ ';

	 	for($i = 0; $i < count($proveedor); $i++){

	 		/*=============================================
			TRAER FOTO USUARIO
			=============================================*/

			if($proveedor[$i]["foto_proveedor"] != ""  && $proveedor[$i]["modo_proveedor"] == "directo"){

				$foto = "<img class='img-circle' src='".$url.$proveedor[$i]["foto_proveedor"]."' width='60px'>";

			}else if($proveedor[$i]["foto_proveedor"] != "" && $proveedor[$i]["modo_proveedor"] != "directo"){

				$foto = "<img class='img-circle' src='".$proveedor[$i]["foto_proveedor"]."' width='60px'>";

			}else{

				$foto = "<img class='img-circle' src='vistas/img/usuarios/default/anonymous.png' width='60px'>";
			}

			/*=============================================
  			REVISAR ESTADO
  			=============================================*/

  			if($proveedor[$i]["modo_proveedor"] == "directo"){

	  			if( $proveedor[$i]["verificacion_proveedor"] == 1){

	  				$colorEstado = "btn-danger";
	  				$textoEstado = "Desactivado";
	  				$estadoProveedor = 0;

	  			}else{

	  				$colorEstado = "btn-success";
	  				$textoEstado = "Activado";
	  				$estadoProveedor = 1;

	  			}

	  			$estado = "<button class='btn btn-xs btnActivar ".$colorEstado."' idProveedor='". $proveedor[$i]["id_proveedor"]."' estadoProveedor='".$estadoProveedor."'>".$textoEstado."</button>";

	  		}else{

	  			$estado = "<button class='btn btn-xs btn-info'>Activado</button>";

	  		}


	 		/*=============================================
			DEVOLVER DATOS JSON
			=============================================*/

			$datosJson	 .= '[
				      "'.($i+1).'",
				      "'.$proveedor[$i]["nombre_proveedor"].'",
				      "'.$proveedor[$i]["email_proveedor"].'",
				      "'.$proveedor[$i]["modo_proveedor"].'",
				      "'.$foto.'",
				      "'.$estado.'",
				      "'.$proveedor[$i]["fecha_proveedor"].'"    
				    ],';

	 	}

	 	$datosJson = substr($datosJson, 0, -1);

		$datosJson.=  ']
			  
		}'; 

		echo $datosJson;

 	}

}

/*=============================================
ACTIVAR TABLA DE PROVEEDOR
=============================================*/ 
$activar = new TablaProveedor();
$activar -> mostrarTabla();



