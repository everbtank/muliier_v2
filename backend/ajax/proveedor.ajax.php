<?php

require_once "../controladores/proveedor.controlador.php";
require_once "../modelos/proveedor.modelo.php";

class AjaxProveedor{

  /*=============================================
  ACTIVAR USUARIOS
  =============================================*/	

  public $activarProveedor;
  public $activarId;

  public function ajaxActivarProveedor(){

  	$respuesta = ModeloProveedor::mdlActualizarProveedor("proveedor", "verificacion", $this->activarProveedor, "id_proveedor", $this->activarId);

  	echo $respuesta;

  }

}

/*=============================================
ACTIVAR CATEGORIA
=============================================*/

if(isset($_POST["activarProveedor"])){

	$activarUsuario = new AjaxProveedor();
	$activarUsuario -> activarProveedor = $_POST["activarProveedor"];
	$activarUsuario -> activarId = $_POST["activarId"];
	$activarUsuario -> ajaxActivarProveedor();

}