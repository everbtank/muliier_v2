<?php

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

class AjaxUsuarios{

  /*=============================================
  ACTIVAR USUARIOS
  =============================================*/	

  public $activarUsuario;
  public $activarId;
  public $idCliente;

  public function ajaxActivarUsuario(){

  	$respuesta = ModeloUsuarios::mdlActualizarUsuario("usuarios", "verificacion_usuario", $this->activarUsuario, "id_usuario", $this->activarId);

  	echo $respuesta;

  }

   public function ajaxDatosCliente(){

    $item = "id_usuario";
    $valor = $this->idCliente;

    $respuesta = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

    echo json_encode($respuesta);

  }

}

/*=============================================
ACTIVAR CATEGORIA
=============================================*/

if(isset($_POST["activarUsuario"])){

	$activarUsuario = new AjaxUsuarios();
	$activarUsuario -> activarUsuario = $_POST["activarUsuario"];
	$activarUsuario -> activarId = $_POST["activarId"];
	$activarUsuario -> ajaxActivarUsuario();

}

if(isset($_POST["idCliente"])){

  $datoscliente = new AjaxUsuarios();
  $datoscliente -> idCliente = $_POST["idCliente"];
  $datoscliente -> ajaxDatosCliente();

}